"use strict";

var cassandraClient = require("./models/cassandraClient");
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var passport = require('passport');

const route_users = require('./routes/route_users');
const route_devices = require('./routes/route_devices');
const route_cycles = require('./routes/route_cycles');
const route_samples = require('./routes/route_samples');
const route_harvests = require('./routes/route_harvests');
const route_measurements = require('./routes/route_measurements');
const route_feedings = require('./routes/route_feedings');
const route_fishactivities = require('./routes/route_fishactivities');
const route_feedingprogram = require('./routes/route_feedingprograms');
const route_clusters = require('./routes/route_clusters');
const route_species = require('./routes/route_species');
const route_feeds = require('./routes/route_feeds');
const route_jobs = require('./routes/route_jobs');
const route_notifications = require('./routes/route_notifications');
const route_problems = require('./routes/route_problems');
const route_recommendations = require('./routes/route_recommendations');
//const route_helps = require('./routes/route_helps');

var port = process.env.port || 80 || 443 || 8080;
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

app.use(passport.initialize());
app.use('/users', route_users);
app.use('/devices', route_devices);
app.use('/cycles', route_cycles);
app.use('/samples', route_samples);
app.use('/harvests', route_harvests);
app.use('/measurements', route_measurements);
app.use('/feedings', route_feedings);
app.use('/fishactivities', route_fishactivities);
app.use('/feedingprograms', route_feedingprogram);
app.use('/clusters', route_clusters);
app.use('/species', route_species);
app.use('/feeds', route_feeds);
app.use('/jobs', route_jobs);
app.use('/notifications', route_notifications);
app.use('/problems', route_problems);
app.use('/recommendations', route_recommendations);
//app.use('/helps', route_helps);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

cassandraClient.then(function(response) {
  console.log(response);

  app.listen(port, function(err) {
    if (err) throw err;
    console.log('server is listening on ' + port);
  });

  app.get('/', function(req, res) {
    res.send('FISHARE API V0.0.5');
  });


}, function(error) {
  console.error(error);
  process.exit(1);
})
