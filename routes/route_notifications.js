'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var notificationsController = require('../controllers/notificationsController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));


//FOR MOBILE APPS ANDROID -------------------------------------------------
//get Meta Notifications By User
router.route('/getMetaNotificationsByUser/:page_state_meta_notification?')
  .get(authController.isAuthenticated, notificationsController.getNotificationsByUser_Meta);

//get Notifications By User
router.route('/getNotificationsByUser/notification_date/:notification_date/:page_state_notification?')
  .get(authController.isAuthenticated, notificationsController.getNotificationsByUser);

//add Notifications
router.route('/addNotification')
  .post(authController.isAuthenticated, notificationsController.addNotification);

//update Notifications as Read
router.route('/updateNotification')
  .put(authController.isAuthenticated, notificationsController.updateNotification);

//END FOR MOBILE APPS ANDROID ----------------------------------------------


//FOR BACK END -------------------------------------------------------------

//END FOR BACK END --------------------------------------------------------
module.exports = router;
