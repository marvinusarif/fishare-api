'use strict';

var path = require('path');
var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var memoryStorage = multer.memoryStorage();

var authController = require('../controllers/authController')(models);
var problemsController = require('../controllers/problemsController')(models);
var filesController = require('../controllers/filesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

var multer_config = {
  storage: memoryStorage,
  limit: {
    fileSize: 1024 * 1024
  },
  fileFilter: function(req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.jpg' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'))
    }
    callback(null, true);
  }
}


//FOR MOBILE APPS ANDROID -------------------------------------------------
//get Problems By Cycle
router.route('/getProblemsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/:page_state_problem?')
  .get(authController.isAuthenticated, problemsController.getProblemsByCycle);

//add Sample
router.route('/addProblem').post(authController.isAuthenticated, multer(multer_config).single('problem_img'), filesController.uploadProblemImage, problemsController.addProblem);

//END FOR MOBILE APPS ANDROID ----------------------------------------------


//FOR BACK END -------------------------------------------------------------

//END FOR BACK END --------------------------------------------------------
module.exports = router;
