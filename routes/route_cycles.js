'use strict';

var path = require('path');
var models = require('express-cassandra');
const express = require('express')
var bodyParser = require('body-parser');
var multer = require('multer');
var memoryStorage = multer.memoryStorage();

var authController = require('../controllers/authController')(models);
var cyclesController = require('../controllers/cyclesController')(models);
var filesController = require('../controllers/filesController')(models);

var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var multer_config = {
  storage: memoryStorage,
  limit: {
    fileSize: 1024 * 1024
  },
  fileFilter: function(req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.jpg' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'))
    }
    callback(null, true);
  }
}

//FOR MOBILE APPS ANDROID -------------------------------------------------

//android cycles by user
router.route('/getMetaCyclesByUser/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/:page_state_meta_cycle?')
.get(authController.isAuthenticated, cyclesController.findMetaCyclesByUser_TimeBeginEnd);

router.route('/getCyclesByUser/cycle_time_begin/:cycle_time_begin/:page_state_cycle?')
.get(authController.isAuthenticated, cyclesController.findCyclesByUser_TimeBegin);

router.route('/getCyclesByUser/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id')
.get(authController.isAuthenticated, cyclesController.findCyclesByUser_cycleId);

router.route('/getCyclesByUser/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/name/:name/:page_state_meta_cycle?')
.get(authController.isAuthenticated, cyclesController.findCyclesByUser_TimeBeginEnd_Name_MetaMerged);
//end android cycles by user

//post cycles
router.route('/addCycle')
.post(authController.isAuthenticated, cyclesController.addCycle);

//update cycle
router.route('/updateCycle')
.put(authController.isAuthenticated, cyclesController.updateCycle);

//upload cycle image
//multer cannot be sent globally
router.route('/uploadImage')
.post(authController.isAuthenticated, multer(multer_config).single('cycle'), filesController.uploadCycleImage, cyclesController.updateCycle);

//close cycle
router.route('/closeCycle')
.put(authController.isAuthenticated, cyclesController.closeCycle, cyclesController.updateCycle);

//END FOR MOBILE APPS ANDROID ----------------------------------------------

//FOR BACK END -------------------------------------------------------------

//backend meta cycles by cluster
router.route('/getMetaCyclesByCluster/cluster_id/:cluster_id/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/:page_state_meta_cycle?')
.get(cyclesController.findMetaCyclesByCluster_TimeBeginEnd);

router.route('/getCyclesByCluster/cluster_id/:cluster_id/cycle_time_begin/:cycle_time_begin/:page_state_cycle?')
.get(cyclesController.findCyclesByCluster_TimeBegin);

router.route('/getCyclesByCluster/cluster_id/:cluster_id/cycle_time_begin/:cycle_time_begin/user_id/:user_id/cycle_id/:cycle_id')
.get(cyclesController.findCyclesByCluster_cycleId);

/*
router.route('/getCyclesByUser/cycle_time_begin/:cycle_time_begin/:page_state_cycle?')
  .get(authController.isAuthenticated, cyclesController.findCyclesByUser_TimeBegin);

//backend cycles by cluster
router.route('/getCyclesByCluster/cluster_id/:cluster_id/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/:page_state_meta_cycle?')
  .get(cyclesController.findCyclesByCluster_TimeBeginEnd_MetaMerged);

//backend cycles by species
router.route('/getCyclesBySpecies/fish_species/:fish_species/crop_species/:crop_species/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/:page_state_meta_cycle?')
  .get(cyclesController.findCyclesBySpecies_TimeBeginEnd_MetaMerged);
*/

//END FOR BACK END --------------------------------------------------------

module.exports = router;
