'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var usersController = require('../controllers/usersController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------
router.route('/check')
  .get(authController.isAuthenticated, usersController.check);

router.route('/auth')
  .post(usersController.login);

router.route('/getEmail/:email')
  .get(usersController.getEmail);

router.route('/getProfile')
  .get(authController.isAuthenticated, usersController.getProfile);

router.route('/addUser')
  .post(usersController.addUser, usersController.sendEmailVerification);

router.route('/updateUser')
  .put(authController.isAuthenticated, usersController.updateUser);

router.route('/updatePassword')
  .put(authController.isAuthenticated,
    usersController.updatePassword);

router.route('/resendEmailVerification')
  .post(authController.isAuthenticated, usersController.sendEmailVerification);

router.route('/verifyEmail')
  .get(usersController.verifyEmail, usersController.confirmEmail);

router.route('/resetPassword')
  .post(usersController.resetPassword);

router.route('/resendPhoneVerification')
  .post(authController.isAuthenticated, usersController.sendPhoneVerification);

router.route('/verifyPhone')
  .post(authController.isAuthenticated, usersController.verifyPhone, usersController.confirmPhone);

router.get('/renderVerificationEmail', function(req, res) {
  var options = {
    firstName: 'first_name',
    lastName: 'last_name',
    tokenUrl: 'https://fishare',
    messageBody: 'Terima kasih telah bergabung bersama kami. Demi menciptakan keamanan akun pribadi anda, mohon klik tombol dibawah ini untuk memverifikasi akun anda. ',
    buttonVerification: 'Klik disini Untuk Verifikasi Email Sekarang',
    messageFooter: 'Selamat bergabung di fishare.asia',
    companyName: 'PT FISHARE INOVASI NUSANTARA',
    companyAddress: 'Intiland Annexe Building 8th Floor, Jl. Jend Sudirman Kav 32. Jakarta Pusat 10220',
    companyPhone: '+62 82 110267895'
  }
  res.render('verificationEmail', options);
});
router.get('/renderResetPassword', function(req, res) {
  var options = {
    firstName: 'first_name',
    lastName: 'last_name',
    messageBody: 'Terima kasih telah bergabung bersama kami. Demi menciptakan keamanan akun pribadi anda, mohon klik tombol dibawah ini untuk memverifikasi akun anda. ',
    passwordNew: '<new Pasword>',
    messageFooter: 'Selamat bergabung di fishare.asia',
    companyName: 'PT FISHARE INOVASI NUSANTARA',
    companyAddress: 'Intiland Annexe Building 8th Floor, Jl. Jend Sudirman Kav 32. Jakarta Pusat 10220',
    companyPhone: '+62 82 110267895'
  }
  res.render('resetPassword', options);
});
//END FOR MOBILE APPS ANDROID -------------------------------------------------

module.exports = router;
