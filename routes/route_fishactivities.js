'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var fishActivitiesController = require('../controllers/fishActivitiesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------

//add fish activities
router.route('/addFishActivities').post(authController.isAuthenticated, fishActivitiesController.addFishActivities);

//END FOR MOBILE APPS ANDROID ----------------------------------------------




//FOR BACK END -------------------------------------------------------------

//get fish activities by feeding time begin
router.route('/getFishActivitiesByFeeding/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/feeding_time_begin/:feeding_time_begin/page_state_fishactivities_meta?')
  .get(fishActivitiesController.getFishActivitiesByFeeding_FeedingTimeBegin_MetaMerged);

//get fish activities by feeding Id
router.route('/getFishActivitiesByFeeding/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/feeding_time_begin/:feeding_time_begin/event_time_bucket/:event_time_bucket/feeding_id/:feeding_id')
  .get(fishActivitiesController.getFishActivitiesByFeeding_FeedingTimeBegin_FeedingId);

//END FOR BACK END --------------------------------------------------------

module.exports = router;
