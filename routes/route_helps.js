"use strict";

var models = require('express-cassandra');
const router = require('express').Router();

router.use(function timeLog(req, res, next) {
  console.log('Time:', Date.now());
  next();
});

router.get('/', function(req, res) {
  res.json({
    message: 'no'
  });
});

module.exports = router;
