'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var clustersController = require('../controllers/clustersController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------
router.route('/getClustersByLocation')
.get(clustersController.getCountries);

router.route('/getClustersByLocation/country/:country')
.get(clustersController.getStates);

router.route('/getClustersByLocation/country/:country/state/:state')
.get(clustersController.getRegencies);

router.route('/getClustersByLocation/country/:country/state/:state/regency/:regency')
.get(clustersController.getDistricts);

router.route('/getClustersByLocation/country/:country/state/:state/regency/:regency/district/:district')
.get(clustersController.getVillages);

router.route('/getClustersByLocation/country/:country/state/:state/regency/:regency/district/:district/village/:village')
.get(clustersController.getCluster);

//END FOR MOBILE APPS ANDROID -------------------------------------------------




//FOR BACKEND -----------------------------------------------------------------

router.route('/addCluster').post(clustersController.addCluster);

//END FOR BACKEND -------------------------------------------------------------

module.exports = router;
