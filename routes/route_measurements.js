'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var measurementsController = require('../controllers/measurementsController')(models);
var cyclesController = require('../controllers/cyclesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID --------------------------------------------------

//get meta measurements by cycle
router.route('/getMetaMeasurementsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/parameter/:parameter/:page_state_measurement_meta?')
  .get(authController.isAuthenticated, measurementsController.getMetaMeasurementsByCycle);

//get measurements by cycle
router.route('/getMeasurementsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/parameter/:parameter/event_time_bucket/:event_time_bucket/:page_state_measurement?')
  .get(authController.isAuthenticated, measurementsController.getMeasurementsByCycle);

//add measurement
router.route('/addMeasurement').post(authController.isAuthenticated, measurementsController.addMeasurement, cyclesController.updateCycle);

//END FOR MOBILE APPS ANDROID ----------------------------------------------




//FOR BACK END ------------------------------------------------------------

//get measurements by Cluster
router.route('/getMeasurementsByCluster/cluster_id/:cluster_id/fish_species/:fish_species/crop_species/:crop_species/parameter/:parameter/:page_state_measurement_meta?')
  .get(measurementsController.getMeasurementsByCluster_Species_MetaMerged);

//END FOR BACK END --------------------------------------------------------

module.exports = router;
