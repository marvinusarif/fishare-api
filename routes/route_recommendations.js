'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var recommendationsController = require('../controllers/recommendationsController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));


//FOR MOBILE APPS ANDROID -------------------------------------------------

//get Sample By Cycle
router.route('/getRecommendationsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/:page_state_recommendation?')
  .get(authController.isAuthenticated, recommendationsController.recommendationsByCycle);


//END FOR MOBILE APPS ANDROID ----------------------------------------------
router.route('/addRecommendation')
  .post(authController.isAuthenticated, recommendationsController.addRecommendation)

router.route('/updateRecommendation')
  .put(authController.isAuthenticated, recommendationsController.updateRecommendation);

//FOR BACK END -------------------------------------------------------------

//END FOR BACK END --------------------------------------------------------
module.exports = router;
