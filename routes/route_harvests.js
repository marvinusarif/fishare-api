'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var harvestsController = require('../controllers/harvestsController')(models);
var cyclesController = require('../controllers/cyclesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));


//FOR MOBILE APPS ANDROID -------------------------------------------------

router.route('/getHarvestsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/type/:type/:page_state_harvest?').get(authController.isAuthenticated, harvestsController.getHarvestsByCycle);

router.route('/addHarvest').post(authController.isAuthenticated, harvestsController.addHarvest, harvestsController.getAllHarvestByCycle, cyclesController.updateCycle);

router.route('/updateHarvest').put(authController.isAuthenticated, harvestsController.updateHarvest, harvestsController.getAllHarvestByCycle, cyclesController.updateCycle);

//END FOR MOBILE APPS ANDROID -------------------------------------------------



//FOR BACK END -------------------------------------------------------------

//harvest by user
router.route('/getHarvestsByUser/user_id/:user_id/harvest_date_begin/:harvest_date_begin/harvest_date_end/:harvest_date_end/:page_state_meta_harvest?')
  .get(harvestsController.getHarvestsByUser_TimeBeginEnd_MetaMerged);

//harvest by cluster
router.route('/getHarvestsByCluster/cluster_id/:cluster_id/harvest_date_begin/:harvest_date_begin/harvest_date_end/:harvest_date_end/:page_state_meta_harvest?')
  .get(harvestsController.getHarvestsByCluster_TimeBeginEnd_MetaMerged);

//END FOR BACK END --------------------------------------------------------

module.exports = router;
