'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var feedsController = require('../controllers/feedsController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------

router.route('/getFeedsByCountry/type/:type')

  .get(authController.isAuthenticated, feedsController.getFeeds);

router.route('/getFeedsByCountry/type/:type/name/:name')

  .get(authController.isAuthenticated, feedsController.getFeedsByName);

//END FOR MOBILE APPS ANDROID -------------------------------------------------



//FOR BACKEND -----------------------------------------------------------------

router.route('/addFeed').post(feedsController.addFeed);

router.route('/updateFeed').put(feedsController.updateFeed);

//END FOR BACKEND -------------------------------------------------------------

module.exports = router;
