'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var speciesController = require('../controllers/speciesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------

router.route('/getSpeciesByCountry/type/:type').get(authController.isAuthenticated, speciesController.getSpecies);

router.route('/getSpeciesByCountry/type/:type/species/:species').get(authController.isAuthenticated, speciesController.getProducer);

router.route('/getSpeciesByCountry/type/:type/species/:species/producer/:producer').get(authController.isAuthenticated, speciesController.getSpeciesByProducerName);

//END FOR MOBILE APPS ANDROID ---------------------------------------------



//FOR BACKEND -----------------------------------------------------------------

router.route('/addSpecies').post(speciesController.addSpecies);
router.route('/updateSpecies').post(speciesController.updateSpecies);

//END FOR BACKEND -------------------------------------------------------------


module.exports = router;
