'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var jobsController = require('../controllers/jobsController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));


//FOR MOBILE APPS ANDROID -------------------------------------------------
//get Meta Notifications By User
router.route('/getJobsByCountry/')
  .get(authController.isAuthenticated, jobsController.getJobsByCountry);

router.route('/getJobsByCountry/job/:job')
  .get(authController.isAuthenticated, jobsController.getJobsByCountry_Name);


//END FOR MOBILE APPS ANDROID ----------------------------------------------


//FOR BACK END -------------------------------------------------------------

//add Jobs
router.route('/addJob').post(jobsController.addJob);

//END FOR BACK END --------------------------------------------------------
module.exports = router;
