'use strict';

var models = require('express-cassandra');
const express = require('express')
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var feedingsController = require('../controllers/feedingsController')(models);
var cyclesController = require('../controllers/cyclesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID -------------------------------------------------

router.route('/getFeedingsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/:page_state_feeding?')
  .get(authController.isAuthenticated, feedingsController.getFeedingsByCycle);

router.route('/getFeedingsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/feeding_time_begin/:feeding_time_begin/:page_state_feeding?')
  .get(authController.isAuthenticated, feedingsController.getFeedingsByCycle_TimeBegin);

router.route('/getFeedingsByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/feeding_time_begin/:feeding_time_begin/feeding_time_end/:feeding_time_end/:page_state_feeding?')
  .get(authController.isAuthenticated, feedingsController.getFeedingsByCycle_TimeBeginEnd);

router.route('/addFeeding').post(authController.isAuthenticated, feedingsController.addFeeding, cyclesController.updateCycle);


//END FOR MOBILE APPS ANDROID -------------------------------------------------




//FOR BACKEND -----------------------------------------------------------------

router.route('/getFeedingsByCluster/cluster_id/:cluster_id/year_month/:year_month/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/:page_state_meta_feeding?')
  .get(feedingsController.getFeedingsByCluster_TimeBeginEnd_MetaMerged);


router.route('/getFeedingsByCluster/cluster_id/:cluster_id/year_month/:year_month/cycle_time_begin/:cycle_time_begin/cycle_time_end/:cycle_time_end/feed/:feed/:page_state_meta_feeding?')
  .get(feedingsController.getFeedingsByCluster_TimeBeginEnd_Feed_MetaMerged);

//END FOR BACKEND -------------------------------------------------------------

module.exports = router;
