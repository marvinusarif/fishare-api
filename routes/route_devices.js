var models = require('express-cassandra');
const express = require('express')
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var devicesController = require('../controllers/devicesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));



//FOR MOBILE APPS ANDROID -------------------------------------------------
router.route('/getDevicesByProduction/date_production/:date_production/device_id/:device_id')
  .get(authController.isAuthenticated, devicesController.findDevicesById);

router.route('/updateDevice')
  .put(authController.isAuthenticated, devicesController.updateDevice);
//END FOR MOBILE APPS ANDROID ---------------------------------------------


//FOR BACKEND -----------------------------------------------------------------

router.route('/getDevicesByProduction/date_production/:date_production/:page_state_device?')
  .get(devicesController.findDevicesByProductionDate);

router.route('/addDevice')
  .post(devicesController.addDevice);

//END FOR BACKEND -------------------------------------------------------------


module.exports = router;
