'use strict';

var path = require('path');
var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var memoryStorage = multer.memoryStorage();

var authController = require('../controllers/authController')(models);
var samplesController = require('../controllers/samplesController')(models);
var cyclesController = require('../controllers/cyclesController')(models);
var filesController = require('../controllers/filesController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

var multer_config = {
  storage: memoryStorage,
  limit: {
    fileSize: 1024 * 1024
  },
  fileFilter: function(req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.jpg' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'))
    }
    callback(null, true);
  }
}


//FOR MOBILE APPS ANDROID -------------------------------------------------
//get Sample By Cycle
router.route('/getSamplesByCycle/cycle_time_begin/:cycle_time_begin/cycle_id/:cycle_id/type/:type/:page_state_sample?')
  .get(authController.isAuthenticated, samplesController.getSamplesByCycle);

//get Weight by Smart Sampling
router.route('/getWeight')
  .post(authController.isAuthenticated, samplesController.getWeightBySpecies);

//add Sample
router.route('/addSample').post(authController.isAuthenticated, multer(multer_config).array('samples_img'), filesController.uploadSampleImage, samplesController.addSample, cyclesController.updateCycle);

//END FOR MOBILE APPS ANDROID ----------------------------------------------


//FOR BACK END -------------------------------------------------------------

//get Sample By Species
router.route('/getSamplesBySpecies/type/:type/species/:species/producer/:producer/date_taken_begin/:date_taken_begin/date_taken_end/:date_taken_end/:page_state_meta_sample?').get(samplesController.getSamplesBySpecies_Producer_TimeBeginEnd_MetaMerged);


//END FOR BACK END --------------------------------------------------------
module.exports = router;
