'use strict';

var models = require('express-cassandra');
const express = require('express');
var bodyParser = require('body-parser');

var authController = require('../controllers/authController')(models);
var feedingProgramsController = require('../controllers/feedingProgramsController')(models);

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

//FOR MOBILE APPS ANDROID --------------------------------------------------

//get feeding programs
router.route('/getFeedingPrograms')
  .post(authController.isAuthenticated, feedingProgramsController.getFeedingProgramsByCycle);


//END FOR MOBILE APPS ANDROID ----------------------------------------------




//FOR BACK END ------------------------------------------------------------


//END FOR BACK END --------------------------------------------------------

module.exports = router;
