'use strict';

var moment = require('moment');
var harvestsController = function(models) {
  var getHarvestsByCycle = function(req, res) {
    /*************** GET HARVESTS BY CYCLE ***************/
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var harvests = [];
      var _callback = {
        page_state_harvest: req.params.page_state_harvest
      }
      var query_harvestsByCycle = {
        cycle_time_begin: req.params.cycle_time_begin,
        cycle_id: models.uuidFromString(req.params.cycle_id),
        type: req.params.type
      }
      var options_harvestsByCycle = {
        select: ['event_time', 'species', 'weight', 'weight_avg', 'quantity'],
        raw: true,
        fetchSize: 15
      }
      if (typeof req.params.page_state_harvest !== "undefined") {
        options_harvestsByCycle.pageState = _callback.page_state_harvest;
      }
      models.instance.harvestsByCycle.eachRow(query_harvestsByCycle, options_harvestsByCycle, function(n, row) {
        harvests.push(row);

      }, function(err, harvest) {
        if (err) throw err;
        if (harvest) {
          if (harvests.length > 0) {
            _callback.page_state_harvest = harvest.pageState;
            _callback.harvests = harvests;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getHarvestsByCycle',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getHarvestsByCycle",
        message: "Unauthorized"
      })
    }

  };
  /*************** UPDATE HARVEST ***************/

  var updateHarvest = function(req, res, next) {

    if ((req.body.cycle_time_begin) && (req.body.cycle_id) && (req.body.event_time) && (req.body.type) && (req.body.species) && (req.user.cluster_id) && (req.user.id) && (req.body.harvest_date)) {

      var query_harvestsByCluster = {
        harvest_date: moment(req.body.harvest_date).format('YYYY-MM-DD'),
        cluster_id: req.user.cluster_id,
        type: req.body.type,
        species: req.body.species,
        event_time: moment(req.body.event_time).format('YYYY-MM-DD HH:mm:ss')
      }
      var query_harvestsByUser = {
        harvest_date: query_harvestsByCluster.harvest_date,
        user_id: req.user.id,
        type: query_harvestsByCluster.type,
        species: query_harvestsByCluster.species,
        event_time: query_harvestsByCluster.event_time
      }
      var query_harvestsByCycle = {
        cycle_time_begin: req.body.cycle_time_begin,
        cycle_id: models.uuidFromString(req.body.cycle_id),
        type: query_harvestsByCluster.type,
        event_time: query_harvestsByCluster.event_time
      }
      var value_harvestsByCluster = {
        weight: req.body.weight,
        weight_avg: req.body.weight_avg,
        quantity: req.body.weight / req.body.weight_avg * 1000
      }
      var value_harvestsByUser = {
        weight: req.body.weight,
        weight_avg: req.body.weight_avg,
        quantity: req.body.weight / req.body.weight_avg * 1000
      }
      var value_harvestsByCycle = {
        species: query_harvestsByCluster.species,
        weight: req.body.weight,
        weight_avg: req.body.weight_avg,
        quantity: req.body.weight / req.body.weight_avg * 1000
      }

      var update_harvest = function() {
        var queries = [];
        return new Promise(function(resolve, reject) {
          var harvestsByCluster = models.instance.harvestsByCluster.update(query_harvestsByCluster, value_harvestsByCluster, {
            return_query: true
          });
          queries.push(harvestsByCluster);

          var harvestsByUser = models.instance.harvestsByUser.update(query_harvestsByUser, value_harvestsByUser, {
            return_query: true
          });
          queries.push(harvestsByUser);

          var harvestsByCycle = models.instance.harvestsByCycle.update(query_harvestsByCycle, value_harvestsByCycle, {
            return_query: true
          });
          queries.push(harvestsByCycle);

          models.doBatch(queries, function(err) {
            (err) ? reject(err): resolve();
          });
        })
      }

      update_harvest()
        .then(function() {
          next();
        }).
      catch(function(error) {
        console.error(error);
      });
    } else {
      res.status(401).json({
        method: "updateHarvest",
        message: "Unauthorized"
      })
    }
  };
  var addHarvest = function(req, res, next) {
    if (req.user.id && req.user.cluster_id && req.user.join_date) {
      var value_harvestsByCluster_Meta = {
        cluster_id: req.user.cluster_id,
        harvest_date: moment().format('YYYY-MM-DD')
      }
      var value_harvestsByCluster = {
        harvest_date: value_harvestsByCluster_Meta.harvest_date,
        cluster_id: value_harvestsByCluster_Meta.cluster_id,
        event_time: moment().format('YYYY-MM-DD HH:mm:ss'),
        type: req.body.type,
        species: req.body.species,
        weight: req.body.weight,
        weight_avg: req.body.weight_avg,
        quantity: req.body.weight / req.body.weight_avg * 1000
      }
      var value_harvestsByUser_Meta = {
        user_id: req.user.id,
        harvest_date: value_harvestsByCluster_Meta.harvest_date
      }
      var value_harvestsByUser = {
        harvest_date: value_harvestsByCluster_Meta.harvest_date,
        user_id: value_harvestsByUser_Meta.user_id,
        event_time: value_harvestsByCluster.event_time,
        type: value_harvestsByCluster.type,
        species: value_harvestsByCluster.species,
        weight: value_harvestsByCluster.weight,
        weight_avg: value_harvestsByCluster.weight_avg,
        quantity: value_harvestsByCluster.quantity
      }
      var value_harvestsByCycle = {
        cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
        cycle_id: models.uuidFromString(req.body.cycle_id),
        event_time: value_harvestsByCluster.event_time,
        type: value_harvestsByCluster.type,
        species: value_harvestsByCluster.species,
        weight: value_harvestsByCluster.weight,
        weight_avg: value_harvestsByCluster.weight_avg,
        quantity: value_harvestsByCluster.quantity
      }
      var save_harvest = function() {
        return new Promise(function(resolve, reject) {
          var queries = [];

          var save_harvestsByCluster_Meta = new models.instance.harvestsByCluster_Meta(value_harvestsByCluster_Meta).save({
            return_query: true
          });
          var save_harvestsByUser_Meta = new models.instance.harvestsByUser_Meta(value_harvestsByUser_Meta).save({
            return_query: true
          });
          queries.push(save_harvestsByCluster_Meta, save_harvestsByUser_Meta);

          var save_harvestsByCluster = new models.instance.harvestsByCluster(value_harvestsByCluster).save({
            return_query: true
          });
          var save_harvestsByCycle = new models.instance.harvestsByCycle(value_harvestsByCycle).save({
            return_query: true
          });
          var save_harvestsByUser = new models.instance.harvestsByUser(value_harvestsByUser).save({
            return_query: true
          });
          queries.push(save_harvestsByCluster, save_harvestsByUser, save_harvestsByCycle);
          //pass parameter to the cycle for callback
          req.body.harvest_date = value_harvestsByCluster_Meta.harvest_date;
          req.body.harvest_event_time = value_harvestsByCluster.event_time;
          models.doBatch(queries, function(err) {
            (err) ? reject(err): resolve();
          });
        });
      }

      save_harvest()
        .then(function() {
          next();
        })
        .catch(function(error) {
          console.error(error);
        })
    } else {
      res.status(401).json({
        method: "addHarvest",
        message: "Unauthorized"
      })
    }
  };
  var getAllHarvestByCycle = function(req, res, next) {
    if (req.user.id && req.user.cluster_id && req.user.join_date) {
      //normalize
      req.body.id = req.body.cycle_id;

      var promise = new Promise(function(resolve, reject) {
        var query_harvestsByCycle = {
          cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
          cycle_id: models.uuidFromString(req.body.cycle_id),
          type: req.body.type
        }
        var options_harvestsByCycle = {
          select: ['species', 'weight', 'quantity']
        }
        req.body.harvest = {};
        req.body.harvest[req.body.type.toLowerCase()] = {
          species: req.body.species,
          weight: 0,
          quantity: 0
        };
        models.instance.harvestsByCycle.eachRow(query_harvestsByCycle, options_harvestsByCycle, function(n, row) {
          req.body.harvest[req.body.type.toLowerCase()].weight += row.weight;
          req.body.harvest[req.body.type.toLowerCase()].quantity += row.quantity;
        }, function(err, rec) {
          if (err) reject(err);
          if (rec) {
            resolve();
          }
        });
      });
      promise.then(function() {
        next();
      }, function(error) {
        console.error(error);
      })
    } else {
      res.status(401).json({
        method: "getAllHarvestByCycle",
        message: "Unauthorized"
      })
    }
  }
  /********* GET HARVEST BY USER TIME BEGIN-END META-MERGED ********/

  var getHarvestsByUser_TimeBeginEnd_MetaMerged = function(req, res) {


    var _callback = {
      page_state_meta_harvest: req.params.page_state_meta_harvest
    }
    var query_harvestsByUser_Meta = {
      user_id: models.uuidFromString(req.params.user_id),
      harvest_date: {
        '$gte': req.params.harvest_date_begin,
        '$lte': req.params.harvest_date_end
      }
    }

    var options_harvestsByUser_Meta = {
      select: ['harvest_date'],
      raw: true,
      fetchSize: 5
    }
    if (typeof req.params.page_state_meta_harvest !== "undefined") {
      options_harvestsByUser_Meta.pageState = _callback.page_state_meta_harvest;
    }
    var send_json = function(response) {
      if (typeof response.meta_harvests !== "undefined") {
        if (response.meta_harvests.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({
          method: 'getHarvestsByUser',
          message: 'invalid params'
        });
      }
    }
    var get_meta_harvests = function(callback) {
      var meta_harvests = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.harvestsByUser_Meta.eachRow(query_harvestsByUser_Meta, options_harvestsByUser_Meta, function(n, row) {
          meta_harvests.push(row);
        }, function(err, meta_harvest) {
          if (err) reject(err);
          if (meta_harvest) {
            _callback.page_state_meta_harvest = meta_harvest.pageState;
            _callback.meta_harvests = meta_harvests;
            _callback.harvests = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        if (response.meta_harvests.length > 0) {
          get_harvests(response);
        } else {
          res.status(404).send("");
        }
      }, function(error) {
        console.error(error);
      });
    }
    var get_harvests = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_harvests.forEach(function(meta_harvest) {
          var query_harvestsByUser = {
            user_id: models.uuidFromString(req.params.user_id),
            harvest_date: meta_harvest.harvest_date
          }
          var options_harvestsByUser = {
            select: ['event_time', 'type', 'species', 'weight', 'weight_avg', 'quantity'],
            raw: true
          }
          models.instance.harvestsByUser.eachRow(query_harvestsByUser, options_harvestsByUser, function(n, row) {
            _callback.harvests.push(row);
          }, function(err, harvest) {
            if (err) reject(err);
            if (harvest) {
              i++;
              if (i === _callback.meta_harvests.length) {
                resolve(_callback);
              }
            }
          });
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      })
    }

  };
  /***** GET HARVEST BY CLUSTER TIME BEGIN-END META-MERGED *****/

  var getHarvestsByCluster_TimeBeginEnd_MetaMerged = function(req, res) {


    var _callback = {
      page_state_meta_harvest: req.params.page_state_meta_harvest
    }
    var query_harvestsByCluster_Meta = {
      cluster_id: models.uuidFromString(req.params.cluster_id),
      harvest_date: {
        '$gte': req.params.harvest_date_begin,
        '$lte': req.params.harvest_date_end
      }
    }

    var options_harvestsByCluster_Meta = {
      select: ['harvest_date'],
      raw: true,
      fetchSize: 5
    }
    if (typeof req.params.page_state_meta_harvest !== "undefined") {
      options_harvestsByCluster_Meta.pageState = _callback.page_state_meta_harvest;
    }
    var send_json = function(response) {
      if (typeof response.meta_harvests !== "undefined") {
        if (response.meta_harvests.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({
          method: 'getHarvestsByCluster',
          message: 'invalid params'
        });
      }
    }
    var get_meta_harvests = function(callback) {
      var meta_harvests = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.harvestsByCluster_Meta.eachRow(query_harvestsByCluster_Meta, options_harvestsByCluster_Meta, function(n, row) {
          meta_harvests.push(row);
        }, function(err, meta_harvest) {
          if (err) reject(err);
          if (meta_harvest) {
            _callback.page_state_meta_harvest = meta_harvest.pageState;
            _callback.meta_harvests = meta_harvests;
            _callback.harvests = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        if (response.meta_harvests.length > 0) {
          get_harvests(response);
        } else {
          res.status(404).send("");
        }
      }, function(error) {
        console.error(error);
      });
    }
    var get_harvests = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_harvests.forEach(function(meta_harvest) {
          var query_harvestsByCluster = {
            cluster_id: models.uuidFromString(req.params.cluster_id),
            harvest_date: req.params.harvest_date
          }
          var options_harvestsByCluster = {
            select: ['event_time', 'type', 'species', 'weight', 'weight_avg', 'quantity'],
            raw: true
          }
          models.instance.harvestsByCluster.eachRow(query_harvestsByCluster, options_harvestsByCluster, function(n, row) {
            _callback.harvests.push(row);
          }, function(err, harvest) {
            if (err) reject(err);
            if (harvest) {
              i++;
              if (i === _callback.meta_harvests.length) {
                resolve(_callback);
              }
            }
          });
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      })
    }

  };
  /***** RETURN HARVEST *****/

  return {
    //android
    getHarvestsByCycle: getHarvestsByCycle,
    addHarvest: addHarvest,
    updateHarvest: updateHarvest,
    getAllHarvestByCycle: getAllHarvestByCycle,
    //end android

    //backend
    getHarvestsByUser_TimeBeginEnd_MetaMerged: getHarvestsByUser_TimeBeginEnd_MetaMerged,
    getHarvestsByCluster_TimeBeginEnd_MetaMerged: getHarvestsByCluster_TimeBeginEnd_MetaMerged

  }
}

module.exports = harvestsController;
