'use strict';

var moment = require('moment');
var cyclesController = function(models) {

  /*********** GET META CYCLES BY USER TIME END-BEGIN ************/
  function isEmpty(obj) {
    return !Object.keys(obj).length;
  }

  var findMetaCyclesByUser_TimeBeginEnd = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {

      var meta_cycles = [];

      var _callback = {
        page_state_meta_cycle: req.params.page_state_meta_cycle
      }

      var query_cyclesByUser_Meta = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        user_id: req.user.id,
        cycle_time_begin: {
          '$gte': req.params.cycle_time_begin,
          '$lte': req.params.cycle_time_end
        }
      }

      var options_cyclesByUser_Meta = {
        select: ['cycle_time_begin'],
        raw: true,
        fetchSize: 3
      }

      if (typeof _callback.page_state_meta_cycle !== "undefined") {
        options_cyclesByUser_Meta.pageState = _callback.page_state_meta_cycle
      }

      models.instance.cyclesByUser_Meta.eachRow(query_cyclesByUser_Meta, options_cyclesByUser_Meta, function(n, row) {
        meta_cycles.push(row);
      }, function(err, meta_cycle) {
        if (err)
          throw err;
        if (meta_cycle) {
          if (meta_cycles.length > 0) {
            _callback.cycle_time_begin = meta_cycles;
            _callback.page_state_meta_cycle = meta_cycle.pageState;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({method: 'getMetaCyclesByUser', message: 'invalid params'});
        }
      })
    } else {
      res.status(401).json({method: "getProfile", message: "Unauthorized"})
    }
  };
  /************* GET CYCLES BY USER TIME BEGIN **************/

  var findCyclesByUser_TimeBegin = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var cycles = [];
      var _callback = {
        page_state_cycle: req.params.page_state_cycle
      }

      var query_cyclesByUser = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        user_id: req.user.id,
        cycle_time_begin: req.params.cycle_time_begin
      }

      var option_cyclesByUser = {
        raw: true,
        fetchSize: 3
      }

      if (typeof _callback.page_state_cycle !== "undefined") {
        option_cyclesByUser.pageState = _callback.page_state_cycle
      }

      models.instance.cyclesByUser.eachRow(query_cyclesByUser, option_cyclesByUser, function(n, row) {
        cycles.push(row);
      }, function(err, cycle) {
        if (err)
          throw err;
        if (cycle) {
          if (cycles.length > 0) {
            _callback.cycles = cycles;
            _callback.page_state_cycle = cycle.pageState;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({method: 'getCyclesByUser', message: 'invalid params'});
        }
      });
    } else {
      res.status(401).json({method: "getCyclesByUser", message: "Unauthorized"})
    }
  };
  /************* GET CYCLES BY USER CYCLE_ID **************/

  var findCyclesByUser_cycleId = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var query_CycleById = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        cycle_time_begin: req.params.cycle_time_begin,
        user_id: req.user.id,
        id: models.uuidFromString(req.params.cycle_id)
      }
      models.instance.cyclesByUser.find(query_CycleById, function(err, rec) {
        if (err)
          throw err;
        if (rec) {
          res.status(200).json(rec);
        } else {
          res.status(404).json({method: 'getCyclesById', message: 'invalid params'});
        }
      });
    } else {
      res.status(401).json({method: "getCyclesByUser", message: "Unauthorized"})
    }
  }
  /*********** GET CYCLES BY USER TIME END-BEGIN NAME **********/

  var findCyclesByUser_TimeBeginEnd_Name_MetaMerged = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var _callback = {
        page_state_meta_cycle: req.params.page_state_meta_cycle
      }

      var send_json = function(response) {
        if (typeof response.meta_cycles !== "undefined") {
          if (response.meta_cycles.length > 0) {
            res.status(200).json(response);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({method: 'getCyclesByUser', message: 'invalid params'});
        }
      }
      var query_CycleByName_Meta = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        user_id: req.user.id,
        cycle_time_begin: {
          '$lte': req.params.cycle_time_end,
          '$gte': req.params.cycle_time_begin
        }
      }
      var options_CycleByName_Meta = {
        select: ['cycle_time_begin']
      }

      if (typeof req.params.page_state_meta_cycle !== undefined) {
        options_CycleByName_Meta.pageState = req.params.page_state_meta_cycle
      }

      var get_meta_cycles = function(callback) {
        var meta_cycles = [];
        var promise = new Promise(function(resolve, reject) {
          models.instance.cyclesByUser_Meta.eachRow(query_CycleByName_Meta, options_CycleByName_Meta, function(n, row) {
            meta_cycles.push(row);
          }, function(err, meta_cycle) {
            if (err)
              reject(err);
            if (meta_cycle) {
              _callback.page_state_meta_cycle = meta_cycle.pageState;
              _callback.meta_cycles = meta_cycles;
              _callback.cycles = [];
              resolve(_callback);
            }
          });
        })
        promise.then(function(response) {
          if (response.meta_cycles.length > 0) {
            callback(response);
          } else {
            res.status(404).send("");
          }

        }, function(error) {
          console.error(error);
        });

      }
      var get_cycles = function(_callback) {
        var promise = new Promise(function(resolve, reject) {
          var i = 0;
          _callback.meta_cycles.forEach(function(metaCycle) {
            var query_CycleByName = {
              user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
              cycle_time_begin: metaCycle.cycle_time_begin,
              user_id: req.user.id,
              name: {
                '$like': req.params.name.toUpperCase() + '%'
              }
            }

            var options_CycleByName = {
              raw: 'true'
            }

            models.instance.cyclesByUser.eachRow(query_CycleByName, options_CycleByName, function(n, row) {
              _callback.cycles.push(row);
            }, function(err, cycle) {
              if (err)
                reject(err);
              if (cycle) {
                i++;
                if (i === _callback.meta_cycles.length) {
                  resolve(_callback);
                }
              }
            })
          });
        });
        promise.then(function(response) {
          send_json(response);
        }, function(error) {
          console.error(error);
        })

      }
      get_meta_cycles(get_cycles);
    } else {
      res.status(401).json({method: "getCyclesByUser", message: "Unauthorized"})
    }
  };
  var closeCycle = function(req, res, next) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {

      var log = function(b, n) {
        return Math.log(n) / Math.log(b);
      }
      //normalize
      req.body.id = req.body.cycle_id;

      if ((req.body.id) && (req.body.cycle_time_begin) && (req.body.fish_species) && (req.body.crop_species)) {
        var query_usersByCycle = {
          user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
          user_id: req.user.id,
          cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
          id: models.uuidFromString(req.body.cycle_id)
        }
        var options_usersByCycle = {
          select: [
            'time_begin',
            'time_end',
            'fish',
            'harvest',
            'pond',
            'feed'
          ]
        }
        models.instance.cyclesByUser.findOneAsync(query_usersByCycle, options_usersByCycle).then(function(rec) {
          if (rec) {
            var quantity_total = 0;
            var protein_total = 0;
            var summing = function() {
              return new Promise(function(resolve, reject) {
                if (Object.keys(rec.feed).length > 0) {
                  var i = 0;
                  for (var prop in rec.feed) {
                    protein_total += prop.protein_total;
                    quantity_total += prop.quantity_total;
                    i++;
                    if (i === Object.keys(rec.feed).length) {
                      resolve();
                    }
                  }
                } else {
                  reject('feed is empty');
                }
              });
            }

            var calculate_ratio = function() {
              return new Promise(function(resolve, reject) {
                var time_begin = moment(rec.time_begin).format('YYYY-MM-DD HH:mm:ss');
                var time_end = moment().format('YYYY-MM-DD HH:mm:ss');

                var object_test = Object.create(rec.harvest);
                if (Object.prototype.hasOwnProperty.call(object_test, 'fish')) {
                  var fish_final_weight = rec.harvest.fish.weight - rec.fish.weight;
                  var fcr = quantity_total / 1000 / fish_final_weight;
                  var fce = 1 / fcr;
                  var doc = time_end.diff(time_begin, 'days');
                  if (doc > 0) {
                    var adg = ((rec.harvest.fish.weight * 1000 / rec.harvest.fish.quantity) - (rec.fish.weight * 1000 / rec.fish.qty)) / doc;
                    var ln_finalweight = log(2, (rec.harvest.fish.weight * 1000 / rec.harvest.fish.quantity));
                    var ln_initialweight = log(2, (rec.fish.weight * 1000 / rec.fish.qty));
                    var sgr = (ln_finalweight - ln_initialweight) / doc;
                  } else {
                    var adg = 0;
                    var sgr = 0;
                  }
                  var sr = rec.harvest.fish.quantity / rec.fish.qty * 100;
                  var density = (rec.harvest.fish.quantity / rec.pond.volume);
                  if (protein_total > 0) {
                    var per = fish_final_weight / protein_total / 1000;
                  } else {
                    var per = 0;
                  }
                  req.body.ratio = {
                    fcr: fcr,
                    fce: fce,
                    adg: adg,
                    sr: sr,
                    sgr: sgr,
                    per: per,
                    density: (rec.harvest.fish.quantity / rec.pond.volume)
                  }
                } else {

                  req.body.ratio = {
                    fcr: 0,
                    fce: 0,
                    adg: 0,
                    sr: 0,
                    sgr: 0,
                    per: 0,
                    density: (rec.fish.qty / rec.pond.volume)
                  }
                }

                if (typeof req.body.ratio !== "undefined") {
                  //normalize
                  req.body.time_end = time_end;
                  resolve();
                } else {
                  reject('ratio is invalid');
                }
              });
            }

            summing().then(calculate_ratio).then(function() {
              next();
            }).catch(function(error) {
              console.error(error);
            });
          }

        });
      }
    }
  }
  /*********** UPDATE CYCLES **********/

  var updateCycle = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      if ((req.body.id) && (req.body.cycle_time_begin) && (req.body.fish_species) && (req.body.crop_species)) {

        var query_cyclesBySpecies = {
          cycle_time_begin: req.body.cycle_time_begin,
          fish_species: req.body.fish_species,
          crop_species: req.body.crop_species,
          cluster_id: req.user.cluster_id,
          id: models.uuidFromString(req.body.id)
        }
        var value_cyclesBySpecies = {}

        var query_cyclesByCluster = {
          cycle_time_begin: query_cyclesBySpecies.cycle_time_begin,
          cluster_id: req.user.cluster_id,
          user_id: req.user.id,
          id: query_cyclesBySpecies.id
        }
        var value_cyclesByCluster = {}

        var query_cyclesByUser = {
          user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
          cycle_time_begin: query_cyclesBySpecies.cycle_time_begin,
          user_id: query_cyclesByCluster.user_id,
          id: query_cyclesBySpecies.id
        }
        var value_cyclesByUser = {}

        var _callback = {
          cycle_time_begin: query_cyclesBySpecies.cycle_time_begin,
          user_id: query_cyclesByCluster.user_id,
          cluster_id: query_cyclesByCluster.cluster_id,
          id: query_cyclesBySpecies.id,
          fish_species: query_cyclesBySpecies.fish_species,
          crop_species: query_cyclesBySpecies.crop_species
        }

        if (typeof req.body.name !== "undefined") {
          value_cyclesByCluster.name = req.body.name.toUpperCase();
          value_cyclesByUser.name = value_cyclesByCluster.name;
          value_cyclesBySpecies.name = value_cyclesByCluster.name;
          _callback.name = value_cyclesByCluster.name;
        }

        //this will be processed after close cycle
        if (typeof req.body.time_end !== "undefined") {
          value_cyclesByCluster.time_end = req.body.time_end;
          value_cyclesBySpecies.time_end = value_cyclesByCluster.time_end;
          value_cyclesByUser.time_end = value_cyclesByCluster.time_end;
          _callback.time_end = value_cyclesByCluster.time_end;
        }

        //this will be processed after close cycles
        if (typeof req.body.ratio !== "undefined") {
          value_cyclesByCluster.ratio = req.body.ratio;
          value_cyclesBySpecies.ratio = req.body.ratio;
          value_cyclesByUser.ratio = req.body.ratio;
          _callback.ratio = req.body.ratio;
        }

        //this will be processed after add samples
        if (typeof req.body.sample !== "undefined") {
          value_cyclesByCluster.sample = {
            '$add': req.body.sample
          };
          value_cyclesBySpecies.sample = {
            '$add': req.body.sample
          };
          value_cyclesByUser.sample = {
            '$add': req.body.sample
          };
          _callback.sample_date_taken = req.body.date_taken;
          _callback.sample = req.body.sample;
        }

        //this will be processed after add/update harvest
        if (typeof req.body.harvest !== "undefined") {
          value_cyclesByCluster.harvest = {
            '$add': req.body.harvest
          };
          value_cyclesBySpecies.harvest = {
            '$add': req.body.harvest
          };
          value_cyclesByUser.harvest = {
            '$add': req.body.harvest
          };
          _callback.harvest_event_time = req.body.harvest_event_time;
          _callback.harvest_date = req.body.harvest_date;
          _callback.harvest = req.body.harvest;
        }

        //this will be processed after get feeding program
        if (typeof req.body.feeding_program !== "undefined") {
          value_cyclesByUser.feeding_program = {
            event_time: moment().format('YYYY-MM-DD'),
            quantity: req.body.feeding_program.quantity,
            frequency: req.body.feeding_program.frequency
          };
          _callback.feeding_program = value_cyclesByUser.feeding_program;
        }

        //this will be processed after add feed to cycles
        if (typeof req.body.feed !== "undefined") {
          value_cyclesByCluster.feed = {
            '$add': req.body.feed
          };
          value_cyclesByUser.feed = {
            '$add': req.body.feed
          };
          if (typeof req.body.feeding_id !== "undefined") {
            _callback.feeding_id = req.body.feeding_id;
          }
          _callback.feed = req.body.feed;
        }

        //this will be processed after add measurement
        if (typeof req.body.measurement !== "undefined") {
          value_cyclesByUser.measurement = req.body.measurement;
          value_cyclesByCluster.measurement = req.body.measurement;
          _callback.measurement = req.body.measurement;
        }

        //this will be processed after upload
        if (typeof req.body.img_url !== "undefined") {
          value_cyclesByUser.img_url = req.body.img_url;
          _callback.img_url = value_cyclesByUser.img_url;
        }

        var update_cycle = function() {
          return new Promise(function(resolve, reject) {
            var queries = [];
            if (!isEmpty(value_cyclesBySpecies)) {
              var update_cyclesBySpecies = models.instance.cyclesBySpecies.update(query_cyclesBySpecies, value_cyclesBySpecies, {return_query: true});
              queries.push(update_cyclesBySpecies);
            }
            if (!isEmpty(value_cyclesByCluster)) {
              var update_cyclesByCluster = models.instance.cyclesByCluster.update(query_cyclesByCluster, value_cyclesByCluster, {return_query: true});
              queries.push(update_cyclesByCluster);
            }
            if (!isEmpty(value_cyclesByUser)) {
              var update_cyclesByUser = models.instance.cyclesByUser.update(query_cyclesByUser, value_cyclesByUser, {return_query: true});
              queries.push(update_cyclesByUser);
            }

            models.doBatch(queries, function(err) {
              (err)
                ? reject(err)
                : resolve(_callback);
            })
          });
        }

        update_cycle().then(function(response) {
          res.status(200).json(response);
        }).catch(function(error) {
          console.error(error);
        });

      }
    } else {
      res.status(401).json({method: "updateCycle", message: "Unauthorized"});
    }
  };
  /*********** ADD CYCLES **********/

  var addCycle = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var value_cyclesBySpecies_Meta = {
        fish_species: req.body.fish.species,
        crop_species: req.body.crop.species,
        cycle_time_begin: moment(req.body.time_begin).format('YYYY-MM-DD')
      }

      var value_cyclesBySpecies = {
        cycle_time_begin: value_cyclesBySpecies_Meta.cycle_time_begin,
        fish_species: value_cyclesBySpecies_Meta.fish_species,
        crop_species: value_cyclesBySpecies_Meta.crop_species,
        cluster_id: req.user.cluster_id,
        id: models.timeuuid(),
        name: req.body.name.toUpperCase(),
        time_begin: moment(req.body.time_begin).format('YYYY-MM-DD HH:mm:ss'),
        time_end: models.datatypes.unset,
        pond_min: {
          volume: req.body.pond.volume,
          aeration: req.body.pond.aeration,
          recirculation: req.body.pond.recirculation
        },
        sample: models.datatypes.unset,
        harvest: models.datatypes.unset,
        ratio: {
          density: (req.body.fish.weight / req.body.pond.volume)
        }
      }

      var value_cyclesByCluster_Meta = {
        cluster_id: value_cyclesBySpecies.cluster_id,
        cycle_time_begin: moment(req.body.time_begin).format('YYYY-MM-DD')
      }

      var value_cyclesByCluster = {
        cycle_time_begin: value_cyclesBySpecies.cycle_time_begin,
        user_id: req.user.id,
        time_begin: value_cyclesBySpecies.time_begin,
        cluster_id: value_cyclesBySpecies.cluster_id,
        name: value_cyclesBySpecies.name,
        id: value_cyclesBySpecies.id,
        fish: req.body.fish,
        crop: req.body.crop,
        pond: req.body.pond,
        sample: value_cyclesBySpecies.sample,
        measurement: models.datatypes.unset,
        harvest: value_cyclesBySpecies.harvest, //null
        ratio: value_cyclesBySpecies.ratio,
        feed: req.body.feed
      }

      var value_cyclesByUser_Meta = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        user_id: value_cyclesByCluster.user_id,
        cycle_time_begin: moment(req.body.time_begin).format('YYYY-MM-DD')
      }
      var value_cyclesByUser = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        cycle_time_begin: value_cyclesBySpecies.cycle_time_begin,
        user_id: value_cyclesByCluster.user_id,
        time_begin: value_cyclesBySpecies.time_begin,
        cluster_id: value_cyclesBySpecies.cluster_id,
        name: value_cyclesBySpecies.name,
        id: value_cyclesBySpecies.id,
        fish: value_cyclesByCluster.fish,
        crop: value_cyclesByCluster.crop,
        pond: value_cyclesByCluster.pond,
        sample: value_cyclesBySpecies.sample,
        harvest: models.datatypes.unset, //null
        ratio: value_cyclesBySpecies.ratio,
        feed: value_cyclesByCluster.feed,
        measurement: models.datatypes.unset,
        feeding_program: {
          event_time: moment(req.body.time_begin).format('YYYY-MM-DD HH:mm:ss'),
          quantity: -1, //it means unset
          frequency: req.body.feeding_program.frequency
        },
        img_url: req.body.img_url
      }

      var save_cycle = function() {
        return new Promise(function(resolve, reject) {
          var queries = [];
          // push meta cycle
          var save_cyclesBySpecies_Meta = new models.instance.cyclesBySpecies_Meta(value_cyclesBySpecies_Meta).save({return_query: true});
          var save_cyclesByCluster_Meta = new models.instance.cyclesByCluster_Meta(value_cyclesByCluster_Meta).save({return_query: true});
          var save_cyclesByUser_Meta = new models.instance.cyclesByUser_Meta(value_cyclesByUser_Meta).save({return_query: true});
          queries.push(save_cyclesBySpecies_Meta, save_cyclesByCluster_Meta, save_cyclesByUser_Meta);

          //push cycle
          var save_cyclesBySpecies = new models.instance.cyclesBySpecies(value_cyclesBySpecies).save({return_query: true});
          var save_cyclesByCluster = new models.instance.cyclesByCluster(value_cyclesByCluster).save({return_query: true});
          var save_cyclesByUser = new models.instance.cyclesByUser(value_cyclesByUser).save({return_query: true});
          queries.push(save_cyclesBySpecies, save_cyclesByCluster, save_cyclesByUser);

          var _callback = {
            cycle_time_begin: value_cyclesByUser.cycle_time_begin,
            user_id: value_cyclesByUser.user_id,
            cluster_id: value_cyclesByUser.cluster_id,
            id: value_cyclesByUser.id,
            name: value_cyclesByUser.name,
            fish_species: value_cyclesBySpecies.fish_species,
            crop_species: value_cyclesBySpecies.crop_species
          }

          models.doBatch(queries, function(err) {
            (err)
              ? reject(err)
              : resolve(_callback);
          });
        });
      }

      save_cycle().then(function(response) {
        res.status(200).json(response);
      }).catch(function(err) {
        console.error(err);
      })

    } else {
      res.status(401).json({method: "addCycle", message: "Unauthorized"})
    }
  };
  /*********** BACK END **********/

  var findMetaCyclesByCluster_TimeBeginEnd = function(req, res) {
    var _callback = {
      page_state_meta_cycle: req.params.page_state_meta_cycle
    }
    var query_cyclesByCluster_Meta = {
      cluster_id: models.uuidFromString(req.params.cluster_id),
      cycle_time_begin: {
        '$gte': req.params.cycle_time_begin,
        '$lte': req.params.cycle_time_end
      }
    }

    var options_cyclesByCluster_Meta = {
      select: ['cycle_time_begin']
    }
    if (typeof _callback.page_state_meta_cycle !== "undefined") {
      options_cyclesByCluster_Meta.pageState = _callback.page_state_meta_cycle
    }
    var send_json = function(response) {
      if (typeof response.meta_cycles !== "undefined") {
        if (response.meta_cycles.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({method: 'getCyclesByCluster', message: 'invalid params'});
      }
    }

    var get_meta_cycles = function(callback) {
      var meta_cycles = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.cyclesByCluster_Meta.eachRow(query_cyclesByCluster_Meta, options_cyclesByCluster_Meta, function(n, row) {
          meta_cycles.push(row);
        }, function(err, meta_cycle) {
          if (err)
            reject(err);
          if (meta_cycle) {
            _callback.page_state_meta_cycle = meta_cycle.pageState;
            _callback.meta_cycles = meta_cycles;
            resolve(_callback);
          }
        });
      });

      promise.then(function(response) {
        if (response.meta_cycles.length > 0) {
          callback(response);
        } else {
          res.status(404).send("");
        }
      }, function(error) {
        console.error(error);
      });
    }
    get_meta_cycles(send_json);

  }
  var findCyclesByCluster_TimeBegin = function(req, res) {
    var _callback = {
      page_state_cycle: req.params.page_state_cycle
    }
    var query_cyclesByCluster = {
      cluster_id: models.uuidFromString(req.params.cluster_id),
      cycle_time_begin: req.params.cycle_time_begin
    }

    var options_cyclesByCluster = {
      fetchSize: 5
    }
    if (typeof _callback.page_state_cycle !== "undefined") {
      options_cyclesByCluster.pageState = _callback.page_state_cycle
    }
    var send_json = function(response) {
      if (typeof response.cycles !== "undefined") {
        if (response.cycles.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({method: 'getCyclesByCluster', message: 'invalid params'});
      }
    }

    var get_cycles = function(callback) {
      var cycles = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.cyclesByCluster.eachRow(query_cyclesByCluster, options_cyclesByCluster, function(n, row) {
          cycles.push(row);
        }, function(err, cycle) {
          if (err)
            reject(err);
          if (cycle) {
            _callback.page_state_cycle = cycle.pageState;
            _callback.cycles = cycles;
            resolve(_callback);
          }
        });
      });

      promise.then(function(response) {
        if (response.cycles.length > 0) {
          callback(response);
        } else {
          res.status(404).send("");
        }
      }, function(error) {
        console.error(error);
      });
    }
    get_cycles(send_json);
  }

  var findCyclesByCluster_cycleId = function(req, res) {
    if (req.params.cluster_id && req.params.cycle_time_begin && req.params.user_id && req.params.cycle_id) {
      var query_CycleById = {
        cluster_id: models.uuidFromString(req.params.cluster_id),
        cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD'),
        user_id : models.uuidFromString(req.params.user_id),
        id: models.uuidFromString(req.params.cycle_id)
      }
      models.instance.cyclesByCluster.find(query_CycleById, function(err, rec) {
        if (err)
          throw err;
        if (rec) {
          res.status(200).json(rec);
        } else {
          res.status(404).json({method: 'getCyclesByCluster', message: 'invalid params'});
        }
      });
    } else {
      res.status(401).json({method: "getCyclesByClusters", message: "Unauthorized"})
    }
  }
  /*********** GET CYCLES BY USER TIME BEGIN-END META MERGED **********/

  var findCyclesByUser_TimeBeginEnd_MetaMerged = function(req, res) {
    var _callback = {
      page_state_meta_cycle: req.params.page_state_meta_cycle
    }
    var query_cyclesByUser_Meta = {
      user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
      user_id: models.uuidFromString(req.params.user_id),
      cycle_time_begin: {
        '$gte': req.params.cycle_time_begin,
        '$lte': req.params.cycle_time_end
      }
    }
    var options_cyclesByUser_Meta = {
      select: ['cycle_time_begin'],
      raw: true,
      fetchSize: 3
    }
    if (typeof _callback.page_state_meta_cycle !== "undefined") {
      options_cyclesByUser_Meta.pageState = _callback.page_state_meta_cycle
    }
    var send_json = function(response) {
      if (typeof response.meta_cycles !== "undefined") {
        if (response.meta_cycles.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({method: 'getCyclesByUser', message: 'invalid params'});
      }
    }
    var get_meta_cycles = function(callback) {
      var meta_cycles = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.cyclesByUser_Meta.eachRow(query_cyclesByUser_Meta, options_cyclesByUser_Meta, function(n, row) {
          meta_cycles.push(row);
        }, function(err, meta_cycle) {
          if (err)
            reject(err);
          if (meta_cycle) {
            _callback.page_state_meta_cycle = meta_cycle.pageState;
            _callback.meta_cycles = meta_cycles;
            _callback.cycles = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        if (response.meta_cycles.length > 0) {
          callback(response);
        } else {
          res.status(404).send("");
        }

      }, function(error) {
        console.error(error);
      });

    }
    var get_cycles = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_cycles.forEach(function(metaCycle) {
          var query_cyclesByUser = {
            user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
            user_id: models.uuidFromString(req.params.user_id),
            cycle_time_begin: metaCycle.cycle_time_begin
          }
          var options_cyclesByUser = {
            raw: true
          }
          models.instance.cyclesByUser.eachRow(query_cyclesByUser, options_cyclesByUser, function(n, row) {
            _callback.cycles.push(row);
          }, function(err, cycle) {
            if (err)
              reject(err);
            if (cycle) {
              i++;
              if (i === _callback.meta_cycles.length) {
                resolve(_callback);

              }
            }
          })
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      });
    }

    get_meta_cycles(get_cycles);
  };
  /******** GET CYCLES BY CLUSTER TIME BEGIN-END META MERGED ********/

  var findCyclesByCluster_TimeBeginEnd_MetaMerged = function(req, res) {
    var _callback = {
      page_state_meta_cycle: req.params.page_state_meta_cycle
    }
    var query_cyclesByCluster_Meta = {
      cluster_id: models.uuidFromString(req.params.cluster_id),
      cycle_time_begin: {
        '$gte': req.params.cycle_time_begin,
        '$lte': req.params.cycle_time_end
      }
    }
    var options_cyclesByCluster_Meta = {
      select: ['cycle_time_begin'],
      raw: true,
      fetchSize: 3
    }
    if (typeof _callback.page_state_meta_cycle !== "undefined") {
      options_cyclesByCluster_Meta.pageState = _callback.page_state_meta_cycle
    }
    var send_json = function(response) {
      if (typeof response.meta_cycles !== "undefined") {
        if (response.meta_cycles.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({method: 'getCyclesByCluster', message: 'invalid params'});
      }
    }
    var get_meta_cycles = function(callback) {
      var meta_cycles = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.cyclesByCluster_Meta.eachRow(query_cyclesByCluster_Meta, options_cyclesByCluster_Meta, function(n, row) {
          meta_cycles.push(row);
        }, function(err, meta_cycle) {
          if (err)
            reject(err);
          if (meta_cycle) {
            _callback.page_state_meta_cycle = meta_cycle.pageState;
            _callback.meta_cycles = meta_cycles;
            _callback.cycles = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        if (response.meta_cycles.length > 0) {
          callback(response);
        } else {
          res.status(404).send("");
        }

      }, function(error) {
        console.error(error);
      });

    }
    var get_cycles = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_cycles.forEach(function(metaCycle) {
          var query_cyclesByCluster = {
            cluster_id: models.uuidFromString(req.params.cluster_id),
            cycle_time_begin: metaCycle.cycle_time_begin
          }
          var options_cyclesByCluster = {
            raw: true
          }
          models.instance.cyclesByCluster.eachRow(query_cyclesByCluster, options_cyclesByCluster, function(n, row) {
            _callback.cycles.push(row);
          }, function(err, cycle) {
            if (err)
              reject(err);
            if (cycle) {
              i++;
              if (i === _callback.meta_cycles.length) {
                resolve(_callback);

              }
            }
          })
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      });

    }

    get_meta_cycles(get_cycles);
  }

  /******** GET CYCLES BY SPECIES TIME BEGIN-END META MERGED ********/

  var findCyclesBySpecies_TimeBeginEnd_MetaMerged = function(req, res) {
    var _callback = {
      page_state_meta_cycle: req.params.page_state_meta_cycle
    }
    var query_cyclesBySpecies_Meta = {
      fish_species: req.params.fish_species,
      crop_species: req.params.crop_species,
      cycle_time_begin: {
        '$gte': req.params.cycle_time_begin,
        '$lte': req.params.cycle_time_end
      }
    }
    var options_cyclesBySpecies_Meta = {
      select: ['cycle_time_begin'],
      raw: true,
      fetchSize: 3
    }
    if (typeof _callback.page_state_meta_cycle !== "undefined") {
      options_cyclesBySpecies_Meta.pageState = _callback.page_state_meta_cycle
    }
    var send_json = function(response) {
      if (typeof response.meta_cycles !== "undefined") {
        if (response.meta_cycles.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({method: 'getCyclesBySpecies', message: 'invalid params'});
      }
    }
    var get_meta_cycles = function(callback) {
      var meta_cycles = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.cyclesBySpecies_Meta.eachRow(query_cyclesBySpecies_Meta, options_cyclesBySpecies_Meta, function(n, row) {
          meta_cycles.push(row);
        }, function(err, meta_cycle) {
          if (err)
            reject(err);
          if (meta_cycle) {
            _callback.page_state_meta_cycle = meta_cycle.pageState;
            _callback.meta_cycles = meta_cycles;
            _callback.cycles = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        if (response.meta_cycles.length > 0) {
          callback(response);
        } else {
          res.status(404).send("");
        }

      }, function(error) {
        console.error(error);
      });

    }
    var get_cycles = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_cycles.forEach(function(metaCycle) {
          var query_cyclesBySpecies = {
            fish_species: req.params.fish_species,
            crop_species: req.params.crop_species,
            cycle_time_begin: metaCycle.cycle_time_begin
          }
          var options_cyclesBySpecies = {
            raw: true
          }
          models.instance.cyclesBySpecies.eachRow(query_cyclesBySpecies, options_cyclesBySpecies, function(n, row) {
            _callback.cycles.push(row);
          }, function(err, cycle) {
            if (err)
              reject(err);
            if (cycle) {
              i++;
              if (i === _callback.meta_cycles.length) {
                resolve(_callback);

              }
            }
          })
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      });

    }

    get_meta_cycles(get_cycles);
  }
  /******** RETURN CYCLES ********/

  return {
    //android
    findMetaCyclesByUser_TimeBeginEnd: findMetaCyclesByUser_TimeBeginEnd,
    findCyclesByUser_TimeBegin: findCyclesByUser_TimeBegin,
    findCyclesByUser_TimeBeginEnd_Name_MetaMerged: findCyclesByUser_TimeBeginEnd_Name_MetaMerged,
    findCyclesByUser_cycleId: findCyclesByUser_cycleId,
    closeCycle: closeCycle,
    updateCycle: updateCycle,
    addCycle: addCycle,
    //end android

    //backend
    findMetaCyclesByCluster_TimeBeginEnd: findMetaCyclesByCluster_TimeBeginEnd,
    findCyclesByCluster_TimeBegin: findCyclesByCluster_TimeBegin,
    findCyclesByCluster_cycleId: findCyclesByCluster_cycleId,

    findCyclesByUser_TimeBeginEnd_MetaMerged: findCyclesByUser_TimeBeginEnd_MetaMerged,
    findCyclesByCluster_TimeBeginEnd_MetaMerged: findCyclesByCluster_TimeBeginEnd_MetaMerged,
    findCyclesBySpecies_TimeBeginEnd_MetaMerged: findCyclesBySpecies_TimeBeginEnd_MetaMerged

  }
}

module.exports = cyclesController;
