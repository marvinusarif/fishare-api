'use strict';

var moment = require('moment');
var feedingsController = function (models) {

    /****************** GET FEEDINGS BY CYCLE **********************/
    var getFeedingsByCycle = function (req, res) {
        if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
            var feedings = [];

            var _callback = {
                page_state_feeding: req.params.page_state_feeding
}

            var query_feedingsByCycle = {
                user_join_date : moment(req.user.join_date).format('YYYY-MM-DD'),
                cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD HH:mm:ss'),
                cycle_id: models.uuidFromString(req.params.cycle_id)
            }

            var options_feedingsByCycle = {
                select: ["cycle_id", "time_begin", "time_end", "protein", "quantity", "species", "fish_weight", "feed", "id"],
                fetchSize: 30
            }

            if (typeof req.params.page_state_feeding !== "undefined") {
                options_feedingsByCycle.pageState = _callback.page_state_feeding;
            }

            models.instance.feedingsByCycle.eachRow(query_feedingsByCycle, options_feedingsByCycle, function (n, row) {
                feedings.push(row);
            }, function (err, feeding) {
                if (err) throw err;
                if (feeding) {
                    if (feedings.length > 0) {
                        _callback.page_state_feeding = feeding.pageState;
                        _callback.feedings = feedings;
                        res.status(200).json(_callback);
                    } else {
                        res.status(404).send("");
                    }
                } else {
                    res.status(404).json({
                        method: "getFeedingsByCycle",
                        message: "invalid params"
                    });
                }
            })
        } else {
            res.status(401).json({
                method: 'getFeedingsByCycle',
                params: 'Unauthorized'
            })
        }

    };
    /********** GET FEEDINGS BY CYCLE TIME BEGIN ************/

    var getFeedingsByCycle_TimeBegin = function (req, res) {
        if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
            var feedings = [];

            var _callback = {
                page_state_feeding: req.params.page_state_feeding
            }

            var query_feedingsByCycle = {
                user_join_date : moment(req.user.join_date).format('YYYY-MM-DD'),
                cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD HH:mm:ss'),
                cycle_id: models.uuidFromString(req.params.cycle_id),
                time_begin: {
                    '$gte': req.params.feeding_time_begin
                }
            }

            var options_feedingsByCycle = {
                select: ["cycle_id", "time_begin", "time_end", "quantity", "species", "fish_weight", "feed", "id"],
                fetchSize: 30
            }

            if (typeof req.params.page_state_feeding !== "undefined") {
                options_feedingsByCycle.pageState = _callback.page_state_feeding;
            }
            models.instance.feedingsByCycle.eachRow(query_feedingsByCycle, options_feedingsByCycle, function (n, row) {
                feedings.push(row);
            }, function (err, feeding) {
                if (err) throw err;
                if (feeding) {
                    if (feedings.length > 0) {
                        _callback.page_state_feeding = feeding.pageState;
                        _callback.feedings = feedings;
                        res.status(200).json(_callback);
                    } else {
                        res.status(404).send("");
                    }
                } else {
                    res.status(404).json({
                        method: "getFeedingsByCycle",
                        message: "invalid params"
                    });
                }
            })
        } else {
            res.status(401).json({
                method: 'getFeedingsByCycle',
                params: 'Unauthorized'
            })
        }
    };
    /********** GET FEEDINGS BY CYCLE TIME BEGIN-END ************/

    var getFeedingsByCycle_TimeBeginEnd = function (req, res) {
        if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
            var feedings = [];

            var _callback = {
                page_state_feeding: req.params.page_state_feeding
            }

            var query_feedingsByCycle = {
                user_join_date : moment(req.user.join_date).format('YYYY-MM-DD'),
                cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD HH:mm:ss'),
                cycle_id: models.uuidFromString(req.params.cycle_id),
                time_begin: {
                    '$gte': req.params.feeding_time_begin,
                    '$lte': req.params.feeding_time_end
                }
            }

            var options_feedingsByCycle = {
                select: ["cycle_id", "time_begin", "time_end", "quantity", "species", "fish_weight", "feed", "id"],
                fetchSize: 30
            }

            if (typeof req.params.page_state_feeding !== "undefined") {
                options_feedingsByCycle.pageState = _callback.page_state_feeding;
            }

            models.instance.feedingsByCycle.eachRow(query_feedingsByCycle, options_feedingsByCycle, function (n, row) {
                feedings.push(row);
            }, function (err, feeding) {
                if (err) throw err;
                if (feeding) {
                    if (feedings.length > 0) {
                        _callback.page_state_feeding = feeding.pageState;
                        _callback.feedings = feedings;
                        res.status(200).json(_callback)
                    } else {
                        res.status(404).send("");
                    }
                } else {
                    res.status(404).json({
                        method: "getFeedingsByCycle",
                        message: "invalid params"
                    });
                }
          });
        } else {
            res.status(401).json({
                method: 'getFeedingsByCycle',
                params: 'Unauthorized'
            });
        }
    };
    /********** ADD FEEDINGS ************/

    var addFeeding = function (req, res, next) {
        if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
            var feed_quantity_total = (req.body.quantity_program - req.body.quantity_remaining);
            var feed_protein_total = (req.body.feed.protein * (req.body.quantity_program - req.body.quantity_remaining));
            var normalize_all_feed = function () {
                return new Promise(function (resolve, reject) {
                    var feeds = { };
                    feeds[req.body.feed.id] = {
                            type: req.body.feed.type,
                            spesification: req.body.feed.spesification,
                            floating: req.body.feed.floating,
                            name: req.body.feed.name,
                            protein: req.body.feed.protein,
                            //parameter below will be used for update cycle
                            protein_total: req.body.feed.protein_total + feed_protein_total,
                            quantity_total: req.body.feed.quantity_total + feed_quantity_total
                        }

                    var i = 0;
                    req.body.additives.forEach(function (additive) {
                        feeds[additive.id] = {
                                type: additive.type,
                                spesification: additive.spesification,
                                floating: additive.floating,
                                name: additive.name,
                                protein: additive.protein,
                                //parameter below will be used for update cycle
                                protein_total: additive.protein_total,
                                quantity_total: additive.quantity_total
                            }
                        i++;
                        if (req.body.additives.length === i) {
                            resolve(feeds);
                        }
                    });
                });
            }
            var save_feeding = function (feeds) {
                var value_feedingsByCycle = {
                    user_join_date : moment(req.user.join_date).format('YYYY-MM-DD'),
                    cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD HH:mm:ss'),
                    cycle_id: models.uuidFromString(req.body.cycle_id),
                    time_begin: moment(req.body.time_begin).format('YYYY-MM-DD hh:mm:ss'),
                    time_end: moment(req.body.time_end).format('YYYY-MM-DD hh:mm:ss'),
                    id: models.timeuuid(),
                    quantity: feed_quantity_total,
                    species: req.body.fish_species,
                    fish_weight_avg: parseFloat(req.body.fish_weight_avg),
                    fish_weight: parseFloat(req.body.fish_weight),
                    fish_quantity: parseFloat(req.body.fish_quantity),
                    protein: feed_protein_total,
                    measurement: req.body.measurement,
                    feed: feeds
                }


                var value_feedingsByCluster_Meta = {
                    year_month: moment(req.body.time_begin).format('YYYYMM'),
                    cluster_id: req.user.cluster_id,
                    cycle_time_begin: value_feedingsByCycle.cycle_time_begin
                }

                var value_feedingsByCluster = {
                    cluster_id: value_feedingsByCluster_Meta.cluster_id,
                    year_month: value_feedingsByCluster_Meta.year_month,
                    cycle_time_begin: value_feedingsByCycle.cycle_time_begin,
                    time_begin: value_feedingsByCycle.time_begin,
                    time_end: value_feedingsByCycle.time_end,
                    id: value_feedingsByCycle.id,
                    cycle_id: value_feedingsByCycle.cycle_id,
                    user_id: req.user.id,
                    quantity: value_feedingsByCycle.quantity,
                    species: value_feedingsByCycle.species,
                    fish_weight_avg: value_feedingsByCycle.fish_weight_avg,
                    fish_weight: value_feedingsByCycle.fish_weight,
                    fish_quantity: value_feedingsByCycle.fish_quantity,
                    measurement: value_feedingsByCycle.measurement,
                    feed: feeds
                }

                var queries = [];
                return new Promise(function (resolve, reject) {

                    var save_feedingsByCluster_Meta = new models.instance.feedingsByCluster_Meta(value_feedingsByCluster_Meta).save({
                        return_query: true
                    });
                    var save_feedingsByCycle = new models.instance.feedingsByCycle(value_feedingsByCycle).save({
                        return_query: true
                    });
                    var save_feedingsByCluster = new models.instance.feedingsByCluster(value_feedingsByCluster).save({
                        return_query: true
                    });

                    queries.push(save_feedingsByCluster_Meta, save_feedingsByCluster, save_feedingsByCycle);

                    models.doBatch(queries, function (err) {
                        (err) ? reject(err): resolve(value_feedingsByCycle.id);
                    });
                });
            }

            normalize_all_feed().then(function (feeds) {
                save_feeding(feeds).then(function (feeding_id) {
                        req.body.id = req.body.cycle_id;
                        req.body.feeding_id = feeding_id;
                        req.body.feed = feeds;
                        //since the field in the cycle has the same name we have to reset this to undefined to prevent closing cycle;
                        delete req.body.time_end;
                        next();
                    }).catch(function(error){
                    console.error(error);
                })
                    .catch(function (err) {
                        console.error(err);
                    })
            });
        } else {
            res.status(401).json({
                method: 'addFeeding',
                params: 'Unauthorized'
            })
        }
    };
    /********** GET FEEDINGS BY CLUSTER TIME BEGIN-END *********/

    var getFeedingsByCluster_TimeBeginEnd_MetaMerged = function (req, res) {
        var _callback = {
            page_state_meta_feeding: req.params.page_state_meta_feeding
        }

        var query_feedingsByCluster_Meta = {
            cluster_id: models.uuidFromString(req.params.cluster_id),
            year_month: req.params.year_month,
            cycle_time_begin: {
                '$gte': req.params.cycle_time_begin,
                '$lte': req.params.cycle_time_begin
            }
        }

        var options_feedingsByCluster_Meta = {
            select: ["year_month", "cluster_id", "cycle_time_begin"],
            fetchSize: 3
        }

        if (typeof _callback.page_state_meta_feeding !== "undefined") {
            options_feedingsByCluster_Meta.pageState = _callback.page_state_meta_feeding
        }
        var send_json = function (response) {
            if (typeof response.meta_feedings !== "undefined") {
                if (response.meta_feedings.length > 0) {
                    res.status(200).json(response);
                } else {
                    res.status(404).send("");
                }
            } else {
                res.status(404).json({
                    method: 'getFeedingsByCluster',
                    message: 'invalid params'
                });
            }
        }
        var get_meta_feedings = function (callback) {
            var meta_feedings = [];
            var promise = new Promise(function (resolve, reject) {
                models.instance.feedingsByCluster_Meta.eachRow(query_feedingsByCluster_Meta, options_feedingsByCluster_Meta, function (n, row) {
                    meta_feedings.push(row);
                }, function (err, meta_feeding) {
                    if (err) reject(err);;
                    if (meta_feeding) {
                        _callback.page_state_meta_feeding = meta_feeding.pageState;
                        _callback.meta_feedings = meta_feedings;
                        _callback.feedings = [];
                    }
                });
            });
            promise.then(function (response) {
                callback(response);
            }, function (error) {
                console.error(error);
            })

        }
        var get_feedings = function (_callback) {
            var promise = new Promise(function (resolve, reject) {
                var i = 0;
                _callback.meta_feedings.forEach(function (meta_feeding) {
                    var query_feedingsByCluster = {
                        cluster_id: models.uuidFromString(req.params.cluster_id),
                        year_month: req.params.year_month,
                        cycle_time_begin: meta_feeding.cycle_time_begin
                    };
                    var options_feedingsByCluster = {
                        raw: true
                    };
                    models.instance.feedingsByCluster.eachRow(query_feedingsByCluster, options_feedingsByCluster, function (n, row) {
                        _callback.feedings.push(row);
                    }, function (err, feedings) {
                        if (err) reject(err);
                        i++;
                        if (i === _callback.meta_feedings.length) {
                            resolve(_callback);
                        }
                    })
                });

            });
            promise.then(function (response) {
                send_json(response);
            }, function (error) {
                console.error(error);
            })
        }
        get_meta_feedings(get_feedings);

    };
    /******* GET FEEDINGS BY CLUSTER TIME BEGIN-END META MERGED *******/

    var getFeedingsByCluster_TimeBeginEnd_Feed_MetaMerged = function (req, res) {
        var _callback = {
            page_state_meta_feeding: req.params.page_state_meta_feeding
        }

        var query_feedingsByCluster_Meta = {
            cluster_id: models.uuidFromString(req.params.cluster_id),
            year_month: req.params.year_month,
            cycle_time_begin: {
                '$gte': req.params.cycle_time_begin,
                '$lte': req.params.cycle_time_begin
            }
        }

        var options_feedingsByCluster_Meta = {
            select: ["year_month", "cluster_id", "cycle_time_begin"],
            fetchSize: 3
        }

        if (typeof _callback.page_state_meta_feeding !== "undefined") {
            options_feedingsByCluster_Meta.pageState = _callback.page_state_meta_feeding
        }
        var send_json = function (response) {
            if (typeof response.meta_feedings !== "undefined") {
                if (response.meta_feedings.length > 0) {
                    res.status(200).json(response);
                } else {
                    res.status(404).send("");
                }
            } else {
                res.status(404).json({
                    method: 'getFeedingsByCluster',
                    message: 'invalid params'
                });
            }
        }
        var get_meta_feedings = function (callback) {
            var meta_feedings = [];
            var promise = new Promise(function (resolve, reject) {
                models.instance.feedingsByCluster_Meta.eachRow(query_feedingsByCluster_Meta, options_feedingsByCluster_Meta, function (n, row) {
                    meta_feedings.push(row);
                }, function (err, meta_feeding) {
                    if (err) reject(err);;
                    if (meta_feeding) {
                        _callback.page_state_meta_feeding = meta_feeding.pageState;
                        _callback.meta_feedings = meta_feedings;
                        _callback.feedings = [];
                    }
                });
            });
            promise.then(function (response) {
                callback(response);
            }, function (error) {
                console.error(error);
            })

        }
        var get_feedings = function (_callback) {
            var promise = new Promise(function (resolve, reject) {
                var i = 0;
                _callback.meta_feedings.forEach(function (meta_feeding) {
                    var query_feedingsByCluster = {
                        cluster_id: models.uuidFromString(req.params.cluster_id),
                        year_month: req.params.year_month,
                        cycle_time_begin: meta_feeding.cycle_time_begin,
                        feed: req.params.feed
                    };
                    var options_feedingsByCluster = {
                        raw: true
                    };
                    models.instance.feedingsByCluster.eachRow(query_feedingsByCluster, options_feedingsByCluster, function (n, row) {
                        _callback.feedings.push(row);
                    }, function (err, feedings) {
                        if (err) reject(err);
                        i++;
                        if (i === _callback.meta_feedings.length) {
                            resolve(_callback);
                        }
                    })
                });
            });
            promise.then(function (response) {
                send_json(response);
            }, function (error) {
                console.error(error);
            })
        }
        get_meta_feedings(get_feedings);

    };
    /****************** RETURN FEEDINGS *******************/

    return {
        //android
        getFeedingsByCycle: getFeedingsByCycle,
        getFeedingsByCycle_TimeBegin: getFeedingsByCycle_TimeBegin,
        getFeedingsByCycle_TimeBeginEnd: getFeedingsByCycle_TimeBeginEnd,
        addFeeding: addFeeding,
        //end android
        getFeedingsByCluster_TimeBeginEnd_MetaMerged: getFeedingsByCluster_TimeBeginEnd_MetaMerged,
        getFeedingsByCluster_TimeBeginEnd_Feed_MetaMerged: getFeedingsByCluster_TimeBeginEnd_Feed_MetaMerged
        //backend

    }
}

module.exports = feedingsController;
