'use strict';

var feedsController = function(models) {
  /************* FIND FEEDS *************/
  var getFeeds = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date) && (req.user.country)) {
      var feeds = [];
      var query_feedsByCountry = {
        country: req.user.country,
        type: req.params.type.toUpperCase()
      }
      var options_feedsByCountry = {
        select: ['type', 'name', 'fish_species', 'company', 'floating', 'id', 'spesification']
      }
      models.instance.feedsByCountry.eachRow(query_feedsByCountry, options_feedsByCountry, function(n, row) {
        feeds.push(row);
      }, function(err, feed) {
        if (err) throw err;
        if (feed) {
          if (feeds.length > 0) {
            res.status(200).json(feeds);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getFeedsByCountry',
            message: 'invalid params'
          });
        }

      });
    } else {
      res.status(401).json({
        method: "getFeedsByCountry",
        message: "Unauthorized"
      })
    }
  };
  /************* FIND FEEDS BY NAME *************/

  var getFeedsByName = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date) && (req.user.country)) {
      var feeds = [];
      var query_feedsByCountry = {
        country: req.user.country,
        type: req.params.type.toUpperCase(),
        name: {
          '$like': req.params.name.toUpperCase() + '%'
        },
      }
      var options_feedsByCountry = {
        select: ['type', 'name', 'fish_species', 'company', 'floating', 'id', 'spesification'],
        fetchSize: 15
      }
      models.instance.feedsByCountry.eachRow(query_feedsByCountry, options_feedsByCountry, function(n, row) {
          feeds.push(row);
        },
        function(err, feed) {
          if (err) throw err;
          if (feed) {
            if (feeds.length > 0) {
              res.status(200).json(feeds);
            } else {
              res.status(404).send("");
            }
          } else {
            res.status(404).json({
              method: 'getFeedsByCountry',
              message: 'invalid params'
            });
          }

        })
    } else {
      res.status(401).json({
        method: "getFeedsByCountry",
        message: "Unauthorized"
      })
    }
  };
  /************* ADD FEEDS *************/

  var addFeed = function(req, res) {
    var value_feedsByCountry = {
      country: req.body.country.toUpperCase(),
      id: models.uuid(),
      type: req.body.type,
      name: req.body.name,
      fish_species: req.body.fish_species,
      company: req.body.company,
      floating: Boolean(req.body.floating),
      spesification: req.body.spesification
    }
    var save_feed = function() {
      return new Promise(function(resolve, reject) {
        var _callback = {
          country: value_feedsByCountry.country,
          id: value_feedsByCountry.id,
          type: value_feedsByCountry.type,
          name: value_feedsByCountry.name,
          fish_species: value_feedsByCountry.fish_species,
          company: value_feedsByCountry.company,
          floating: value_feedsByCountry.floating,
          spesification: value_feedsByCountry.spesification
        }
        new models.instance.feedsByCountry(value_feedsByCountry).save(function(err) {
          (err) ? reject(err): resolve(_callback);
        });
      });
    }
    save_feed()
      .then(function(response) {
        res.status(200).json(response);
      })
      .catch(function(error) {
        console.error(error);
      })

  };

  var updateFeed = function(req, res) {
    if ((req.body.country) && (req.body.id) && (req.body.type)) {
      var query_feedsByCountry = {
        country: req.body.country,
        type: req.body.type,
        id: models.uuidFromString(req.body.id)
      }
      var value_feedsByCountry = {
        name: req.body.name,
        fish_species: req.body.fish_species,
        company: req.body.company,
        floating: Boolean(req.body.floating),
        spesification: req.body.spesification
      }
      var update_feed = function() {
        return new Promise(function(resolve, reject) {
          var _callback = {
            country: query_feedsByCountry.country,
            id: query_feedsByCountry.id,
            type: value_feedsByCountry.type,
            name: value_feedsByCountry.name,
            fish_species: value_feedsByCountry.fish_species,
            company: value_feedsByCountry.company,
            floating: value_feedsByCountry.floating,
            spesification: value_feedsByCountry.spesification
          }

          models.instance.feedsByCountry.update(query_feedsByCountry, value_feedsByCountry, function(err) {
            (err) ? reject(err): resolve(_callback);
          });
        });
      }
      update_feed()
        .then(function(response) {
          res.status(200).json(response);
        })
        .catch(function(error) {
          console.error(error);
        })

    }
  };
  return {
    //android
    getFeeds: getFeeds,
    getFeedsByName: getFeedsByName,
    //end android

    //backend
    addFeed: addFeed,
    updateFeed: updateFeed
  }
}

module.exports = feedsController;
