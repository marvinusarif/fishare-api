'use strict';

var moment = require('moment');
var jobsController = function(models) {
  var getJobsByCountry = function(req, res) {
    if (req.user.id && req.user.country && req.user.join_date) {
      var query_jobsByCountry = {
        country: req.user.country
      }
      var options_jobsByCountry = {
        select: ['job', 'description']
      }

      models.instance.jobsByCountry.find(query_jobsByCountry, options_jobsByCountry, function(err, jobs) {
        if (err) console.error(err);
        if (jobs) {
          res.status(200).json(jobs)
        } else {
          res.status(404).json({
            method: "getJobsByCountry",
            message: "invalid params"
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getJobsByCountry",
        message: "Unauthorized"
      });
    }


  }
  var getJobsByCountry_Name = function(req, res) {
    if (req.user.id && req.user.country && req.user.join_date) {
      var query_jobsByCountry = {
        country: req.user.country,
        job: {
          '$like': req.params.job + '%'
        }
      }
      var options_jobsByCountry = {
        select: ['job', 'description']
      }

      models.instance.jobsByCountry.find(query_jobsByCountry, options_jobsByCountry, function(err, jobs) {
        if (err) console.error(err);
        if (jobs) {
          res.status(200).json(jobs)
        } else {
          res.status(404).json({
            method: "getJobsByCountry",
            message: "invalid params"
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getJobsByCountry",
        message: "Unauthorized"
      });
    }
  }
  var addJob = function(req, res) {
    var value_jobsByCountry = {
      country: req.body.country,
      job: req.body.job,
      description: req.body.description,
      id: models.uuid()
    }

    new models.instance.jobsByCountry(value_jobsByCountry).save(function(err) {
      if (err) {
        console.error(err);
      } else {
        res.status(200).json(value_jobsByCountry);
      }
    });
  }
  /****************** RETURN DEVICES **********************/

  return {
    //android
    getJobsByCountry: getJobsByCountry,
    getJobsByCountry_Name: getJobsByCountry_Name,
    //end android

    //backend
    addJob: addJob
    //end backend

  }
}

module.exports = jobsController;
