'use strict';
var moment = require('moment');

var recommendationsController = function(models) {
  var recommendationsByCycle = function(req, res) {
    if (req.user.id && req.user.email && req.user.join_date) {
      var recommendations = [];
      var _callback = {
        page_state_recommendation: req.params.page_state_recommendation
      }
      var query_recommendationsByCycle = {
        cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD'),
        cycle_id: models.uuidFromString(req.params.cycle_id)
      }

      var options_recommendationsByCycle = {
        select: ['cycle_id', 'event_time', 'topic', 'description', 'problem', 'posted_by'],
        fetchSize: 10
      }
      if (typeof _callback.page_state_recommendation !== "undefined") {
        options_recommendationsByCycle.pageState = _callback.page_state_recommendation;
      }
      models.instance.recommendationsByCycle.eachRow(query_recommendationsByCycle, options_recommendationsByCycle,
        function(n, row) {
          recommendations.push(row);
        },
        function(err, recommendation) {
          if (err) console.error(err);
          if (recommendation) {
            if (recommendations.length > 0) {
              _callback.recommendations = recommendations;
              _callback.page_state_recommendation = recommendation.pageState;
              res.status(200).json(_callback);
            } else {
              res.status(404).send("");
            }
          } else {
            res.status(404).json({
              method: 'getRecommendationsByCycle',
              message: 'invalid params'
            });
          }
        });
    } else {
      res.status(404).json({
        method: 'getRecommendationsByCycle',
        message: 'Unauthorized'
      });
    }

  }
  var addRecommendation = function(req, res) {
    if (req.user.id && req.user.email && req.user.join_date) {
      var value_recommendationsByCycle = {
        cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
        cycle_id: models.uuidFromString(req.body.cycle_id),
        event_time: moment().format('YYYY-MM-DD HH:mm:ss'),
        helpful: models.datatypes.unset,
        topic: req.body.topic,
        description: req.body.description,
        problem: models.datatypes.unset,
        posted_by: req.body.posted_by
      }

      new models.instance.recommendationsByCycle(value_recommendationsByCycle).save(function(err) {
        if (err) {
          console.error(err);
        } else {
          res.status(200).json(value_recommendationsByCycle);
        }
      });
    } else {
      res.status(404).json({
        method: 'getRecommendationsByCycle',
        message: 'Unauthorized'
      });
    }
  }
  var updateRecommendation = function(req, res) {
    var query_recommendationsByCycle = {
      cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
      cycle_id: models.uuidFromString(req.body.cycle_id),
      event_time: moment(req.body.event_time).format('YYYY-MM-DD HH:mm:ss')
    }

    var value_recommendationsByCycle = {
      helpful: Boolean(req.body.helpful)
    }
    models.instance.recommendationsByCycle.update(query_recommendationsByCycle, value_recommendationsByCycle, function(err) {
      if (err) {
        console.error(err);
      } else {
        var _callback = {
          cycle_time_begin: query_recommendationsByCycle.cycle_time_begin,
          cycle_id: query_recommendationsByCycle.cycle_id,
          event_time: query_recommendationsByCycle.event_time,
          helpful: value_recommendationsByCycle.helpful
        }
        res.status(200).json(_callback);
      }
    })
  }
  return {
    //ANDROID
    recommendationsByCycle: recommendationsByCycle,
    //BACK END
    addRecommendation: addRecommendation,
    updateRecommendation: updateRecommendation
  }
}

module.exports = recommendationsController;
