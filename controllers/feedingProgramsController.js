"use strict";

var feedingProgramsControllers = function(models) {
  var getFeedingProgramsByCycle = function(req, res) {
    if (req.user.id && req.user.cluster_id && req.user.join_date) {
      var query_usersByCycle = {
        user_join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        user_id: req.user.id,
        cycle_time_begin: req.body.cycle_time_begin,
        id: models.uuidFromString(req.body.cycle_id)
      }
      var options_usersByCycle = {
        select: ['fish', 'crop', 'measurement', 'sample', 'harvest', 'feeding_program']
      }
      models.instance.cyclesByUser.findOneAsync(query_usersByCycle, options_usersByCycle).then(function(rec) {
        //calculation remaining fish
        if (rec) {
          var calculate_quantity = function(callback) {
            var fish = {
              quantity: rec.fish.qty,
              weight: rec.fish.weight,
              weight_avg: 0
            };
            var crop = {
              quantity: rec.crop.qty,
              weight: rec.crop.weight,
              weight_avg: 0
            };
            if (typeof rec.harvest.fish !== "undefined") {
              fish.quantity = rec.fish.qty - rec.harvest.fish.quantity;
            }
            if (typeof rec.harvest.crop !== "undefined") {
              crop.quantity = rec.crop.qty - rec.harvest.crop.quantity;
            }
            if (typeof rec.sample.fish !== "undefined") {
              fish.weight_avg = rec.sample.fish.weight;
              fish.weight = fish.quantity * fish.weight_avg / 1000;
              if (fish.weight < 0) {
                fish.weight = 0;
              }
            }
            if (typeof rec.sample.crop !== "undefined") {
              crop.weight_avg = rec.sample.crop.weight;
              crop.weight = crop.quantity * crop.weight_avg / 1000;
              if (crop.weight < 0) {
                crop.weight = 0;
              }
            }
            callback(fish, crop);
          }
          var set_feeding_program = function(fish, crop) {
            var query_feeding_programs = {
              fish: {
                species: rec.fish.species,
                producer: rec.fish.producer,
                quantity: fish.quantity,
                weight: fish.weight,
                weight_avg: fish.weight_avg
              },
              crop: {
                species: rec.crop.species,
                producer: rec.crop.producer,
                quantity: crop.quantity,
                weight: crop.weight,
                weight_avg: crop.weight_avg
              },
              measurement: rec.measurement,
              feed: req.body.feed,
              additives: req.body.additive,
              feeding_program: {
                quantity: 100 // should be gathered from FeedingProgramML_MODELS
              }
            }
            res.status(200).json(query_feeding_programs);
          }
          calculate_quantity(set_feeding_program);
        }
      }).catch(function(err) {
        console.error(err);
      })
    }
  }
  return {
    getFeedingProgramsByCycle: getFeedingProgramsByCycle
  }
}

module.exports = feedingProgramsControllers;
