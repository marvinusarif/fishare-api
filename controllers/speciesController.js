'use strict';

var speciesController = function(models) {

  Array.prototype.inArray = function(value) {
    // Returns true if the passed value is found in the
    // array. Returns false if it is not.
    var i;
    for (i = 0; i < this.length; i++) {
      if (this[i] == value) {
        return true;
      }
    }
    return false;
  };
  /************************* GET SPECIES *************************/
  var getSpecies = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date) && (req.user.country)) {
      var speciess = [];
      var speciess_temp = [];
      var query_speciesByCountry = {
        country: req.user.country,
        type: req.params.type.toUpperCase()
      }
      var options_speciesByCountry = {
        select: ['species']
      }
      models.instance.speciesByCountry.eachRow(query_speciesByCountry, options_speciesByCountry, function(n, row) {
        if (speciess_temp.inArray(row.species) === false) {
          speciess_temp.push(row.species);
          speciess.push(row);
        }
      }, function(err, species) {
        if (err) throw err;
        if (species) {
          if (speciess.length > 0) {
            res.status(200).json(speciess);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getSpeciesByCountry',
            message: 'invalid params'
          });
        }

      });
    } else {
      res.status(401).json({
        method: "getSpeciesByCountry",
        message: "Unauthorized"
      })
    }
  };
  /************************* GET PRODUCER *************************/

  var getProducer = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date) && (req.user.country)) {
      var producers = [];
      var query_speciesByCountry = {
        country: req.user.country,
        type: req.params.type.toUpperCase(),
        species: req.params.species.toUpperCase()
      }
      var options_speciesByCountry = {
        select: ['producer', 'id', 'img_url']
      }
      models.instance.speciesByCountry.eachRow(query_speciesByCountry, options_speciesByCountry, function(n, row) {
          producers.push(row);
        },
        function(err, producer) {
          if (err) throw err;
          if (producer) {
            if (producers.length > 0) {
              res.status(200).json(producers);
            } else {
              res.status(404).send("");
            }
          } else {
            res.status(404).json({
              method: 'getProducerByCountry',
              message: 'invalid params'
            });
          }
        });
    } else {
      res.status(401).json({
        method: "getProducerByCountry",
        message: "Unauthorized"
      })
    }
  }
  /*************** GET SPECIES PRODUCER NAME **************/
  var getSpeciesByProducerName = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date) && (req.user.country)) {
      var producers = [];
      var query_speciesByCountry = {
        country: req.user.country,
        type: req.params.type.toUpperCase(),
        species: req.params.species.toUpperCase(),
        producer: {
          '$like': req.params.producer + '%'
        }
      }

      var options_speciesByCountry = {
        select: ['producer', 'id', 'img_url']
      }
      models.instance.speciesByCountry.eachRow(query_speciesByCountry, options_speciesByCountry, function(n, row) {
          producers.push(row);
        },
        function(err, producer) {
          if (err) throw err;
          if (producer) {
            if (producers.length > 0) {
              res.status(200).json(producers);
            } else {
              res.status(404).send("");
            }
          } else {
            res.status(404).json({
              method: 'getProducerByCountry',
              message: 'invalid params'
            });
          }

        })
    } else {
      res.status(401).json({
        method: "getProducerByCountry",
        message: "Unauthorized"
      })
    }
  };
  /*************** ADD SPECIES **************/
  var addSpecies = function(req, res) {

    var value_speciesByCountry = {
      country: req.body.country,
      type: req.body.type.toUpperCase(),
      species: req.body.species.toUpperCase(),
      producer: req.body.producer.toUpperCase(),
      id: models.uuid(),
      img_url: req.body.img_url
    }
    var save_species = function() {
      return new Promise(function(resolve, reject) {
        var _callback = {
          country: value_speciesByCountry.country,
          type: value_speciesByCountry.type,
          species: value_speciesByCountry.species,
          producer: value_speciesByCountry.producer,
          id: value_speciesByCountry.id,
          img_url: value_speciesByCountry.img_url
        }
        new models.instance.speciesByCountry(value_speciesByCountry).save(function(err) {
          (err) ? reject(err): resolve(_callback);
        });
      });
    }
    save_species()
      .then(function(response) {
        res.status(200).json(response);
      })
      .catch(function(error) {
        console.error(error);
      })

  };
  var updateSpecies = function(req, res) {
    if ((req.body.id) && (req.body.country) && (req.body.type)) {
      var query_speciesByCountry = {
        country: req.body.country,
        type: req.body.type.toUpperCase(),
        species: req.body.species.toUpperCase(),
        id: models.uuidFromString(req.body.id)
      }
      var value_speciesByCountry = {
        producer: req.body.producer.toUpperCase()
      }
      if (typeof req.body.img_url == undefined) {
        value_speciesByCountry.img_url = req.body.img_url;
      }
      var update_species = function() {
        return new Promise(function(resolve, reject) {
          var _callback = {
            country: query_speciesByCountry.country,
            type: query_speciesByCountry.type,
            species: query_speciesByCountry.species,
            producer: value_speciesByCountry.producer,
            id: query_speciesByCountry.id,
            img_url: value_speciesByCountry.img_url
          }
          models.instance.speciesByCountry.update(query_speciesByCountry, value_speciesByCountry, function(err) {
            (err) ? reject(err): resolve(_callback);
          });
        });
      }
      update_species()
        .then(function(response) {
          res.status(200).json(response);
        })
        .catcH(function(error) {
          console.error(error);
        })
    }
  };
  /*************** RETURN SPECIES **************/
  return {
    //android
    getSpecies: getSpecies,
    getProducer: getProducer,
    getSpeciesByProducerName: getSpeciesByProducerName,
    //end android

    //backend
    addSpecies: addSpecies,
    updateSpecies: updateSpecies
  }
}

module.exports = speciesController;
