'use strict';

var moment = require('moment');
var devicesController = function(models) {

  /******** GET DEVICES BY PRODUCTION DEVICE_ID ********/
  var findDevicesById = function(req, res) {

    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {

      var query_devicesByProduction = {
        date_production: req.params.date_production,
        id: models.uuidFromString(req.params.device_id)
      }
      var options_devicesByProduction = {
        select: ['id', 'date_calibration', 'date_subscription_end', 'lat', 'long', 'characteristic', 'users'],
      }

      models.instance.devicesByProduction.findOne(query_devicesByProduction, options_devicesByProduction, function(err, device) {
        if (err) throw err;
        if (device) {
          res.status(200).json(device)
        } else {
          res.status(404).json({
            method: 'getDevicesByProduction',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getDevicesByProduction",
        message: "Unauthorized"
      });
    }

  };
  /******** GET DEVICES BY PRODUCTION ********/
  var findDevicesByProductionDate = function(req, res) {
    var devices = [];

    var _callback = {
      page_state_device: req.params.page_state_device
    }
    var query_devicesByProduction = {
      date_production: req.params.date_production
    }
    var options_devicesByProduction = {
      select: ['id', 'date_production', 'date_calibration', 'date_subscription_end', 'lat', 'long', 'characteristic', 'users'],
      fetchSize: 30
    }
    if (typeof req.params.page_state_device !== "undefined") {
      options_devicesByProduction.pageState = _callback.page_state_device;
    }
    models.instance.devicesByProduction.eachRow(query_devicesByProduction, options_devicesByProduction, function(n, row) {
      devices.push(row);
    }, function(err, device) {
      if (err) throw err;
      if (device) {
        if (devices.length > 0) {
          _callback.page_state_device = device.pageState;
          _callback.devices = devices;
          res.status(200).json(_callback);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({
          method: 'getDevicesByProduction',
          message: 'invalid params'
        })
      }
    });
  };
  /****************** UPDATE DEVICES **********************/

  var updateDevice = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.email) {
      if ((req.body.id) && (req.body.date_production)) {
        var query_devicesByProduction = {
          date_production: req.body.date_production,
          id: models.uuidFromString(req.body.id)
        }
        var value_devicesByProduction = {};

        if (typeof req.body.date_calibration !== "undefined") {
          value_devicesByProduction.date_calibration = moment(req.body.date_calibration).format('YYYY-MM-DD');
        }
        /** only admin
        if (typeof req.body.date_subscription_end !== "undefined") {
            value_devicesByProduction.date_subscription_end = moment(req.body.date_subscription_end).format('YYYY-MM-DD');
        }
        if (typeof req.body.characteristic !== "undefined") {
            value_devicesByProduction.characteristic = req.body.characteristic;
        }
        **/
        if ((typeof req.body.lat !== "undefined") && (typeof req.body.long !== "undefined")) {
          value_devicesByProduction.lat = req.body.lat;
          value_devicesByProduction.long = req.body.long;
        }

        if (typeof req.body.users !== "undefined") {
          value_devicesByProduction.users = {
            '$add': {
              [req.user.email]: moment().format('YYYY-MM-DD HH:mm:ss')
            }
          };
        }
        var update_device = function() {
          return new Promise(function(resolve, reject) {
            var _callback = {
              date_production: query_devicesByProduction.date_production,
              id: query_devicesByProduction.id,
              date_calibration: value_devicesByProduction.date_calibration,
              date_subscription_end: value_devicesByProduction.date_subscription_end,
              lat: value_devicesByProduction.lat,
              long: value_devicesByProduction.long,
              characteristic: value_devicesByProduction.characteristic,
              users: value_devicesByProduction.users
            }
            models.instance.devicesByProduction.update(query_devicesByProduction, value_devicesByProduction, function(err) {
              (err) ? reject(err): resolve(_callback);
            })
          });
        }
        update_device()
          .then(function(response) {
            res.status(200).json(response);
          })
          .catch(function(error) {
            console.error(error);
          });
      }
    } else {
      res.status(401).json({
        method: "getDevicesByProduction",
        message: "Unauthorized"
      });
    }
  };

  /****************** ADD DEVICES **********************/

  var addDevice = function(req, res) {
    if (req.body.date_production && req.body.characteristic) {
      var value_devicesByProduction = {
        date_production: moment(req.body.date_production).format('YYYY-MM-DD'),
        id: models.uuid(),
        date_calibration: models.datatypes.unset,
        date_subscription_end: models.datatypes.unset,
        lat: models.datatypes.unset,
        long: models.datatypes.unset,
        characteristic: req.body.characteristic,
        users: models.datatypes.unset
      }
      var save_device = function() {
        return new Promise(function(resolve, reject) {
          var _callback = {
            date_production: value_devicesByProduction.date_production,
            id: value_devicesByProduction.id,
            date_calibration: value_devicesByProduction.date_calibration,
            date_subscription_end: value_devicesByProduction.date_subscription_end,
            lat: value_devicesByProduction.lat,
            long: value_devicesByProduction.long,
            characteristic: value_devicesByProduction.characteristic,
            users: value_devicesByProduction.users
          }
          new models.instance.devicesByProduction(value_devicesByProduction).save(function(err) {
            (err) ? reject(err): resolve(_callback);
          });
        });
      }
      save_device()
        .then(function(response) {
          res.status(200).json(response);
        })
        .catch(function(error) {
          console.error(error);
        });
    }
  };

  /****************** RETURN DEVICES **********************/

  return {
    //android
    findDevicesById: findDevicesById,
    //end android

    //backend
    findDevicesByProductionDate: findDevicesByProductionDate,
    addDevice: addDevice,
    updateDevice: updateDevice
  }
}

module.exports = devicesController;
