'use strict';

var azure_config = require('../config/azure_storage.json');
var cyclesAzureAccount = azure_config.STORAGE_NAME_CYCLES;
var cyclesAzureKey = azure_config.STORAGE_KEY_CYCLES;
var samplesAzureAccount = azure_config.STORAGE_NAME_SAMPLES;
var samplesAzureKey = azure_config.STORAGE_KEY_SAMPLES;
var problemsAzureAccount = azure_config.STORAGE_NAME_PROBLEMS;
var problemsAzureKey = azure_config.STORAGE_KEY_PROBLEMS;

var path = require('path');
var streamifier = require('streamifier');
var azureStorage = require('azure-storage');
var moment = require('moment');

var filesController = function(models) {
  var uploadCycleImage = function(req, res, next) {
    if (req.user.id && req.user.join_date && req.user.cluster_id) {
      if (req.file && req.body.cycle_time_begin && req.body.id) {
        var blobsvc = azureStorage.createBlobService(cyclesAzureAccount, cyclesAzureKey);
        var createContainer = function() {
          var containerName = 'cycles-' + moment(req.body.cycle_time_begin).format('YYYY-MM-DD');
          return new Promise(function(resolve, reject) {
            blobsvc.createContainerIfNotExists(containerName, {
              publicAccessLevel: 'blob'
            }, function(error, result, response) {
              if (error) {
                reject(error);
              } else {
                if (result) {
                  resolve(containerName);
                } else {
                  resolve(containerName);
                }
              }
            });
          });
        }
        var fileStream = function(containerName) {
          return new Promise(function(resolve, reject) {
            var fileName = req.body.id + path.extname(req.file.originalname);
            var stream = streamifier.createReadStream(req.file.buffer);
            blobsvc.createBlockBlobFromStream(
              containerName,
              fileName,
              stream,
              req.file.size,
              function(error, result, response) {
                if (error) {
                  reject(error);
                } else {
                  resolve(fileName);
                }
              });
          });
        }
        createContainer().then(fileStream).then(function(fileName) {
            req.body.img_url = 'https://fishareblob.blob.core.windows.net/cycles-' + moment(req.body.cycle_time_begin).format('YYYY-MM-DD') + '/' + fileName;
            next();
          })
          .catch(function(error) {
            console.error(error);
            res.status(404).json({
              method: "uploadCycleImage",
              message: "failed"
            });
          })
      } else {
        res.status(404).json({
          method: "uploadCycleImage",
          message: "invalid params"
        });
      }

    } else {
      res.status(401).json({
        method: "uploadCycleImage",
        message: "Unauthorized"
      });
    }
  }
  var uploadSampleImage = function(req, res, next) {
    if (req.user.id && req.user.join_date && req.user.cluster_id) {
      if (req.body.cycle_time_begin && req.body.cycle_id && req.body.fish_species && req.body.crop_species && req.body.type && req.body.species && req.body.producer && req.body.samples) {
        //normalize
        req.body.date_taken = moment().format('YYYY-MM-DD');
        if (req.files) {
          var blobsvc = azureStorage.createBlobService(samplesAzureAccount, samplesAzureKey);
          var createContainer = function() {
            var containerName = 'samples-' + req.body.date_taken + '-' + req.body.type.toLowerCase() + '-' + req.body.species.replace(/\s+/g, '').toLowerCase();
            return new Promise(function(resolve, reject) {
              blobsvc.createContainerIfNotExists(containerName, function(error, result, response) {
                if (error) {
                  reject(error);
                } else {
                  if (result) {
                    resolve(containerName);
                  } else {
                    resolve(containerName);
                  }
                }
              });
            });
          }
          var fileStream = function(containerName, fileName, i) {

            var stream = streamifier.createReadStream(req.files[i].buffer);
            blobsvc.createBlockBlobFromStream(
              containerName,
              fileName,
              stream,
              req.files[i].size,
              function(error, result, response) {
                if (error) {
                  console.error(error);
                }
              });
          }
          var loop_samples = function(containerName) {
            var i = 0;
            return new Promise(function(resolve, reject) {
              req.files.forEach(function() {
                var event_time = models.timeuuidFromDate(moment());
                var fileName = req.body.producer.replace(/\s+/g, '').toLowerCase() + '-' + event_time.toString() + path.extname(req.files[i].originalname);
                req.body.samples[i].event_time = event_time;
                req.body.samples[i].img_url = 'https://fisharesamplesblob.blob.core.windows.net/' + containerName + '/' + fileName;
                fileStream(containerName, fileName, i);
                i++;
                if (i === req.files.length) {
                  resolve();
                }
              });

            });
          }
          createContainer().then(function(containerName) {
              loop_samples(containerName).then(function(response) {
                  next();
                })
                .catch(function(error) {
                  console.error(error);
                });
            })
            .catch(function(error) {
              console.error(error);
            });

        } else {
          //timbangan normalize
          var unit = 0;
          if (req.body.samples[0].unit.toLowerCase() === "kg") {
            unit = 1000;
          } else {
            unit = 1;
          }
          req.body.samples[0] = {
            length: req.body.samples[0].length,
            width: req.body.samples[0].width,
            weight: req.body.samples[0].weight * unit / req.body.samples[0].quantity
          }
          //no samples images
          next();
        }
      } else {
        res.status(404).json({
          method: "uploadSampleImage",
          message: "invalid params"
        });
      }

    } else {
      res.status(401).json({
        method: "uploadSampleImage",
        message: "Unauthorized"
      });
    }

  }
  var uploadProblemImage = function(req, res, next) {
    if (req.user.id && req.user.join_date && req.user.cluster_id) {
      if (req.file) {
        if (req.file && req.body.cycle_time_begin && req.body.cycle_id) {
          var blobsvc = azureStorage.createBlobService(problemsAzureAccount, problemsAzureKey);
          var createContainer = function() {
            var containerName = 'problems-' + moment(req.body.cycle_time_begin).format('YYYY-MM-DD');
            return new Promise(function(resolve, reject) {
              blobsvc.createContainerIfNotExists(containerName, {
                publicAccessLevel: 'blob'
              }, function(error, result, response) {
                if (error) {
                  reject(error);
                } else {
                  if (result) {
                    resolve(containerName);
                  } else {
                    resolve(containerName);
                  }
                }
              });
            });
          }
          var fileStream = function(containerName) {
            return new Promise(function(resolve, reject) {

              var fileName = problem_id + path.extname(req.file.originalname);
              var stream = streamifier.createReadStream(req.file.buffer);
              blobsvc.createBlockBlobFromStream(
                containerName,
                fileName,
                stream,
                req.file.size,
                function(error, result, response) {
                  if (error) {
                    reject(error);
                  } else {
                    resolve(fileName);
                  }
                });
            });
          }
          var problem_id = models.uuid();
          createContainer().then(fileStream).then(function(fileName) {
              req.body.id = problem_id;
              req.body.img_url = 'https://fishareproblemsblob.blob.core.windows.net/problems-' + moment(req.body.cycle_time_begin).format('YYYY-MM-DD') + '/' + fileName;
              next();
            })
            .catch(function(error) {
              console.error(error);
              res.status(404).json({
                method: "uploadProblemImage",
                message: "failed"
              });
            })
        } else {
          res.status(404).json({
            method: "uploadProblemImage",
            message: "invalid params"
          });
        }
      } else {
        req.body.id = models.uuid();
        req.body.img_url = models.datatypes.unset;
        next();
      }
    }
  }
  return {
    uploadCycleImage: uploadCycleImage,
    uploadSampleImage: uploadSampleImage,
    uploadProblemImage: uploadProblemImage
  }
}

module.exports = filesController;
