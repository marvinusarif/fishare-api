'use strict';

var moment = require('moment');
var samplesController = function(models) {
  Array.prototype.inArray = function(value) {
    // Returns true if the passed value is found in the
    // array. Returns false if it is not.
    var i;
    for (i = 0; i < this.length; i++) {
      if (this[i] == value) {
        return true;
      }
    }
    return false;
  };
  /************* GET SAMPLES BY CYCLE *************/
  var getSamplesByCycle = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var samples = [];
      var _callback = {
        page_state_samples: req.params.page_state_sample
      }
      var query_samplesByCycle = {
        cycle_time_begin: req.params.cycle_time_begin,
        cycle_id: models.uuidFromString(req.params.cycle_id),
        type: req.params.type
      }
      var options_samplesByCycle = {
        select: ['toTimestamp(event_time)', 'weight', 'width', 'length', 'type', 'species'],
        fetchSize: 100
      }
      if (typeof req.params.page_state_sample !== "undefined") {
        options_samplesByCycle.pageState = _callback.page_state_samples;
      }
      var event_time_temp = [];
      var i = 0;
      models.instance.samplesByCycle.eachRow(query_samplesByCycle, options_samplesByCycle, function(n, row) {
        var event_time = moment(row["system.totimestamp(event_time)"]).format('YYYY-MM-DD HH:mm:00');
        if (event_time_temp.inArray(event_time) === false) {
          //delete object
          delete row["system.totimestamp(event_time)"];
          row.event_time = event_time;
          i = 1;
          event_time_temp.push(event_time);
          samples.push(row);
        } else {
          samples[samples.length - 1]['length'] =
            (samples[samples.length - 1]['length'] * i + row.length) / (i + 1);
          samples[samples.length - 1]['width'] =
            (samples[samples.length - 1]['width'] * i + row.width) / (i + 1);
          samples[samples.length - 1]['weight'] =
            (samples[samples.length - 1]['weight'] * i + row.weight) / (i + 1);
          i++;

        }

      }, function(err, sample) {
        if (err) throw err;
        if (sample) {
          if (samples.length > 0) {
            _callback.page_state_samples = sample.pageState;
            _callback.samples = samples;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getSamplesByCycle',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getSamplesByCycle",
        message: "Unauthorized"
      })
    }

  };

  var getWeightBySpecies = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      if (req.body.type && req.body.species && req.body.length && req.body.width) {
        var weight = -1;
        var _callback = {
          type: req.body.type,
          species: req.body.species,
          length: req.body.length,
          width: req.body.width,
          weight: weight
        }
        res.status(200).json(_callback);
      } else {
        res.status(404).json({
          method: "getWeightBySpecies",
          message: "invalid params"
        });
      }
    } else {
      res.status(401).json({
        method: "getWeightBySpecies",
        message: "Unauthorized"
      });
    }
  }

  /************* ADD SAMPLES *************/

  var addSample = function(req, res, next) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      //normalize
      req.body.id = req.body.cycle_id;
      var save_samples = function() {
        var queries = [];
        return new Promise(function(resolve, reject) {
          var i = 0;
          var length_temp = 0;
          var width_temp = 0;
          var weight_temp = 0;
          req.body.samples.forEach(function(_sample) {
            if (typeof _sample.event_time === 'undefined') {
              _sample.event_time = models.timeuuidFromDate(moment());
            }
            if (typeof _sample.orientation === 'undefined') {
              _sample.orientation = '-1';
            }
            if (typeof _sample.img_url === "undefined") {
              _sample.img_url = models.datatypes.unset;
            }
            var value_samplesByCycle = {
              cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
              cycle_id: models.uuidFromString(req.body.cycle_id),
              event_time: _sample.event_time,
              type: req.body.type,
              species: req.body.species,
              length: parseFloat(_sample.length),
              width: parseFloat(_sample.width),
              weight: parseFloat(_sample.weight)
            }

            var value_samplesBySpecies_Meta = {
              type: value_samplesByCycle.type,
              species: value_samplesByCycle.species,
              producer: req.body.producer,
              date_taken: req.body.date_taken
            }

            var value_samplesBySpecies = {
              date_taken: value_samplesBySpecies_Meta.date_taken,
              type: value_samplesByCycle.type,
              species: value_samplesByCycle.species,
              producer: value_samplesBySpecies_Meta.producer,
              orientation: _sample.orientation,
              event_time: value_samplesByCycle.event_time,
              length: value_samplesByCycle.length,
              width: value_samplesByCycle.width,
              weight: value_samplesByCycle.weight,
              img_url: _sample.img_url
            }
            var save_samplesBySpecies_Meta = new models.instance.samplesBySpecies_Meta(value_samplesBySpecies_Meta).save({
              return_query: true
            });
            queries.push(save_samplesBySpecies_Meta);

            var save_samplesByCycle = new models.instance.samplesByCycle(value_samplesByCycle).save({
              return_query: true
            });
            queries.push(save_samplesByCycle);

            var save_samplesBySpecies = new models.instance.samplesBySpecies(value_samplesBySpecies).save({
              return_query: true
            });
            queries.push(save_samplesBySpecies);

            i++;
            length_temp += value_samplesByCycle.length;
            width_temp += value_samplesByCycle.width;
            weight_temp += value_samplesByCycle.weight;

            if (i === req.body.samples.length) {
              //calculate average
              length_temp = length_temp / i;
              weight_temp = weight_temp / i;
              width_temp = width_temp / i;
              req.body.sample = {};
              req.body.sample[value_samplesBySpecies_Meta.type.toLowerCase()] = {
                date_taken: value_samplesBySpecies_Meta.date_taken,
                species: value_samplesBySpecies_Meta.species,
                length: length_temp,
                width: width_temp,
                weight: weight_temp
              };
              req.body.sample_date_taken = value_samplesBySpecies_Meta.date_taken;
              models.doBatch(queries, function(err) {
                (err) ? reject(err): resolve(req);
              });
            }
          });
        });
      }

      save_samples()
        .then(function() {
          next();
        })
        .catch(function(err) {
          console.error(err);
        });

    } else {
      res.status(401).json({
        method: "addSamples",
        message: "Unauthorized"
      })
    }
  };

  /****** GET SAMPLES BY SPECIES PRODUCER TIME BEGIN-END METAMERGED ******/
  var getSamplesBySpecies_Producer_TimeBeginEnd_MetaMerged = function(req, res) {


    var _callback = {
      page_state_meta_sample: req.params.page_state_meta_sample
    }

    var query_samplesBySpecies_Meta = {
      type: req.params.type,
      species: req.params.species,
      producer: req.params.producer,
      date_taken: {
        '$gte': req.params.date_taken_begin,
        '$lte': req.params.date_taken_end
      }
    }
    var options_samplesBySpecies_Meta = {
      select: ['date_taken'],
      raw: true,
      fetchSize: 15
    }

    if (typeof req.params.page_state_meta_sample !== "undefined") {
      options_samplesBySpecies_Meta.pageState = _callback.page_state_meta_sample;
    }

    var get_meta_samples = function(callback) {
      var meta_samples = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.samplesBySpecies_Meta.eachRow(query_samplesBySpecies_Meta, options_samplesBySpecies_Meta, function(n, row) {
          meta_samples.push(row);
        }, function(err, meta_sample) {
          if (err) reject(err);
          if (meta_sample) {
            _callback.page_state_meta_sample = meta_sample.pageState;
            _callback.meta_samples = meta_samples;
            _callback.samples = [];
            resolve(_callback);
          }
        });
      });
      promise.then(function(response) {
        callback(_callback);
      }, function(error) {
        console.error(error);
      });
    }
    var get_samples = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        _callback.meta_samples.forEach(function(meta_sample) {
          var query_samplesBySpecies = {
            type: req.params.type,
            species: req.params.species,
            producer: req.params.producer,
            date_taken: meta_sample.date_taken
          }
          var options_samplesBySpecies = {
            raw: true
          }
          models.instance.samplesBySpecies.eachRow(query_samplesBySpecies, options_samplesBySpecies, function(n, row) {
            _callback.samples.push(row);
          }, function(err, sample) {
            if (err) reject(err);
            if (samples) {
              i++;
              if (i === _callback.meta_samples.length) {
                resolve(_callback);
              }
            }
          });
        });
      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      })
    }
    get_meta_samples(get_samples);
  }
  /***************** RETURN SAMPLES ******************/
  return {
    //android
    getSamplesByCycle: getSamplesByCycle,
    getWeightBySpecies: getWeightBySpecies,
    addSample: addSample,
    //end android

    //backend
    getSamplesBySpecies_Producer_TimeBeginEnd_MetaMerged: getSamplesBySpecies_Producer_TimeBeginEnd_MetaMerged
  }
}

module.exports = samplesController;
