'use strict';

var randomstring = require('randomstring');
var mailer = require('./emailController');
var sms = require('./smsController');
var hash = require('./../lib/hash');
var moment = require('moment');

var usersController = function(models) {
  /************************* AUTH ******************************/
  var check = function(req, res) {
    if ((req.user.cluster_id) && (req.user.id) && (req.user.join_date)) {
      var query_usersByCluster = {
        cluster_id: req.user.cluster_id,
        id: req.user.id,
        join_date: req.user.join_date
      };
      var usersByCluster_options = {
        select : ['email', 'join_date', 'gender']
      }
      models.instance.usersByCluster.findOne(query_usersByCluster, usersByCluster_options, function(err, rec) {
        if (err)
          throw err;
        if (rec) {
          res.status(200).json(rec);
        } else {
          res.status(404).json({method: "getProfile", message: "invalid params"});
        }
      });
    } else {
      res.status(401).json({method: "getProfile", message: "Unauthorized"})
    }
  }
  var login = function(req, res) {
    if (req.body.email && req.body.password) {

      var query_usersByEmail = {
        email_bucket: req.body.email.substr(0, 3).toUpperCase(),
        email: req.body.email.toUpperCase()
      }

      var options_usersByEmail = {
        select: [
          'id',
          'cluster_id',
          'email',
          'password',
          'join_date',
          'country',
          'full_name',
          'verification_email',
          'verification_phone'
        ],
        limit: 1
      }
      models.instance.usersByEmail.findOne(query_usersByEmail, options_usersByEmail, function(err, rec) {
        if (err)
          console.error(err);
        if (rec) {
          hash.compare(req.body.password, rec.password).then(function(isMatch) {
            if (isMatch) {
              var verification_email,
                verification_phone;
              (rec.verification_email !== null)
                ? verification_email = true
                : verification_email = false;

              (rec.verification_phone !== null)
                ? verification_phone = true
                : verification_phone = false;

              var _cb = {
                id: rec.id,
                cluster_id: rec.cluster_id,
                email: rec.email,
                join_date: rec.join_date,
                country: rec.country,
                full_name: rec.full_name,
                verification_email: verification_email,
                verification_phone: verification_phone
              }

              res.status(200).json(_cb);
            } else {
              res.status(401).json({method: 'login', message: 'invalid params'});
            }
          }).catch(function(error) {
            console.error(error);
          })
        } else {
          res.status(401).json({method: 'login', message: 'invalid params'});
        }
      });
    }

  }
  /************************* GET EMAIL *************************/

  var getEmail = function(req, res) {
    var query_usersByEmail = {
      email_bucket: req.params.email.substr(0, 3).toUpperCase(),
      email: req.params.email.toUpperCase()
    }
    var option_usersByEmail = {
      select: ['email'],
      limit: '1'
    }
    models.instance.usersByEmail.findOne(query_usersByEmail, option_usersByEmail, function(err, rec) {
      if (err)
        throw err;

      if (rec) {
        var _callback = {
          exists: true
        };
      } else {
        var _callback = {
          exists: false
        };
      }
      res.status(200).json(_callback);
    })
  };
  /************************* GET PROFILE *************************/

  var getProfile = function(req, res) {
    if ((req.user.cluster_id) && (req.user.id) && (req.user.join_date)) {
      var query_usersByCluster = {
        cluster_id: req.user.cluster_id,
        id: req.user.id,
        join_date: req.user.join_date
      };

      models.instance.usersByCluster.findOne(query_usersByCluster, function(err, rec) {
        if (err)
          throw err;
        if (rec) {
          res.status(200).json(rec);
        } else {
          res.status(404).json({method: "getProfile", message: "invalid params"});
        }
      });
    } else {
      res.status(401).json({method: "getProfile", message: "Unauthorized"})
    }

  };
  /************************* ADD USER *************************/

  var addUser = function(req, res, next) {
    // UPDATE
    /***
        SEE json/users.json
    ***/
    //insert
    var value_usersByCluster = {
      cluster_id: models.uuidFromString(req.body.cluster_id),
      join_date: moment().format('YYYY-MM-DD'),
      id: models.uuid(),
      gender: req.body.gender,
      email: req.body.email.toUpperCase(),
      birthdate: req.body.birthdate,
      full_name: {
        first_name: req.body.full_name.first_name.toUpperCase(),
        last_name: req.body.full_name.last_name.toUpperCase()
      },
      address: req.body.address,
      phone: req.body.phone,
      country: req.body.address['home'].country,
      verification_phone: models.datatypes.unset,
      verification_email: models.datatypes.unset
    }

    var value_usersByEmail = {
      email_bucket: value_usersByCluster.email.substr(0, 3),
      email: value_usersByCluster.email,
      password: req.body.password,
      id: value_usersByCluster.id,
      cluster_id: value_usersByCluster.cluster_id,
      full_name: value_usersByCluster.full_name,
      birthdate: value_usersByCluster.birthdate,
      country: value_usersByCluster.country,
      gender: value_usersByCluster.gender,
      join_date: value_usersByCluster.join_date,
      verification_phone: models.datatypes.unset,
      verification_email: models.datatypes.unset
    }

    var save_user = function(password) {
      return new Promise(function(resolve, reject) {
        var queries = [];
        value_usersByEmail.password = password;
        var save_usersByEmail = new models.instance.usersByEmail(value_usersByEmail).save({return_query: true});
        queries.push(save_usersByEmail);

        var save_usersByCluster = new models.instance.usersByCluster(value_usersByCluster).save({return_query: true});
        queries.push(save_usersByCluster);

        //end insert
        var _callback = {
          id: value_usersByCluster.id,
          email: value_usersByEmail.email,
          gender: value_usersByCluster.gender,
          join_date: value_usersByCluster.join_date,
          full_name: value_usersByCluster.full_name,
          birthdate: value_usersByCluster.birthdate,
          country: value_usersByCluster.country,
          cluster_id: value_usersByCluster.cluster_id
        }

        models.doBatch(queries, function(err) {
          (err)
            ? reject(err)
            : resolve(_callback);
        });
      });
    }

    if (typeof req.body.password !== "undefined") {
      hash.hashing(req.body.password).then(save_user).then(function(callback) {
        req.body.callback = callback;
        next();
      }).catch(function(error) {
        console.error(error);
      })
    };
  }

  var sendEmailVerification = function(req, res) {
    if (typeof req.body.callback !== "undefined") {
      //first email sent after registration
      hash.hashing(req.body.callback.email).then(function(token_email) {
        var value_verificationByEmail = {
          email_bucket: req.body.callback.email.substr(0, 3),
          email: req.body.callback.email,
          token_date: moment().format('YYYY-MM-DD HH:mm:ss'),
          first_name: req.body.callback.full_name.first_name,
          last_name: req.body.callback.full_name.last_name,
          token: token_email
        }
        new models.instance.verificationByEmail(value_verificationByEmail).save();

        //from addUser - First try of sending mail
        var tokenOptions = {
          email: req.body.callback.email,
          token_url: req.protocol + '://' + req.hostname + '/users/verifyEmail?email=' + req.body.callback.email + '&token=' + token_email,
          first_name: req.body.callback.full_name.first_name,
          last_name: req.body.callback.full_name.last_name
        }
        mailer.sendVerificationEmail(tokenOptions);
        res.status(200).json(req.body.callback);
      }).catch(function(error) {
        console.error(error);
      })
    } else if (req.user.email && req.user.id && req.user.join_date && (req.user.verification_email === false)) {
      //for resend verification email
      hash.hashing(req.user.email).then(function(token_email) {
        //update token
        var value_verificationByEmail = {
          email_bucket: req.user.email.substr(0, 3).toUpperCase(),
          email: req.user.email.toUpperCase(),
          first_name: req.user.full_name.first_name,
          last_name: req.user.full_name.last_name,
          token_date: moment().format('YYYY-MM-DD'),
          token: token_email
        }

        new models.instance.verificationByEmail(value_verificationByEmail).save();

        //send email
        var tokenOptions = {
          email: req.user.email,
          token_url: req.protocol + '://' + req.hostname + '/users/verifyEmail?email=' + req.user.email + '&token=' + token_email,
          first_name: req.user.full_name.first_name,
          last_name: req.user.full_name.last_name
        }
        mailer.sendVerificationEmail(tokenOptions);
        res.status(200).json({method: 'resendEmailVerification', message: 'success'});
      });
    } else {
      res.status(400).json({method: 'resendEmailVerification', message: 'invalid params'});
    }
  }
  var verifyEmail = function(req, res, next) {
    var query_verificationByEmail = {
      email_bucket: req.query.email.substr(0, 3).toUpperCase(),
      email: req.query.email.toUpperCase()
    };
    models.instance.verificationByEmail.findOne(query_verificationByEmail, function(err, rec) {
      if (err) {
        console.error(err)
      }
      if (rec) {
        if (rec.token === req.query.token) {
          next();
        } else {
          res.status(400).json({method: 'verifyEmail', message: 'invalid token'});
        }
      }
    });
  }

  var confirmEmail = function(req, res) {
    var query_usersByEmail = {
      email_bucket: req.query.email.substr(0, 3).toUpperCase(),
      email: req.query.email.toUpperCase()
    }
    var options_usersByEmail = {
      select: ['cluster_id', 'id', 'join_date', 'verification_email']
    }
    models.instance.usersByEmail.findOneAsync(query_usersByEmail, options_usersByEmail).then(function(rec) {
      if (rec.verification_email === null) {
        var queries = [];
        var query_usersByEmail = {
          email_bucket: req.query.email.substr(0, 3).toUpperCase(),
          email: req.query.email.toUpperCase()
        }
        var value_usersByEmail = {
          verification_email: moment().format('YYYY-MM-DD HH:mm:ss')
        }
        var update_usersByEmail = models.instance.usersByEmail.update(query_usersByEmail, value_usersByEmail, {return_query: true});
        queries.push(update_usersByEmail);

        var query_usersByCluster = {
          cluster_id: rec.cluster_id,
          join_date: rec.join_date,
          id: rec.id
        }
        var value_usersByCluster = {
          verification_email: value_usersByEmail.verification_email
        }
        var update_usersByCluster = models.instance.usersByCluster.update(query_usersByCluster, value_usersByCluster, {return_query: true});
        queries.push(update_usersByCluster);

        models.doBatch(queries, function(err) {
          if (err) {
            console.error(error);
          } else {
            res.status(200).json({method: 'verifyEmail', message: 'success'});
          }
        })
      } else {
        res.status(400).json({method: 'verifyEmail', message: 'Email has been verified'});
      }
    }).catch(function(error) {
      console.error(error);
    });
  }

  /************************* UPDATE USER *************************/

  var updateUser = function(req, res) {
    // update user ?
    if ((req.user.id) && (req.user.join_date) && (req.user.cluster_id) && (req.user.email)) {
      //update
      var query_usersByCluster = {
        cluster_id: req.user.cluster_id,
        join_date: moment(req.user.join_date).format('YYYY-MM-DD'),
        id: req.user.id
      }
      var value_usersByCluster = {
        gender: req.body.gender,
        email: req.user.email.toUpperCase(),
        birthdate: req.body.birthdate,
        full_name: {
          first_name: req.body.full_name.first_name.toUpperCase(),
          last_name: req.body.full_name.last_name.toUpperCase()
        },
        address: req.body.address,
        phone: req.body.phone,
        identification: req.body.identification,
        profile: req.body.profile,
        social_media: req.body.social_media,
        lat: req.body.lat,
        long: req.body.long,
        country: req.body.address['home'].country
      }

      var query_usersByEmail = {
        email_bucket: value_usersByCluster.email.substr(0, 3),
        email: value_usersByCluster.email
      }

      var value_usersByEmail = {
        id: query_usersByCluster.id,
        cluster_id: query_usersByCluster.cluster_id,
        full_name: value_usersByCluster.full_name,
        birthdate: value_usersByCluster.birthdate,
        country: value_usersByCluster.country,
        gender: value_usersByCluster.gender,
        join_date: query_usersByCluster.join_date
      }

      if (typeof req.body.verification_phone !== "undefined") {
        value_usersByCluster.verification_phone = models.datatypes.unset;
        value_usersByEmail.verification_phone = models.datatypes.unset;
      }

      var user_update = function() {
        return new Promise(function(resolve, reject) {
          var queries = [];
          var update_usersByEmail = models.instance.usersByEmail.update(query_usersByEmail, value_usersByEmail, {return_query: true});
          queries.push(update_usersByEmail);

          var update_usersByCluster = models.instance.usersByCluster.update(query_usersByCluster, value_usersByCluster, {return_query: true});
          queries.push(update_usersByCluster);

          var _callback = {
            id: query_usersByCluster.id,
            email: value_usersByEmail.email,
            gender: value_usersByCluster.gender,
            join_date: query_usersByCluster.join_date,
            full_name: value_usersByCluster.full_name,
            birthdate: value_usersByCluster.birthdate,
            country: value_usersByCluster.country,
            cluster_id: query_usersByCluster.cluster_id
          }

          models.doBatch(queries, function(err) {
            (err)
              ? reject(err)
              : resolve(_callback);
          });
        });
      }

      user_update().then(function(response) {
        res.status(200).json(response);
      }).catch(function(error) {
        console.error(error);
      });
      //end update
    } else {
      res.status(401).json({method: "sendPhoneVerification", message: "Unauthorized"})
    }
  };
  /************************* RETURN USER *************************/
  var sendPhoneVerification = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.cluster_id && req.user.email && (req.user.verification_phone === false) && req.body.phone) {
      var token = randomstring.generate({length: 4, charset: 'numeric'});
      var query_verificationByPhone = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        email: req.user.email.toUpperCase(),
        phone: req.body.phone.number,
        country_code: req.body.phone.country_code
      }
      //save into
      models.instance.verificationByPhone.findOne(query_verificationByPhone, function(err, rec) {
        if (err)
          console.error(err);
        if (rec) {
          var current_time = moment();
          var last_time = moment(rec.token_date);
          var time_span = current_time.diff(last_time, 'seconds');
          var time_limit = 90; //in seconds
          if (time_span > time_limit) {
            //second verification
            rec.token = token;
            rec.token_date = moment().format('YYYY-MM-DD HH:mm:ss');
            rec.save(function(err) {
              if (err)
                console.error(err);
              }
            );
            sms.sendVerificationSMS(req.body.phone, token);
            res.status(200).json({method: 'sendPhoneVerification', message: 'success'});
          } else {
            res.status(403).json({
              method: 'sendPhoneVerification',
              message: 'The sms verification only can be generated in ' + (time_limit - time_span) + 'seconds'
            });
          }
        } else {
          //first verification
          var value_verificationByPhone = {
            email_bucket: req.user.email.substr(0, 3).toUpperCase(),
            email: req.user.email.toUpperCase(),
            phone: req.body.phone.number,
            country_code: req.body.phone.country_code,
            token: token,
            token_date: moment().format('YYYY-MM-DD HH:mm:ss')
          }

          new models.instance.verificationByPhone(value_verificationByPhone).save(function(err) {
            if (err)
              console.error(err);
            }
          );
          sms.sendVerificationSMS(req.body.phone, token);
          res.status(200).json({method: 'sendPhoneVerification', message: 'success'});
        }
      })

    } else {
      res.status(401).json({method: "sendPhoneVerification", message: "Unauthorized"})
    }
  }

  var verifyPhone = function(req, res, next) {
    if (req.user.id && req.user.join_date && req.user.cluster_id && (req.user.verification_phone === false)) {

      var query_verificationByPhone = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        email: req.user.email.toUpperCase(),
        phone: req.body.phone.number,
        country_code: req.body.phone.country_code
      }
      models.instance.verificationByPhone.findOne(query_verificationByPhone, function(err, rec) {
        if (err)
          console.error(err);
        if (rec) {
          if (rec.token === req.body.token) {
            next();
          } else {
            res.status(400).json({method: "verifyPhone", message: "invalid token"});
          }
        } else {
          res.status(400).json({method: "verifyPhone", message: "invalid params"});
        }
      });
    } else {
      res.status(401).json({method: "verifyPhone", message: "Unauthorized"})
    }
  }

  var confirmPhone = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.cluster_id && (req.user.verification_phone === false)) {
      var queries = [];

      var query_usersByEmail = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        email: req.user.email.toUpperCase()
      }

      var value_usersByEmail = {
        verification_phone: moment().format('YYYY-MM-DD HH:mm:ss')
      }
      var update_usersByEmail = models.instance.usersByEmail.update(query_usersByEmail, value_usersByEmail, {return_query: true});
      queries.push(update_usersByEmail);

      var query_usersByCluster = {
        cluster_id: req.user.cluster_id,
        join_date: req.user.join_date,
        id: req.user.id
      }

      var value_usersByCluster = {
        verification_phone: value_usersByEmail.verification_phone
      }
      var update_usersByCluster = models.instance.usersByCluster.update(query_usersByCluster, value_usersByCluster, {return_query: true});
      queries.push(update_usersByCluster);

      models.doBatch(queries, function(err) {
        res.status(200).json({method: "verifyPhone", message: "success"});
      });
    } else {
      res.status(401).json({method: "verifyPhone", message: "Unauthorized"});
    }

  }
  var updatePassword = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.email) {
      var query_usersByEmail = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        email: req.user.email.toUpperCase()
      }
      models.instance.usersByEmail.findOne(query_usersByEmail, function(err, rec) {
        if (err)
          console.error(err);
        if (rec) {
          if (req.body.password !== req.body.password_old) {
            //compare old password
            hash.compare(req.body.password_old, rec.password).then(function(isMatch) {
              if (isMatch && (typeof req.body.password !== "undefined")) {
                if (req.body.password.length > 7) {
                  hash.hashing(req.body.password).then(function(password) {
                    rec.password = password;
                    rec.save(function(err) {
                      if (err)
                        console.error(err);
                      }
                    );
                    res.status(200).json({method: 'updatePassword', message: 'Password has been successfully updated'});
                  });
                } else {
                  res.status(400).json({method: 'updatePassword', message: 'Password must be 8 character in length'});
                }

              } else {
                res.status(400).json({method: 'updatePassword', message: 'Your current password does not match'});
              }
            });
          } else {
            res.status(400).json({method: 'updatePassword', message: 'Your new password must not be same with your current password'});
          }
        }
      });
    } else {
      res.status(401).json({method: 'updatePassword', message: 'Unauthorized'});
    }

  }
  var resetPassword = function(req, res) {
    if (req.body.email && req.body.birthdate && req.body.first_name && req.body.last_name) {
      var query_usersByEmail = {
        email_bucket: req.body.email.substr(0, 3).toUpperCase(),
        email: req.body.email.toUpperCase()
      }
      models.instance.usersByEmail.findOne(query_usersByEmail, function(err, rec) {
        if (err)
          console.error(err);
        if (rec) {
          //check firstname& lastname & birthdate
          if ((rec.full_name.first_name === req.body.first_name.toUpperCase()) && (rec.full_name.last_name === req.body.last_name.toUpperCase()) && moment(rec.birthdate).format('YYYY-MM-DD') === moment(req.body.birthdate).format('YYYY-MM-DD')) {
            //generate random token
            var token = randomstring.generate(12);
            //hash
            hash.hashing(token).then(function(hash) {
              rec.password = hash;
              rec.save(function(err) {
                if (err) {
                  console.error(error);
                } else {
                  var tokenOptions = {
                    email: rec.email,
                    first_name: rec.full_name.first_name,
                    last_name: rec.full_name.last_name,
                    token: token
                  }

                  mailer.sendResetPasswordEmail(tokenOptions);
                  res.status(200).json({method: 'resetPassword', message: 'success'});
                }
              });
            }).catch(function(error) {
              console.error(error);
            });
          } else {
            res.status(400).json({method: 'resetPassword', message: 'email account is not valid'});
          }
        } else {
          res.status(400).json({method: 'resetPassword', message: 'email account is not valid'});
        }
      });
    } else {
      res.status(404).json({method: 'resetPassword', message: 'invalid params'});
    }

  }

  return {
    //android
    check : check,
    login: login,
    getEmail: getEmail,
    getProfile: getProfile,
    addUser: addUser,
    updateUser: updateUser,

    sendEmailVerification: sendEmailVerification,
    verifyEmail: verifyEmail,
    confirmEmail: confirmEmail,

    sendPhoneVerification: sendPhoneVerification,
    verifyPhone: verifyPhone,
    confirmPhone: confirmPhone,

    updatePassword: updatePassword,
    resetPassword: resetPassword

    //end android

    //backend
  }
}

module.exports = usersController;
