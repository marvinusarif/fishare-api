var nodemailer = require('nodemailer');
var hogan = require('hogan.js');
var fs = require('fs');

var templateEmailVerification = fs.readFileSync('./views/verificationEmail.html', 'utf-8');
var templateResetPassword = fs.readFileSync('./views/resetPassword.html', 'utf-8');

var compiledEmailVerification = hogan.compile(templateEmailVerification);
var compiledResetPassword = hogan.compile(templateResetPassword);

var emailController = function() {

  /* comment this since we use mandrill as smtp server
  let poolConfig = {
      pool: true,
      host: 'mail.fishare.asia',
      port: 465,
      secure: true, // use TLS
      tls: {
          rejectUnauthorized: false
      },
      auth: {
          user: 'no-reply@fishare.asia',
          pass: '69l]kCcNdJ69'
      }
  }
  */

  var transporter = nodemailer.createTransport({
    service: 'Mandrill',
    auth: {
      user: 'Fishare',
      pass: 'w75eNgRyVE7rYXN8y8QvTw'
    }
  }, {
    from: 'Desi - Fishare <no-reply@fishare.asia>'
  });

  var verifyTransporter = function() {
    return new Promise(function(resolve, reject) {
      transporter.verify(function(error, success) {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  var sendVerificationEmail = function(tokenOptions) {
    verifyTransporter()
      .then(function() {
        var template_value = {
          firstName: tokenOptions.first_name,
          lastName: tokenOptions.last_name,
          tokenUrl: tokenOptions.token_url,
          messageBody: 'Terima kasih telah bergabung bersama kami. Demi menciptakan keamanan akun pribadi anda, mohon klik tombol dibawah ini untuk memverifikasi akun anda. ',
          buttonVerification: 'Klik disini Untuk Verifikasi Email Sekarang',
          messageFooter: 'Selamat bergabung di fishare.asia',
          companyName: 'PT FISHARE INOVASI NUSANTARA',
          companyAddress: 'Intiland Annexe Building 8th Floor, Jl. Jend Sudirman Kav 32. Jakarta Pusat 10220',
          companyPhone: '+62 82 110267895'
        }
        var mailOptions = {
          to: tokenOptions.email,
          subject: 'Verifikasi Akun Anda Sekarang',
          html: compiledEmailVerification.render(template_value)
        }
        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.error(error);
          } else {
            console.log(info);
          }
        });
      })
      .catch(function(error) {
        console.error(error);
      })

  }
  var sendResetPasswordEmail = function(tokenOptions) {
    verifyTransporter()
      .then(function() {
        var template_value = {
          firstName: tokenOptions.first_name,
          lastName: tokenOptions.last_name,
          messageBody: 'Anda baru saja merubah kata sandi. Kata sandi yang baru tercetak dibawah ini: ',
          passwordNew: tokenOptions.token,
          messageFooter: 'Mohon gunakan kata sandi yang baru  untuk masuk ke dalam aplikasi. Selanjutnya anda dapat merubah lagi kata sandi sesuai dengan profil anda.',
          companyName: 'PT FISHARE INOVASI NUSANTARA',
          companyAddress: 'Intiland Annexe Building 8th Floor, Jl. Jend Sudirman Kav 32. Jakarta Pusat 10220',
          companyPhone: '+62 82 110267895'
        }
        var mailOptions = {
          to: tokenOptions.email,
          subject: 'Reset Password',
          html: compiledResetPassword.render(template_value)
        }
        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.error(error);
          } else {
            console.log(info);
          }
        });
      })
      .catch(function(error) {
        console.error(error);
      });
  }
  return {
    sendVerificationEmail: sendVerificationEmail,
    sendResetPasswordEmail: sendResetPasswordEmail
  }
}

module.exports = emailController();
