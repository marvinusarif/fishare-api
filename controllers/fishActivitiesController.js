'use strict';

var moment = require('moment');
var fishActivitiesController = function(models) {
  /************* ADD fISH ACTIVITIES *************/

  var addFishActivities = function(req, res) {
    if ((req.user.id) && (req.user.join_date) && (req.user.cluster_id)) {
      var save_fishactivities = function() {
        return new Promise(function(resolve, reject) {
          var queries = [];

          var time_last = req.body.time_begin;
          var minute_bucket, event_time_bucket;
          var signal_count = 0;
          var fishactivities = [];
          req.body.fishactivities.forEach(function(fishactivity) {

            minute_bucket = Math.floor(moment(time_last).format('mm') / 13);
            event_time_bucket = moment(time_last).format('HH') + minute_bucket;

            var value_fishActivitiesByFeeding_Meta = {
              cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
              feeding_time_begin: moment(req.body.time_begin).format('YYYY-MM-DD'),
              cycle_id: models.uuidFromString(req.body.cycle_id),
              event_time_bucket: event_time_bucket
            }

            var value_fishActivitiesByFeeding = {
              cycle_time_begin: value_fishActivitiesByFeeding_Meta.cycle_time_begin,
              feeding_time_begin: value_fishActivitiesByFeeding_Meta.feeding_time_begin,
              event_time_bucket: event_time_bucket,
              time_begin: moment(time_last).format('YYYY-MM-DD HH:mm:ss.SSS'),
              time_end: moment(time_last).add(fishactivity.time_elapsed, 'ms').format('YYYY-MM-DD HH:mm:ss.SSS'),
              cycle_id: value_fishActivitiesByFeeding_Meta.cycle_id,
              feeding_id: models.uuidFromString(req.body.feeding_id),
              motion: fishactivity.motion,
              sound: fishactivity.sound,
              label: fishactivity.label
            }

            var save_fishActivitesByFeeding_Meta = new models.instance.fishActivitiesByFeeding_Meta(value_fishActivitiesByFeeding_Meta).save({
              return_query: true,
              ttl: 94867200
            });
            queries.push(save_fishActivitesByFeeding_Meta);

            var save_fishActivitesByFeeding = new models.instance.fishActivitiesByFeeding(value_fishActivitiesByFeeding).save({
              return_query: true,
              ttl: 94867200
            });
            queries.push(save_fishActivitesByFeeding);

            fishactivities.push({
              event_time_bucket: value_fishActivitiesByFeeding.event_time_bucket,
              time_begin: value_fishActivitiesByFeeding.time_begin,
              time_end: value_fishActivitiesByFeeding.time_end,
              motion: value_fishActivitiesByFeeding.motion,
              sound: value_fishActivitiesByFeeding.sound,
              label: value_fishActivitiesByFeeding.label
            });

            time_last = value_fishActivitiesByFeeding.time_end;
            signal_count++;
            if (signal_count === req.body.fishactivities.length) {
              var _callback = {
                cycle_time_begin: value_fishActivitiesByFeeding_Meta.cycle_time_begin,
                feeding_time_begin: value_fishActivitiesByFeeding_Meta.feeding_time_begin,
                cycle_id: value_fishActivitiesByFeeding_Meta.cycle_id,
                feeding_id: value_fishActivitiesByFeeding.feeding_id,
                signal_count: signal_count,
                fishactivities: fishactivities
              };
              models.doBatch(queries, function(err) {
                (err) ? reject(err): resolve(_callback);
              });
            }
          });
        });
      }
      save_fishactivities()
        .then(function(response) {
          res.status(200).json(response);
        })
        .catch(function(error) {
          console.error(error);
        });

    } else {
      res.status(401).json({
        method: "addFishActivities",
        message: "Unauthorized"
      })
    }

  };
  /******** GET fISH ACTIVITIES BY FEEDING META-MERGED ********/

  var getFishActivitiesByFeeding_FeedingTimeBegin_MetaMerged = function(req, res) {
    var meta_fishactivities = [];
    var _callback = {
      page_state_fishactivities_meta: req.params.page_state_fishactivities_meta
    }
    var query_fishActivitiesByFeeding_Meta = {
      cycle_time_begin: req.params.cycle_time_begin,
      cycle_id: models.uuidFromString(req.params.cycle_id),
      feeding_time_begin: req.params.feeding_time_begin
    }

    var options_fishActivitiesByFeeding_Meta = {
      select: ["event_time_bucket"],
      fetchSize: 50
    }
    var send_json = function(response) {
      if (typeof response.meta_fishactivities !== "undefined") {
        if (response.meta_fishactivities.length > 0) {
          res.status(200).json(response);
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({
          method: 'getFishActivitiesByFeeding',
          message: 'invalid params'
        });
      }
    }
    if (typeof req.params.page_state_fishactivities_meta !== "undefined") {
      options_fishActivitiesByFeeding_Meta.pageState = _callback.page_state_fishactivities_meta
    }
    var get_meta_fishactivities = function(callback) {
      var promise = new Promise(function(resolve, reject) {
        models.instance.fishActivitiesByFeeding_Meta.eachRow(query_fishActivitiesByFeeding_Meta, options_fishActivitiesByFeeding_Meta, function(n, row) {
          meta_fishactivities.push(row);
        }, function(err, meta_fishactivity) {
          if (err) reject(err);
          if (meta_fishactivity) {
            _callback.page_state_fishactivities_meta = meta_fishactivity.pageState;
            _callback.meta_fishactivities = meta_fishactivities;
            _callback.fishactivities = [];
            resolve(_callback);
          }
        })
      });
      promise.then(function(response) {
        if (response.meta_fishactivities.length > 0) {
          callback(response);
        } else {
          res.status(404).send('{}');
        }
      }, function(error) {
        console.error(error);
      });
    }
    var get_fishactivities = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        var i = 0;
        _callback.meta_fishactivities.forEach(function(meta_fishactivity) {
          var query_fishActivitiesByFeeding = {
            cycle_time_begin: req.params.cycle_time_begin,
            cycle_id: models.uuidFromString(req.params.cycle_id),
            feeding_time_begin: req.params.feeding_time_begin,
            event_time_bucket: meta_fishactivity.event_time_bucket
          }

          var options_fishActivitiesByFeeding = {
            select: ["cycle_id", "feeding_id", "time_begin", 'species', 'fish_weight_avg', 'motion', 'sound']
          }
          models.instance.fishActivitiesByFeeding.eachRow(query_fishActivitiesByFeeding, options_fishActivitiesByFeeding, function(n, row) {
            _callback.fishactivities.push(row);
          }, function(err, fishactivity) {
            if (err) throw err;
            if (fishactivity) {
              i++;
              if (i === _callback.meta_fishactivities.length) {
                resolve(_callback);
              }
            }
          });
        });

      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      });
    }
    get_meta_fishactivities(get_fishactivities);
  };
  /******** GET fISH ACTIVITIES BY FEEDING ID ********/
  var getFishActivitiesByFeeding_FeedingTimeBegin_FeedingId = function(req, res) {
    var fishactivities = [];

    var query_fishActivitiesByFeeding = {
      cycle_time_begin: req.params.cycle_time_begin,
      cycle_id: models.uuidFromString(req.params.cycle_id),
      feeding_time_begin: req.params.feeding_time_begin,
      event_time_bucket: req.params.event_time_bucket,
      feeding_id: models.uuidFromString(req.params.feeding_id)
    }

    var options_fishActivitiesByFeeding = {
      select: ["cycle_id", "feeding_id", "time_begin", 'species', 'fish_weight_avg', 'motion', 'sound']
    }

    models.instance.fishActivitiesByFeeding.eachRow(query_fishActivitiesByFeeding, options_fishActivitiesByFeeding, function(n, row) {
      fishactivities.push(row);
    }, function(err, fishactivity) {
      if (err) throw err;
      if (fishactivity) {
        if (fishactivities.length > 0) {
          res.status(200).json(fishactivities)
        } else {
          res.status(404).send("");
        }
      } else {
        res.status(404).json({
          method: "getFishActivitiesByFeeding",
          message: "invalid params"
        });
      }
    })
  };

  /******** RETURN FISH ACTIVITIES ********/

  return {
    //android
    addFishActivities: addFishActivities,
    //end android

    //backend
    getFishActivitiesByFeeding_FeedingTimeBegin_MetaMerged: getFishActivitiesByFeeding_FeedingTimeBegin_MetaMerged,
    getFishActivitiesByFeeding_FeedingTimeBegin_FeedingId: getFishActivitiesByFeeding_FeedingTimeBegin_FeedingId
  }
}

module.exports = fishActivitiesController;
