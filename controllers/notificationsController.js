'use strict';
var moment = require('moment');

var notificationsController = function(models) {
  var getNotificationsByUser_Meta = function(req, res) {
    if (req.user.id && req.user.email && req.user.join_date) {
      var meta_notifications = [];

      var _callback = {
        page_state_meta_notification: req.params.page_state_meta_notification
      }
      var query_notificationByUser_Meta = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        user_id: req.user.id
      }
      var options_notificationByUser_Meta = {
        select: ['notification_date'],
        fetchSize: 5
      }
      if (typeof _callback.page_state_meta_notification !== "undefined") {
        options_notificationByUser.pageState = _callback.page_state_meta_notification
      }
      models.instance.notificationsByUser_Meta.eachRow(query_notificationByUser_Meta, options_notificationByUser_Meta, function(n, row) {
        meta_notifications.push(row);
      }, function(err, meta_notification) {
        if (err) console.error(err);
        if (meta_notification) {
          if (meta_notifications.length > 0) {
            _callback.meta_notifications = meta_notifications;
            _callback.page_state_meta_notification = meta_notification.pageState;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getMetaNotificationsByUser',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: 'getMetaNotificationsByUser',
        message: 'Unauthorized'
      });
    }
  }

  var getNotificationsByUser = function(req, res) {
    if (req.user.id && req.user.email && req.user.join_date) {
      var notifications = [];

      var _callback = {
        page_state_notification: req.params.page_state_notification
      }
      var query_notificationByUser = {
        notification_date: moment(req.body.notification_date).format('YYYY-MM-DD'),
        user_id: req.user.id
      }
      var options_notificationByUser = {
        fetchSize: 15
      }
      if (typeof _callback.page_state_notification !== "undefined") {
        options_notificationByUser.pageState = _callback.page_state_notification
      }
      models.instance.notificationsByUser.eachRow(query_notificationByUser, options_notificationByUser, function(n, row) {
        notifications.push(row);
      }, function(err, notification) {
        if (err) console.error(err);
        if (notification) {
          if (notifications.length > 0) {
            _callback.notifications = notifications;
            _callback.page_state_notification = notification.pageState;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getNotificationsByUser',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: 'getNotificationsByUser',
        message: 'Unauthorized'
      });
    }
  }

  var addNotification = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.cluster_id && req.user.email) {
      var queries = [];
      var value_notificationByUser_Meta = {
        email_bucket: req.user.email.substr(0, 3).toUpperCase(),
        user_id: req.user.id,
        notification_date: moment().format('YYYY-MM-DD')
      }

      var value_notificationByUser = {
        notification_date: value_notificationByUser_Meta.notification_date,
        user_id: value_notificationByUser_Meta.user_id,
        event_time: moment().format('YYYY-MM-DD HH:mm:ss'),
        topic: req.body.topic,
        description: req.body.description,
        return_to: req.body.return_to,
        read: false
      }
      var save_notificationsByUser_Meta = new models.instance.notificationsByUser_Meta(value_notificationByUser_Meta).save({
        return_query: true,
        ttl: 604800
      });
      queries.push(save_notificationsByUser_Meta);

      var save_notificationsByUser = new models.instance.notificationsByUser(value_notificationByUser).save({
        return_query: true,
        ttl: 604800
      });
      queries.push(save_notificationsByUser);

      models.doBatch(queries, function(err) {
        if (err) {
          console.error(err)
          res.status(404).json({
            method: "addNotification",
            message: "failed"
          });
        } else {
          res.status(200).json(value_notificationByUser);
        }
      });
    } else {
      res.status(401).json({
        method: "addNotification",
        message: "Unauthorized"
      });
    }
  }
  var updateNotification = function(req, res) {
    var query_notificationsByUser = {
      notification_date: moment(req.body.notification_date).format('YYYY-MM-DD'),
      user_id: req.user.id,
      event_time: moment(req.body.event_time).format('YYYY-MM-DD HH:mm:ss')
    }

    var value_notificationsByUser = {
      read: true
    }

    models.instance.notificationsByUser.update(query_notificationsByUser, value_notificationsByUser, function(err) {
      if (err) {
        console.error(err);
      } else {
        var _callback = {
          notification_date: query_notificationsByUser.notification_date,
          user_id: query_notificationsByUser.user_id,
          event_time: query_notificationsByUser.event_time,
          read: value_notificationsByUser.read
        }
        res.status(200).json(_callback);
      }
    })
  }
  return {
    //ANDROID
    getNotificationsByUser_Meta: getNotificationsByUser_Meta,
    getNotificationsByUser: getNotificationsByUser,
    addNotification: addNotification,
    updateNotification: updateNotification
    //BACK END

  }
}

module.exports = notificationsController;
