'use strict';
var moment = require('moment');

var problemsController = function(models) {

  var getProblemsByCycle = function(req, res) {
    if (req.user.id && req.user.email && req.user.join_date) {
      var problems = [];

      var _callback = {
        page_state_problem: req.params.page_state_problem
      }

      var query_problemsByCycle = {
        cycle_time_begin: moment(req.params.cycle_time_begin).format('YYYY-MM-DD'),
        cycle_id: models.uuidFromString(req.params.cycle_id)
      }

      var options_problemsByCycle = {
        select: ["topic", "subtopic", "description", "img_url", "status", "generated_by"],
        fetchSize: 15
      }

      if (typeof _callback.page_state_problem !== "undefined") {
        options_problemsByCycle.pageState = _callback.page_state_problem
      }
      models.instance.problemsByCycle.eachRow(query_problemsByCycle, options_problemsByCycle,
        function(n, row) {
          problems.push(row);
        },
        function(err, problem) {
          if (err) console.error(err);
          if (problem) {
            if (problems.length > 0) {
              _callback.problems = problems;
              _callback.page_state_problem = problem.pageState;
              res.status(200).json(_callback);
            } else {
              res.status(404).send("");
            }
          } else {
            res.status(404).json({
              method: 'getProblemsByCycle',
              message: 'invalid params'
            });
          }
        });
    } else {
      res.status(401).json({
        method: 'getProblemsByCycle',
        message: 'Unauthorized'
      });
    }

  }

  var addProblem = function(req, res) {
    if (req.user.id && req.user.join_date && req.user.cluster_id) {
      var value_problemsByCycle = {
        cycle_time_begin: moment(req.body.cycle_time_begin).format('YYYY-MM-DD'),
        cycle_id: models.uuidFromString(req.body.cycle_id),
        topic: req.body.topic.toUpperCase(),
        subtopic: req.body.subtopic.toUpperCase(),
        id: req.body.id,
        event_time: moment().format('YYYY-MM-DD HH:mm:ss'),
        measurement: models.datatypes.unset,
        description: req.body.description,
        img_url: req.body.img_url,
        status: false,
        generated_by: req.body.generated_by
      }

      new models.instance.problemsByCycle(value_problemsByCycle).save(function(err) {
        if (err) {
          console.error(err)
          res.status(404).json({
            method: "addProblem",
            message: "failed"
          });
        } else {
          res.status(200).json(value_problemsByCycle);
        }
      });
    } else {
      res.status(401).json({
        method: "addProblem",
        message: "Unauthorized"
      });
    }
  }

  return {
    //ANDROID
    getProblemsByCycle: getProblemsByCycle,
    addProblem: addProblem
    //BACK END

  }
}

module.exports = problemsController;
