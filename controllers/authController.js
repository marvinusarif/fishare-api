"use strict";

var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var hash = require('./../lib/hash');
var authControllers = function(models) {

  passport.use(new BasicStrategy(function(email, password, callback) {
    var query_usersByEmail = {
      email_bucket: email.substr(0, 3).toUpperCase(),
      email: email.toUpperCase()
    }
    var options_usersByEmail = {
      select: ['id', 'cluster_id', 'email', 'password', 'join_date', 'country', 'verification_email', 'full_name', 'verification_phone'],
      limit: 1
    }
    var promise = new Promise(function(resolve, reject) {
      models.instance.usersByEmail.findOne(query_usersByEmail, options_usersByEmail, function(err, rec) {
        if (err) reject(err);
        if (rec) {
          hash.compare(password, rec.password)
            .then(function(isMatch) {
              if (isMatch) {
                var verification_email, verification_phone;
                (rec.verification_email !== null) ?
                verification_email = true:
                  verification_email = false;

                (rec.verification_phone !== null) ?
                verification_phone = true:
                  verification_phone = false;

                var _cb = {
                  id: rec.id,
                  cluster_id: rec.cluster_id,
                  email: rec.email,
                  join_date: rec.join_date,
                  country: rec.country,
                  full_name: rec.full_name,
                  verification_email: verification_email,
                  verification_phone: verification_phone,
                }
                resolve(_cb);
              } else {
                reject('User[' + email + '] : authorization is not valid ');
              }
            })
            .catch(function(error) {
              console.error(error);
            })
        } else {
          reject('User[' + email + '] : authorization is not valid ');
        }
      });
    });
    promise.then(function(response) {
      return callback(null, response);
    }, function(error) {
      console.error(error);
    });

  }));
  return {
    isAuthenticated: passport.authenticate('basic', {
      session: false
    })
  }
}

module.exports = authControllers;
