'use strict';

var moment = require('moment');
var measurementsController = function(models) {
  /************** GET META MEASUREMENTS BY CYCLE *************/
  var getMetaMeasurementsByCycle = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var meta_measurements = [];
      var _callback = {
        page_state_meta_measurement: req.params.page_state_meta_measurement
      }
      var query_measurementsByCycle_Meta = {
        cycle_time_begin: req.params.cycle_time_begin,
        parameter: req.params.parameter.toUpperCase(),
        cycle_id: models.uuidFromString(req.params.cycle_id)
      }

      var options_measurementsByCycle_Meta = {
        select: ['event_time_bucket'],
        raw: true,
        fetchSize: 50
      }
      if (typeof req.params.page_state_meta_measurement !== "undefined") {
        options_measurementsByCycle_Meta.pageState = _callback.page_state_meta_measurement;
      }
      models.instance.measurementsByCycle_Meta.eachRow(query_measurementsByCycle_Meta, options_measurementsByCycle_Meta, function(n, row) {
        meta_measurements.push(row);
      }, function(err, meta_measurement) {
        if (err) throw err;
        if (meta_measurement) {
          if (meta_measurements.length > 0) {
            _callback.page_state_meta_measurement = meta_measurement.pageState;
            _callback.meta_measurements = meta_measurements;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getMetaMeasurementsByCycle',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getMetaMeasurementsByCycle",
        message: "Unauthorized"
      })
    }
  };
  /************** GET MEASUREMENTS BY CYCLE *************/

  var getMeasurementsByCycle = function(req, res) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var measurements = [];
      var _callback = {
        page_state_measurement: req.params.page_state_measurement
      }
      var query_measurementsByCycle = {
        cycle_time_begin: req.params.cycle_time_begin,
        cycle_id: models.uuidFromString(req.params.cycle_id),
        parameter: req.params.parameter.toUpperCase(),
        event_time_bucket: req.params.event_time_bucket
      }
      var options_measurementsByCycle = {
        select: ['parameter', 'value', 'event_time'],
        raw: true,
        fetchSize: 50
      }
      if (typeof req.params.page_state_measurement !== "undefined") {
        options_measurementsByCycle.pageState = _callback.page_state_measurement;
      }
      models.instance.measurementsByCycle.eachRow(query_measurementsByCycle, options_measurementsByCycle, function(n, row) {
        measurements.push(row);

      }, function(err, measurement) {
        if (err) throw err;
        if (measurement) {
          if (measurements.length > 0) {
            _callback.page_state_measurement = measurement.pageState;
            _callback.measurements = measurements;
            res.status(200).json(_callback);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getMeasurementsByCycle',
            message: 'invalid params'
          });
        }
      });
    } else {
      res.status(401).json({
        method: "getMeasurementsByCycle",
        message: "Unauthorized"
      })
    }
  };
  /************** ADD MEASUREMENT *************/
  var addMeasurement = function(req, res, next) {
    if ((req.user.id) && (req.user.cluster_id) && (req.user.join_date)) {
      var _event_time_bucket = moment(req.body.event_time).format('YYYYMMDDHH');
      var value_measurementsByCycle_Meta = {
        cycle_time_begin: req.body.cycle_time_begin,
        cycle_id: models.uuidFromString(req.body.cycle_id),
        event_time_bucket: _event_time_bucket
      }
      var value_measurementsByCycle = {
        cycle_time_begin: value_measurementsByCycle_Meta.cycle_time_begin,
        cycle_id: value_measurementsByCycle_Meta.cycle_id,
        event_time_bucket: value_measurementsByCycle_Meta.event_time_bucket,
        event_time: req.body.event_time,
      }
      var value_measurementsByCluster_Meta = {
        cluster_id: req.user.cluster_id,
        fish_species: req.body.fish_species,
        crop_species: req.body.crop_species,
        event_time_bucket: value_measurementsByCycle_Meta.event_time_bucket
      }
      var value_measurementsByCluster = {
        cluster_id: value_measurementsByCluster_Meta.cluster_id,
        fish_species: value_measurementsByCluster_Meta.fish_species,
        crop_species: value_measurementsByCluster_Meta.crop_species,
        cycle_id: value_measurementsByCycle_Meta.cycle_id,
        event_time_bucket: value_measurementsByCycle_Meta.event_time_bucket,
        event_time: value_measurementsByCycle.event_time,
      }
      var save_measurements = function() {
        return new Promise(function(resolve, reject) {
          var queries = [];
          var i = 0;
          var parameters_passed = {};
          req.body.parameters.forEach(function(_parameter) {
            value_measurementsByCycle_Meta.parameter = _parameter[0].toUpperCase();
            value_measurementsByCycle.parameter = value_measurementsByCycle_Meta.parameter;
            value_measurementsByCycle.value = _parameter[1];
            value_measurementsByCluster_Meta.parameter = value_measurementsByCycle_Meta.parameter;
            value_measurementsByCluster.parameter = value_measurementsByCycle_Meta.parameter;
            value_measurementsByCluster.value = value_measurementsByCycle.value;

            var save_measurementsByCycle_Meta = new models.instance.measurementsByCycle_Meta(value_measurementsByCycle_Meta).save({
              ttl: 94867200,
              return_query: true
            });
            var save_measurementsByCluster_Meta = new models.instance.measurementsByCluster_Meta(value_measurementsByCluster_Meta).save({
              ttl: 94867200,
              return_query: true
            });
            queries.push(save_measurementsByCycle_Meta, save_measurementsByCluster_Meta);

            var save_measurementsByCycle = new models.instance.measurementsByCycle(value_measurementsByCycle).save({
              ttl: 94867200,
              return_query: true
            });
            var save_measurementsByCluster = new models.instance.measurementsByCluster(value_measurementsByCluster).save({
              ttl: 94867200,
              return_query: true
            });
            queries.push(save_measurementsByCycle, save_measurementsByCluster);

            parameters_passed[value_measurementsByCycle_Meta.parameter] = {
              event_time: req.body.event_time,
              value: value_measurementsByCycle.value,
              trend: 0
            };

            i++;
            if (i === req.body.parameters.length) {
              //normalize parameter
              req.body.id = req.body.cycle_id;
              req.body.measurement = parameters_passed;
              models.doBatch(queries, function(err) {
                (err) ? reject(err): resolve();
              });
            }
          });
        });
      }

      save_measurements()
        .then(function() {
          next();
        })
        .catch(function(error) {
          console.error(error);
        })

    } else {
      res.status(401).json({
        method: "AddMeasurement",
        message: "Unauthorized"
      })
    }
  };

  /***** GET MEASUREMENTS BY CLUSTER SPECIES META-MERGED ******/
  var getMeasurementsByCluster_Species_MetaMerged = function(req, res) {

    var _callback = {
      page_state_meta_measurement: req.params.page_state_meta_measurement
    }
    var query_measurementsByCluster_Meta = {
      cluster_id: models.uuidFromString(req.params.cluster_id),
      fish_species: req.params.fish_species,
      crop_species: req.params.crop_species,
      parameter: req.params.parameter
    }
    var options_measurementsByCluster_Meta = {
      select: ['event_time_bucket'],
      raw: true,
      fetchSize: 120
    }
    if (typeof req.params.page_state_meta_measurement !== "undefined") {
      options_measurementsByCluster_Meta.pageState = _callback.page_state_meta_measurement;
    }
    var get_meta_measurements = function(callback) {
      var meta_measurements = [];
      var promise = new Promise(function(resolve, reject) {
        models.instance.measurementsByCluster_Meta.eachRow(query_measurementsByCluster_Meta, options_measurementsByCluster_Meta, function(n, row) {
          meta_measurements.push(row);

        }, function(err, meta_measurement) {
          if (err) throw err;
          if (meta_measurement) {
            _callback.page_state_meta_measurement = meta_measurement.pageState;
            _callback.meta_measurements = meta_measurements;
            _callback.measurements = [];
            resolve(_callback);
          }
        });
      })
    }
    var get_measurements = function(_callback) {
      var promise = new Promise(function(resolve, reject) {
        _callback.meta_measurements.forEach(function(meta_measurement) {
          var query_measurementsByCluster = {
            cluster_id: models.uuidFromString(req.params.cluster_id),
            fish_species: req.params.fish_species,
            crop_species: req.params.crop_species,
            parameter: req.params.parameter,
            event_time_bucket: meta_measurement.event_time_bucket
          }
          var options_measurementsByCluster = {
            select: ['parameter', 'value', 'event_time'],
            raw: true
          }
          models.instance.measurementsByCluster.eachRow(query_measurementsByCluster, options_measurementsByCluster, function(n, row) {
            _callback.measurements.push(row);

          }, function(err, measurement) {
            if (err) reject(err);
            if (measurement) {
              i++
              if (i === _callback.meta_measurements.length) {
                resolve(_callback);
              }
            }
          });
        });

      });
      promise.then(function(response) {
        send_json(response);
      }, function(error) {
        console.error(error);
      })
    }
    get_meta_measurements(get_measurements);

    var _callback = {
      cycle_time_begin: value_measurementsByCycle_Meta.cycle_time_begin,
      cycle_id: value_measurementsByCycle.cycle_id,
      event_time_bucket: value_measurementsByCycle_Meta.event_time_bucket,
      event_time: value_measurementsByCycle.event_time,
      fish_species: value_measurementsByCluster_Meta.fish_species,
      crop_species: value_measurementsByCluster_Meta.crop_species,
      parameters: params
    }
    res.status(200).json(_callback);
  };

  /***** RETURN MEASUREMENTS ******/

  return {
    //android
    getMetaMeasurementsByCycle: getMetaMeasurementsByCycle,
    getMeasurementsByCycle: getMeasurementsByCycle,
    addMeasurement: addMeasurement,
    //end android

    //backend
    getMeasurementsByCluster_Species_MetaMerged: getMeasurementsByCluster_Species_MetaMerged
  }
}

module.exports = measurementsController;
