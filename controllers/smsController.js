var clientConfig = require('./../config/mainAPI');
var querystring = require('querystring');
var https = require('https');

var smsController = function() {

  function isEmpty(obj) {
    return !Object.keys(obj).length;
  }

  var requestedToken = {};

  var getToken = function() {
    return new Promise(function(resolve, reject) {
      var postData = querystring.stringify({
        grant_type: 'client_credentials'
      });
      var postOptions = {
        host: clientConfig.hostname,
        port: clientConfig.port,
        path: clientConfig.path_token,
        method: 'POST',
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": clientConfig.api_token_type + ' ' + clientConfig.api_access_token
        }
      };
      // Set up the request
      var post_req = https.request(postOptions, function(res) {
        res.setEncoding('utf8');
        res.on('data', function(data) {
          data = JSON.parse(data);
          resolve(data);
        });
      });
      // post the data
      post_req.write(postData);
      post_req.end();
    });
  }
  var sendSMS = function(userMobileNumber, codeStr) {
    var postData = querystring.stringify({
      msisdn: userMobileNumber.country_code + userMobileNumber.number,
      content: 'Fishare - kode verifikasi anda: ' + codeStr
    });
    var postOptions = {
      host: clientConfig.hostname,
      port: clientConfig.port,
      path: clientConfig.path_send_sms,
      method: 'POST',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": requestedToken.token_type + ' ' + requestedToken.access_token
      }
    };
    var post_req = https.request(postOptions, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(data) {
        console.log(data);
      });
    });
    // post the data
    post_req.write(postData);
    post_req.end();
  }
  var sendVerificationSMS = function(userMobileNumber, codeStr) {
    if (isEmpty(requestedToken)) {
      getToken().then(function(new_token) {
        requestedToken = new_token;
        sendSMS(userMobileNumber, codeStr);
      });
    } else if ((requestedToken.expires_in <= 5) && (!isEmpty(requestedToken))) {
      getToken().then(function(new_token) {
        requestedToken = new_token;
        sendSMS(userMobileNumber, codeStr);
      });
    } else if (!isEmpty(requestedToken)) {
      sendSMS(userMobileNumber, codeStr);
    }
  }
  return {
    sendVerificationSMS: sendVerificationSMS
  }
}

module.exports = smsController();
