'use strict';

var clustersController = function(models) {
  Array.prototype.inArray = function(value) {
    // Returns true if the passed value is found in the
    // array. Returns false if it is not.
    var i;
    for (i = 0; i < this.length; i++) {
      if (this[i] == value) {
        return true;
      }
    }
    return false;
  };
  var getCountries = function(req,res){
      var countries = require('../models/schemas/countries_list.json');
      if(countries){
        res.status(200).json(countries);
      }else{
        res.status(404).send("");
      }
  }
  var getStates = function(req,res){
    if(req.params.country){
      var states = require('../models/schemas/states_list.json');
      if(states){
        res.status(200).json(states[req.params.country]);
      }else{
        res.status(404).send("");
      }
    }else{
      res.status(404).json({
        method: 'getClustersByLocation',
        message: 'invalid params'
      });
    }
  }
  /************************* GET REGENCIES *************************/

  var getRegencies = function(req, res) {
    var regencies = [];
    var regencies_temp = [];
    var query_regencyByLocation = {
      country: req.params.country,
      state: req.params.state
    }
    var options_regencyByLocation = {
      select: ['regency']
    }
    models.instance.clustersByLocation.eachRow(query_regencyByLocation, options_regencyByLocation, function(n, row) {
        if (regencies_temp.inArray(row.regency) === false) {
          regencies_temp.push(row.regency);
          regencies.push(row);
        }
      },
      function(err, regency) {
        if (err) throw err;
        if (regency) {
          if (regencies.length > 0) {
            res.status(200).json(regencies);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getClustersByLocation',
            message: 'invalid params'
          });
        }

      })
  }
  /************************* GET DISTRICTS *************************/

  var getDistricts = function(req, res) {
    var districts = [];
    var districts_temp = [];
    var query_districtByLocation = {
      country: req.params.country,
      state: req.params.state,
      regency: req.params.regency
    }
    var options_districtByLocation = {
      select: ['district']
    }
    models.instance.clustersByLocation.eachRow(query_districtByLocation, options_districtByLocation, function(n, row) {
        if (districts_temp.inArray(row.district) === false) {
          districts_temp.push(row.district);
          districts.push(row);
        }
      },
      function(err, district) {
        if (err) throw err;
        if (district) {
          if (districts.length > 0) {
            res.status(200).json(districts);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getClustersByLocation',
            message: 'invalid params'
          });
        }

      })
  };
  /************************* GET VILLAGES *************************/

  var getVillages = function(req, res) {
    var villages = [];
    var villages_temp = [];
    var query_villageByLocation = {
      country: req.params.country,
      state: req.params.state,
      regency: req.params.regency,
      district: req.params.district
    }
    var options_villageByLocation = {
      select: ['village', 'id']
    }
    models.instance.clustersByLocation.eachRow(query_villageByLocation, options_villageByLocation, function(n, row) {
        if (villages_temp.inArray(row.village) === false) {
          villages_temp.push(row.village);
          villages.push(row);
        }
      },
      function(err, village) {
        if (err) throw err;
        if (village) {
          if (villages.length > 0) {
            res.status(200).json(villages);
          } else {
            res.status(404).send("");
          }
        } else {
          res.status(404).json({
            method: 'getClustersByLocation',
            message: 'invalid params'
          });
        }

      })
  };
  /************************* GET CLUSTER *************************/

  var getCluster = function(req, res) {
    var villages = [];
    var villages_temp = [];
    var query_villageByLocation = {
      country: req.params.country,
      state: req.params.state,
      regency: req.params.regency,
      district: req.params.district,
      village: req.params.village
    }
    var options_villageByLocation = {
      select: ['village', 'id', 'country', 'state', 'regency', 'district']
    }
    models.instance.clustersByLocation.findOne(query_villageByLocation, options_villageByLocation, function(err, village) {
      if (err) throw err;
      if (village) {
        res.status(200).json(village);
      } else {
        res.status(404).json({
          method: 'getClustersByLocation',
          message: 'invalid params'
        });
      }
    });
  };
  /************************* ADD CLUSTER *************************/

  var addCluster = function(req, res) {
    var value_clustersByLocation = {
      id: models.uuid(),
      country: req.body.country.toUpperCase(),
      state: req.body.state.toUpperCase(),
      regency: req.body.regency.toUpperCase(),
      district: req.body.district.toUpperCase(),
      village: req.body.village.toUpperCase()
    }
    var save_cluster = function() {
      return new Promise(function(resolve, reject) {
        var _callback = {
          id: models.uuid(),
          country: req.body.country.toUpperCase(),
          state: req.body.state.toUpperCase(),
          regency: req.body.regency.toUpperCase(),
          district: req.body.district.toUpperCase(),
          village: req.body.village.toUpperCase()
        }
        new models.instance.clustersByLocation(value_clustersByLocation).save(function(err) {
          (err) ? reject(err): resolve(_callback);
        });
      });
    }
    save_cluster()
      .then(function(response) {
        res.status(200).json(response);
      })
      .catch(function(error) {
        console.error(error);
      })
  };

  /************************* RETURN CLUSTER *************************/

  return {
    //ANDROID
    getCountries : getCountries,
    getStates : getStates,
    getRegencies: getRegencies,
    getDistricts: getDistricts,
    getVillages: getVillages,
    getCluster: getCluster,
    //BACK END
    addCluster: addCluster
  }
}

module.exports = clustersController;
