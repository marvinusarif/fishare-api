var bcrypt = require('bcrypt-nodejs');

var hash = function() {
  var hashing = function(original_string) {
    return new Promise(function(resolve, reject) {
      bcrypt.genSalt(Math.floor(Math.random() * 200), function(err, salt) {
        if (err) reject(err);
        bcrypt.hash(original_string, salt, null, function(err, hash) {
          if (err) reject(err);
          if (hash) {
            resolve(hash);
          }
        })
      });
    });
  }

  var compare = function(strA, strB) {
    return new Promise(function(resolve, reject) {
      bcrypt.compare(strA, strB, function(err, isMatch) {
        if (err) reject(err);
        resolve(isMatch);
      });
    })

  }

  return {
    hashing: hashing,
    compare: compare
  }

}

module.exports = hash();
