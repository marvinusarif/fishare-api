var driver = require('dse-driver');
var models = require("express-cassandra");

var cassandra_establish_connection = new Promise(function(resolve, reject) {
  models.setDirectory(__dirname + '/schemas').bind({
    clientOptions: {
      //development environment : digitalocean
      contactPoints: ['139.59.127.2'],
      compression: true,
      queryOptions: {
        consistency: models.consistencies.one
      },
      /*
      production environment : azure
      10.0.0.4 is the docker agents IP Address not the cassandra broadcast IP Address! It is different!
      docker agents IP Address : 10.0.0.4 - 10.0.0.6 ( 3 agents )
      cassandra broadcast address : 10.0.0.2 - 4
      //*/
      /* contactPoints: [ '10.0.0.4', '10.0.0.5', '10.0.0.6', 'fishareagents.southeastasia.cloudapp.azure.com'],
            compression: true,
            policies: {
                loadBalancing: new driver.policies.loadBalancing.DCAwareRoundRobinPolicy('dc1')
            },
            queryOptions: {
                consistency: models.consistencies.quorum
            },
        */
      protocolOptions: {
        port: 9042
      },
      keyspace: 'fishare_db',
      authProvider: new models.driver.auth.DsePlainTextAuthProvider('cassandra', 'cassandra')
    },
    ormOptions: {
      udts: {
        full_name: {
          first_name: "text",
          last_name: "text"
        },
        address: {
          street: "text",
          region: "text",
          district: "text",
          city: "text",
          state: "text",
          country: "text",
          zip: "text"
        },
        phone: {
          country_code: "text",
          number: "text"
        },
        identification: {
          id_number: "text",
          valid_date: "text",
          description: "text"
        },
        social_media: {
          username: "text",
          token: "text",
          last_login: "timestamp"
        },
        profile: {
          marital: "text",
          dependant: "text",
          job_first: "text",
          job_second: "text",
          experience_first: "text",
          experience_second: "text"
        },
        fish: {
          id: "uuid",
          species: "text",
          producer: "text",
          qty: "float",
          weight: "float"
        },
        crop: {
          id: "uuid",
          species: "text",
          producer: "text",
          qty: "float",
          weight: "float"
        },
        pond: {
          type: "text",
          shape: "text",
          length: "float",
          width: "float",
          depth: "float",
          volume: "float",
          aeration: "boolean",
          recirculation: "boolean"
        },
        pond_min: {
          volume: "float",
          aeration: "boolean",
          recirculation: "boolean"
        },
        sample: {
          date_taken: "timestamp",
          species: "text",
          weight: "float",
          width: "float",
          length: "float"
        },
        harvest: {
          species: "text",
          weight: "float",
          quantity: "float"
        },
        ratio: {
          fce: "float",
          fcr: "float",
          sgr: "float",
          adg: "float",
          density: "float",
          per: "float",
          sr: "float"
        },
        feed: {
          type: "text",
          spesification: "text",
          floating: "boolean",
          name: "text",
          protein: "float"
        },
        feed_sum: {
          type: "text",
          floating: "boolean",
          spesification: "text",
          name: "text",
          protein: "float",
          protein_total: "int",
          quantity_total: "int"
        },
        measurement: {
          event_time: "timestamp",
          value: "float",
          trend: "float"
        },
        feeding_program: {
          event_time: "timestamp",
          quantity: "float",
          frequency: "float"
        },
        problem: {
          id: "uuid",
          topic: "text",
          subtopic: "text",
          event_time: "timestamp"
        },
        sensor: {
          type: "text",
          date: "timestamp"
        }
      },
      defaultReplicationStrategy: {
        class: 'SimpleStrategy',
        replication_factor: 3
      },
      migration: 'alter',
      createKeyspace: true
    }
  }, function(err) {
    if (err) {
      reject(err.message);
    } else {
      resolve('connected to db');
    }
  });
});

module.exports = cassandra_establish_connection;
