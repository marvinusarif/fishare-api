module.exports = {
  fields: {
    email_bucket: "text",
    email: "text",
    password: "text",
    id: "uuid",
    cluster_id: "uuid",
    full_name: {
      type: "frozen",
      typeDef: "<full_name>"
    },
    birthdate: "timestamp",
    country: "text",
    gender: "text",
    join_date: "timestamp",
    verification_phone: "timestamp",
    verification_email: "timestamp"
  },
  key: [
    ["email_bucket"], "email"
  ],
  clustering_order: {
    "email": "ASC"
  },
  table_name: "users_by_email"
}
