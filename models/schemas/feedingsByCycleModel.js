module.exports = {
  fields: {
    user_join_date: "timestamp",
    cycle_time_begin: "timestamp",
    cycle_id: "uuid",
    time_begin: "timestamp",
    time_end: "timestamp",
    id: "uuid",
    quantity: "float",
    species: "text",
    fish_weight_avg: "float",
    fish_weight: "float",
    fish_quantity: "float",
    protein: "float", //*quantity * feed_protein
    measurement: {
      type: "map",
      typeDef: "<text, frozen <measurement>>"
    },
    feed: {
      type: "map",
      typeDef: "<uuid, frozen<feed>>"
    }
  },
  key: [
    ["user_join_date", "cycle_time_begin"], "cycle_id", "time_begin", "id"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "time_begin": "DESC",
    "id": "DESC"
  },
  table_name: "feedings_by_cycle"
}
