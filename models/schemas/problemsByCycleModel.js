module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    cycle_id: "uuid",
    topic: "text",
    subtopic: "text",
    id: "uuid",
    event_time: "timestamp",
    description: "text",
    measurement: {
      type: "frozen",
      typeDef: "<measurement>"
    },
    img_url: "text",
    status: "boolean",
    generated_by: "text"
  },
  key: [
    ["cycle_time_begin"], "cycle_id", "topic", "subtopic", "id"
  ],
  clustering_order: {
    cycle_id: "DESC",
    topic: "DESC",
    subtopic: "DESC",
    id: "DESC"
  },
  table_name: "problems_by_cycle"
}
