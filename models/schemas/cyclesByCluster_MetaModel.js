module.exports = {
  fields: {
    cluster_id: "uuid",
    cycle_time_begin: "timestamp"
  },
  key: ["cluster_id", "cycle_time_begin"],

  clustering_order: {
    "cycle_time_begin": "DESC"
  },
  table_name: "cycles_by_cluster_meta"
}
