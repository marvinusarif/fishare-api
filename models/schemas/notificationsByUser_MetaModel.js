module.exports = {
  fields: {
    email_bucket: "text",
    user_id: "uuid",
    notification_date: "timestamp"
  },
  key: [
    ["email_bucket"], "user_id", "notification_date"
  ],
  clustering_order: {
    user_id: "DESC",
    notification_date: "DESC"
  },
  table_name: "notifications_by_user_meta"
}
