module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    cycle_id: "uuid",
    event_time: "timestamp",
    helpful: "boolean",
    topic: "text",
    description: "text",
    problem: {
      type: "frozen",
      typeDef: "<problem>"
    },
    posted_by: "text"
  },
  key: [
    ["cycle_time_begin"], "cycle_id", "event_time"
  ],
  clustering_order: {
    cycle_id: "DESC",
    event_time: "DESC"
  },
  table_name: "recommendations_by_cycle"
}
