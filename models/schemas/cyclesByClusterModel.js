module.exports = {
  fields: {
    cluster_id: "uuid",
    cycle_time_begin: "timestamp", //'2016-12-29 00:00:00.000000+0000'
    time_end: "timestamp",
    time_begin: "timestamp",
    user_id: "uuid",
    id: "uuid",
    name: "text",
    fish: {
      type: "frozen",
      typeDef: "<fish>"
    },
    crop: {
      type: "frozen",
      typeDef: "<crop>"
    },
    pond: {
      type: "frozen",
      typeDef: "<pond>"
    },
    sample: {
      type: "map",
      typeDef: "<text, frozen<sample>>"
    },
    harvest: {
      type: "map",
      typeDef: "<text, frozen<harvest>>"
    },
    ratio: {
      type: "frozen",
      typeDef: "<ratio>"
    },
    feed: {
      type: "map",
      typeDef: "<uuid, frozen<feed_sum>>"
    },
    measurement: {
      type: "map",
      typeDef: "<text, frozen<measurement>>"
    },
  },
  key: [
    ["cluster_id", "cycle_time_begin"], "user_id", "id"
  ],
  clustering_order: {
    "user_id": "DESC",
    "id": "DESC"
  },
  table_name: "cycles_by_cluster"
}
