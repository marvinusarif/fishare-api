module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    feeding_time_begin: "timestamp",
    cycle_id: "uuid",
    event_time_bucket: "text"
  },
  key: [
    ["cycle_time_begin", "feeding_time_begin"], "cycle_id", "event_time_bucket"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "event_time_bucket": "DESC"
  },
  table_name: "fishactivities_by_feeding_meta"
}
