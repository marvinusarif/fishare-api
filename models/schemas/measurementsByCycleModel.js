module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    cycle_id: "uuid",
    parameter: "text",
    event_time_bucket: "text", //yyyymmddhh+order number of every 5 minutes
    event_time: "timestamp",
    value: "float"
  },
  key: [
    ["cycle_time_begin", "parameter", "event_time_bucket"], "cycle_id", "event_time"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "event_time": "DESC"
  },
  table_name: "measurements_by_cycle"
}
