module.exports = {
  fields: {
    date_taken: "timestamp", //'2016-12-29 00:00:00.000000+0000'
    type: "text",
    species: "text",
    event_time: "timeuuid",
    producer: "text",
    length: "float",
    width: "float",
    weight: "float",
    orientation: "text",
    img_url: "text"
  },
  key: [
    ["date_taken", "type", "species"], "producer", "orientation", "event_time"
  ],
  clustering_order: {
    "producer": "DESC",
    "orientation": "ASC",
    "event_time": "DESC"
  },
  table_name: "samples_by_species"
}
