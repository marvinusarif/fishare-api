module.exports = {
  fields: {
    user_id: "uuid",
    harvest_date: "timestamp" //dd-mm-yy only
  },
  key: [
    ["user_id"], "harvest_date"
  ],
  clustering_order: {
    "harvest_date": "DESC"
  },
  table_name: "harvests_by_user_meta"
}
