module.exports = {
  fields: {
    cycle_time_begin: "timestamp", //'2016-12-29 00:00:00.000000+0000'
    cycle_id: "uuid",
    event_time: "timeuuid",
    type: "text",
    species: "text",
    length: "float",
    width: "float",
    weight: "float"
  },
  key: [
    ["cycle_time_begin"], "cycle_id", "type", "event_time"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "type": "DESC",
    "event_time": "DESC"
  },
  table_name: "samples_by_cycle"
}
