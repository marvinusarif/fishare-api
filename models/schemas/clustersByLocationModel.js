module.exports = {
  fields: {
    country: "text",
    state: "text",
    regency: "text",
    district: "text",
    village: "text",
    id: "uuid"
  },
  key: [
    ["country", "state"], "regency", "district", "village"
  ],
  clustering_order: {
    "regency": "ASC",
    "district": "ASC",
    "village": "ASC"
  },
  table_name: "clusters_by_location"
}
