module.exports = {
  fields: {
    cluster_id: "uuid",
    fish_species: "text",
    crop_species: "text",
    parameter: "text",
    event_time_bucket: "text" //yyyymmddhh+order number of every 15 minutes
  },
  key: [
    ["cluster_id", "fish_species", "crop_species", "parameter"], "event_time_bucket"
  ],
  clustering_order: {
    "event_time_bucket": "DESC"
  },
  table_name: "measurements_by_cluster_meta"
}
