module.exports = {
  fields: {
    year_month: "text",
    cluster_id: "uuid",
    cycle_time_begin: "timestamp"
  },
  key: [
    ["year_month", "cluster_id"], "cycle_time_begin"
  ],
  clustering_order: {
    "cycle_time_begin": "DESC"
  },
  table_name: "feedings_by_cluster_meta"
}
