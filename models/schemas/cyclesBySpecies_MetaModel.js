module.exports = {
  fields: {
    fish_species: "text",
    crop_species: "text",
    cycle_time_begin: "timestamp" //'2016-12-29 00:00:00.000000+0000'
  },
  key: [
    ["fish_species", "crop_species"], "cycle_time_begin"
  ],
  clustering_order: {
    "cycle_time_begin": "DESC"
  },
  table_name: "cycles_by_species_meta"
}
