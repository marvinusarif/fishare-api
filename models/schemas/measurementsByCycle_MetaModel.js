module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    parameter: "text",
    cycle_id: "uuid",
    event_time_bucket: "text" //yyyymmddhh+order number of every 5 minutes
  },
  key: [
    ["cycle_time_begin", "parameter"], "cycle_id", "event_time_bucket"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "event_time_bucket": "DESC"
  },
  table_name: "measurements_by_cycle_meta"
}
