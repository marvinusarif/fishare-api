module.exports = {
  fields: {
    cluster_id: "uuid",
    fish_species: "text",
    crop_species: "text",
    parameter: "text",
    event_time_bucket: "text", //yyyymmddhh+order number of every 5 minutes
    event_time: "timestamp",
    cycle_id: "uuid",
    value: "float"
  },
  key: [
    ["cluster_id", "fish_species", "crop_species", "parameter", "event_time_bucket"], "event_time", "cycle_id"
  ],
  clustering_order: {
    "event_time": "DESC",
    "cycle_id": "DESC"
  },
  table_name: "measurements_by_cluster"
}
