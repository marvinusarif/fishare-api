module.exports = {
  fields: {
    notification_date: "timestamp",
    user_id: "uuid",
    event_time: "timestamp",
    topic: "text",
    description: "text",
    return_to: "text",
    read: "boolean"
  },
  key: [
    ["notification_date"], "user_id", "event_time"
  ],
  clustering_order: {
    user_id: "DESC",
    event_time: "DESC"
  },
  table_name: "notifications_by_user"
}
