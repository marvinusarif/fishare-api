module.exports = {
  fields: {
    user_join_date: "timestamp",
    user_id: "uuid",
    cycle_time_begin: "timestamp" // //'2016-12-29 00:00:00.000000+0000'
  },
  key: [
    ["user_join_date"], "user_id", "cycle_time_begin"
  ],
  clustering_order: {
    "user_id": "DESC",
    "cycle_time_begin": "DESC"
  },
  table_name: "cycles_by_user_meta"
}
