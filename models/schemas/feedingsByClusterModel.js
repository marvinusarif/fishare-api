module.exports = {
  fields: {
    cluster_id: "uuid",
    year_month: "text",
    cycle_time_begin: "timestamp",
    time_begin: "timestamp",
    time_end: "timestamp",
    user_id: "uuid",
    id: "uuid",
    quantity: "float",
    cycle_id: "uuid",
    species: "text",
    fish_weight_avg: "float",
    fish_weight: "float",
    fish_quantity: "float",
    measurement: {
      type: "map",
      typeDef: "<text, frozen <measurement>>"
    },
    feed: {
      type: "map",
      typeDef: "<uuid, frozen<feed>>"
    }
  },
  key: [
    ["cluster_id", "year_month", "cycle_time_begin"], "cycle_id", "user_id", "time_begin"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "user_id": "DESC",
    "time_begin": "DESC"
  },
  table_name: "feedings_by_cluster"
}
