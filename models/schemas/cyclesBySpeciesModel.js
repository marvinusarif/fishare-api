module.exports = {
  fields: {
    cycle_time_begin: "timestamp", //'2016-12-29 00:00:00.000000+0000'
    fish_species: "text",
    crop_species: "text",
    cluster_id: "uuid",
    time_end: "timestamp",
    time_begin: "timestamp",
    "name": "text",
    id: "uuid",
    pond_min: {
      type: "frozen",
      typeDef: "<pond_min>"
    },
    sample: {
      type: "map",
      typeDef: "<text, frozen<sample>>"
    },
    harvest: {
      type: "map",
      typeDef: "<text, frozen<harvest>>"
    },
    ratio: {
      type: "frozen",
      typeDef: "<ratio>"
    }
  },
  key: [
    ["cycle_time_begin", "fish_species", "crop_species"], "cluster_id", "id"
  ],
  clustering_order: {
    "cluster_id": "ASC",
    "id": "DESC"
  },
  table_name: "cycles_by_species"
}
