module.exports = {
  fields: {
    cluster_id: "uuid",
    join_date: "timestamp",
    gender: "text",
    email: "text",
    id: "uuid",
    birthdate: "timestamp",
    full_name: {
      type: "frozen",
      typeDef: "<full_name>"
    },
    address: {
      type: "map",
      typeDef: "<text, frozen <address>>"
    },
    phone: {
      type: "map",
      typeDef: "<text, frozen <phone>>"
    },
    identification: {
      type: "map",
      typeDef: "<text, frozen <identification>>"
    },
    profile: {
      type: "frozen",
      typeDef: "<profile>"
    },
    social_media: {
      type: "map",
      typeDef: "<text, frozen <social_media>>"
    },
    lat: "text",
    long: "text",
    verification_phone: "timestamp",
    verification_email: "timestamp"
  },
  key: [
    ["cluster_id"], "join_date", "id"
  ],
  clustering_order: {
    "join_date": "DESC",
    "id": "DESC"
  },
  table_name: "users_by_cluster"
}
