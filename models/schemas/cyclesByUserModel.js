module.exports = {
  fields: {
    user_join_date: "timestamp",
    cycle_time_begin: "timestamp", //'2016-12-29 00:00:00.000000+0000'
    user_id: "uuid",
    time_end: "timestamp",
    time_begin: "timestamp",
    cluster_id: "uuid",
    name: "text",
    id: "uuid",
    fish: {
      type: "frozen",
      typeDef: "<fish>"
    },
    crop: {
      type: "frozen",
      typeDef: "<crop>"
    },
    pond: {
      type: "frozen",
      typeDef: "<pond>"
    },
    sample: {
      type: "map",
      typeDef: "<text, frozen<sample>>"
    },
    harvest: {
      type: "map",
      typeDef: "<text, frozen<harvest>>"
    },
    ratio: {
      type: "frozen",
      typeDef: "<ratio>"
    },
    feed: {
      type: "map",
      typeDef: "<uuid, frozen<feed_sum>>"
    },
    measurement: {
      type: "map",
      typeDef: "<text, frozen<measurement>>"
    },
    feeding_program: {
      type: "frozen",
      typeDef: "<feeding_program>"
    },
    img_url: "text"
  },
  key: [
    ["user_join_date", "cycle_time_begin"], "user_id", "id"
  ],
  clustering_order: {
    "user_id": "DESC",
    "id": "DESC"
  },
  custom_indexes: [{
    on: "name",
    using: "org.apache.cassandra.index.sasi.SASIIndex",
    options: {
      'mode': 'PREFIX',
      'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer',
      'case_sensitive': 'false'
    }
  }],
  table_name: "cycles_by_user"
}
