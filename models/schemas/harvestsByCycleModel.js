module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    cycle_id: "uuid",
    event_time: "timestamp",
    type: "text",
    species: "text",
    weight: "float",
    weight_avg: "float",
    quantity: "float"

  },
  key: [
    ["cycle_time_begin"], "cycle_id", "type", "event_time"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "type": "DESC",
    "event_time": "DESC"
  },
  table_name: "harvests_by_cycle"
}
