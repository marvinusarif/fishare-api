module.exports = {
  fields: {
    harvest_date: "timestamp", //date-month-year only
    cluster_id: "uuid",
    event_time: "timestamp",
    type: "text",
    species: "text",
    weight: "float",
    weight_avg: "float",
    quantity: "float"

  },
  key: [
    ["harvest_date", "cluster_id"], "type", "species", "event_time"
  ],
  clustering_order: {
    "type": "DESC",
    "species": "DESC",
    "event_time": "DESC"
  },
  table_name: "harvests_by_cluster"
}
