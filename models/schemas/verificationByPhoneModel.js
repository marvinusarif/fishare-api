module.exports = {
  fields: {
    email_bucket: "text",
    email: "text",
    phone: "text",
    country_code: "text",
    token_date: "timestamp",
    token: "text"
  },
  key: [
    ["email_bucket"], "email", "phone", "country_code"
  ],
  clustering_order: {
    email: "ASC",
    phone: "ASC",
    country_code: "ASC"
  },
  table_name: "verification_by_phone"
}
