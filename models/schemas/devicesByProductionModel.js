module.exports = {
  fields: {
    date_production: "timestamp",
    id: "uuid",
    date_calibration: "timestamp",
    date_subscription_end: "timestamp",
    lat: "text",
    long: "text",
    characteristic: {
      type: "map",
      typeDef: "<text, frozen <sensor>>"
    },
    users: {
      type: "map",
      typeDef: "<text, timestamp>"
    }
  },
  key: [
    ["date_production"], "id"
  ],
  clustering_order: {
    "id": "DESC"
  },
  table_name: "devices_by_production"
}
