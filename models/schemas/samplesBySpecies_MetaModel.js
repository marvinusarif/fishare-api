module.exports = {
  fields: {
    type: "text",
    species: "text",
    producer: "text",
    date_taken: "timestamp", //'2016-12-29 00:00:00.000000+0000'
  },
  key: [
    ["type", "species"], "producer", "date_taken"
  ],
  clustering_order: {
    "producer": "DESC",
    "date_taken": "DESC"
  },
  table_name: "samples_by_species_meta"
}
