module.exports = {
  fields: {
    country: "text",
    type: "text",
    name: "text",
    fish_species: "text",
    company: "text",
    floating: "boolean",
    id: "uuid",
    spesification: "text"
  },
  key: [
    ["country", "type"], "id"
  ],
  clustering_order: {
    "id": "DESC"
  },
  custom_indexes: [{
    on: "name",
    using: "org.apache.cassandra.index.sasi.SASIIndex",
    options: {
      'mode': 'PREFIX',
      'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer',
      'case_sensitive': 'false'
    }
  }],
  table_name: "feeds_by_country"
}
