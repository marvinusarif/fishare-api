module.exports = {
  fields: {
    cycle_time_begin: "timestamp",
    feeding_time_begin: "timestamp",
    event_time_bucket: "text",
    cycle_id: "uuid",
    feeding_id: "uuid",
    time_begin: "timestamp",
    time_end: "timestamp",
    motion: "text",
    sound: "text",
    label: "text"
  },
  key: [
    ["cycle_time_begin", "feeding_time_begin", "event_time_bucket"], "cycle_id", "feeding_id", "time_begin"
  ],
  clustering_order: {
    "cycle_id": "DESC",
    "feeding_id": "DESC",
    "time_begin": "DESC"
  },
  table_name: "fishactivities_by_feeding"
}
