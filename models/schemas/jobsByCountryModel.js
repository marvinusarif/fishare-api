module.exports = {
  fields: {
    country: "text",
    job: "text",
    description: "text",
    id: "uuid"
  },
  key: [
    ["country"], "job"
  ],
  clustering_order: {
    "job": "ASC"
  },
  custom_indexes: [{
    on: "job",
    using: "org.apache.cassandra.index.sasi.SASIIndex",
    options: {
      'mode': 'PREFIX',
      'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer',
      'case_sensitive': 'false'
    }
  }],
  table_name: "jobs_by_country"
}
