module.exports = {
  fields: {
    country: "text",
    type: "text",
    species: "text",
    producer: "text",
    id: "uuid",
    img_url: "text"
  },
  key: [
    ["country", "type"], "species", "id"
  ],
  clustering_order: {
    "species": "DESC",
    "id": "ASC"
  },
  custom_indexes: [{
    on: "producer",
    using: "org.apache.cassandra.index.sasi.SASIIndex",
    options: {
      'mode': 'PREFIX',
      'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer',
      'case_sensitive': 'false'
    }
  }],
  table_name: "species_by_country"
}
