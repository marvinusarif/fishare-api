module.exports = {
  fields: {
    email_bucket: "text",
    email: "text",
    token_date: "timestamp",
    token: "text",
    first_name: "text",
    last_name: "text"
  },
  key: [
    ["email_bucket"], "email"
  ],
  clustering_order: {
    email: "ASC"
  },
  table_name: "verification_by_email"
}
