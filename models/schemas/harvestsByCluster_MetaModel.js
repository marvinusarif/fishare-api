module.exports = {
  fields: {
    cluster_id: "uuid",
    harvest_date: "timestamp" //dd-mm-yy only
  },
  key: [
    ["cluster_id"], "harvest_date"
  ],
  clustering_order: {
    "harvest_date": "DESC"
  },
  table_name: "harvests_by_cluster_meta"
}
